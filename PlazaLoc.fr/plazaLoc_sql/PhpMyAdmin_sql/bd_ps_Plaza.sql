-- ROCEDURE Admin --

DELIMITER $$

-- Retourne info user --
CREATE PROCEDURE getUserInfo(IN _mail varchar(50),IN _mdp varchar(50))
BEGIN
  SELECT u.nom, u.prenom, u.mail, u.mdp, a.idutilisateur as admin, p.idutilisateur as proprietaire, l.idutilisateur as locataire 
  FROM utilisateur u
  LEFT JOIN admin a ON a.idutilisateur = u.idutilisateur
  LEFT JOIN proprietaire p ON p.idutilisateur = u.idutilisateur
  LEFT JOIN locataire l ON l.idutilisateur = u.idutilisateur
  WHERE mail = _mail  AND mdp = _mdp;
END $$

-- rechercher un user --
CREATE PROCEDURE getClient(IN _nom varchar(50))
BEGIN
  SELECT u.nom, u.prenom, u.adresse, u.mail, u.numtel, p.idutilisateur as proprietaire, l.idutilisateur as locataire 
  FROM utilisateur u
  LEFT JOIN proprietaire p ON p.idutilisateur = u.idutilisateur
  LEFT JOIN locataire l ON l.idutilisateur = u.idutilisateur
  WHERE LOCATE(_nom, nom)>0;
END $$

-- rechercher une location --
CREATE PROCEDURE getLocation(IN _dateDebut Date, In _dateFin Date)
BEGIN
  SELECT l.idlocation, l.datedebut, l.datefin, m.descriptionmotif, b.montantLoyer,
  DATEDIFF(LEAST(l.datefin, _datefin), GREATEST(l.datedebut, _datedebut)) as nbJr 
  FROM location l, bien b, motiflocation m
  WHERE l.idbien = b.idbien
  AND l.idmotiflocation = m.idmotiflocation
  AND l.datedebut <= _datefin 
  AND l.datefin >= _datedebut;
END $$

-- test--
CREATE PROCEDURE getListeProprietaire()
BEGIN
  SELECT * from utilisateur u, proprietaire p
  WHERE p.idutilisateur = u.idutilisateur
  AND statut is true;
END $$

CREATE PROCEDURE getListeProprietaire2()
BEGIN
  SELECT * from utilisateur u, proprietaire p
  WHERE p.idutilisateur = u.idutilisateur
  AND statut is false;
END $$

-- Retourne la liste des locataires --
CREATE PROCEDURE getListeLocataire()
BEGIN
  SELECT * from utilisateur u, locataire l
  WHERE l.idutilisateur = u.idutilisateur;
END $$

-- Supprime un proprietaire par son id --
CREATE PROCEDURE deleteProprietaire(IN _idProprietaire Int)
BEGIN
  DELETE FROM utilisateur
  WHERE idutilisateur = _idProprietaire;
END $$

CREATE PROCEDURE desactiveProprietaire(IN _idProprietaire Int)
BEGIN
  UPDATE proprietaire SET statut = false
  WHERE idutilisateur = _idProprietaire;
END $$

-- Supprime un locataire par son id --
CREATE PROCEDURE deleteLocataire(IN _idLocataire Int)
BEGIN
  DELETE FROM utilisateur
  WHERE idutilisateur = _idLocataire;
END $$

-- Insere un message --
CREATE PROCEDURE insertMessage( 
	IN _nom varchar  (50), 
	IN _prenom varchar  (50),
	IN _sujet varchar  (50), 
	IN _mail varchar  (50), 
	IN _numtel varchar (50), 
	IN _textemessage varchar (100)
	
)
BEGIN
  INSERT into messagesRecus 
  values(
		 null, null, _nom, _prenom, _sujet, _mail, _numtel, _textemessage
		) ;
END $$

-- répond sur un message --
CREATE PROCEDURE repondre(
	IN _idmessage int (5),
	IN _mail varchar  (50), 
	IN _sujet varchar  (50), 
	IN _textemessage varchar (100)
)
BEGIN
  INSERT into messagesEnvoyes (sujet, mail, textemessage) 
  SELECT _sujet, _mail, _textemessage FROM messagesRecus WHERE idmessage = _idmessage;
  DELETE from messagesRecus where idmessage= _idmessage;
END $$

-- delimiter ; --

-- Retourne la liste des biens --
CREATE PROCEDURE getListeBien()
BEGIN
  SELECT b.*, bc.chargeforfaitaire 
  from bien b, bienAvecCharges bc
  where b.idbien = bc.idbien;
END $$

-- Retourne la liste des locations --
CREATE PROCEDURE getListeLocation()
BEGIN
  SELECT l.*, m.descriptionmotif, b.montantLoyer, b.descriptionbien
  FROM location l, motiflocation m, bien b
  Where l.idmotiflocation = m.idmotiflocation
  AND l.idbien = b.idbien;
END $$

-- Retourne la liste des demandes de location --
CREATE PROCEDURE getListeDemandeLocation()
BEGIN
  SELECT * from demandelocation;
END $$

-- Retourne la liste des demandes de location sans bien --
CREATE PROCEDURE getListeDemandeLocationSansBien() 
BEGIN
  SELECT * from demandelocationsansbien;
END $$

-- Retourne la liste des messages --
CREATE PROCEDURE getListeMessage()
BEGIN
  SELECT * from messagesRecus;
END $$

-- Retourne un message via son id --
CREATE PROCEDURE getListeOnmessage(in _idmessage int)
BEGIN
  SELECT * from messagesRecus where idmessage = _idmessage;
END $$

-- Retourne la liste des messages envoyés --
CREATE PROCEDURE getListeMessageEnvoye()
BEGIN
  SELECT * from messagesEnvoyes;
END $$

-- Retourne la liste des contrats --
CREATE PROCEDURE getListeContrat()
BEGIN
  SELECT cont.idcontrat, cont.datedebut, 
		 cont.datefin, cont.prixloyer, 
		 loc.mail as locMail, 
		 pro.mail as proMail 
		 from contrat as cont, utilisateur loc, utilisateur pro
		 WHERE cont.idproprietaire = pro.idutilisateur
		 AND cont.idlocataire = loc.idutilisateur;
END $$

-- Supprime un message envoyé par son id --
CREATE PROCEDURE deleteMessageEnvoye(IN _idmessage Int)
BEGIN
  delete from messagesEnvoyes where idmessage =_idmessage;
END $$

-- Retourne le nbr des proprietaires --
CREATE PROCEDURE countProprietaires()
BEGIN
  SELECT count(idutilisateur) from proprietaire;
END $$

-- Retourne le nbr des locataires --
CREATE PROCEDURE countLocataires()
BEGIN
  SELECT count(idutilisateur) from locataire;
END $$

-- Retourne le nbr des biens --
CREATE PROCEDURE countBiens()
BEGIN
  SELECT count(idbien) from bien;
END $$

-- Retourne le nbr des demandes de location --
CREATE PROCEDURE countDemandesLocation()
BEGIN
  SELECT count(iddemandelocation) from demandelocation;
END $$

-- Retourne le nbr de locations --
CREATE PROCEDURE countLocations()
BEGIN
  SELECT count(idlocation) from location;
END $$

-- Retourne le nbr des messages --
CREATE PROCEDURE countMessages()
BEGIN
  SELECT count(idmessage) from messagesRecus;
END $$

-- Retourne le nbr des contrats --
CREATE PROCEDURE countContrats()
BEGIN
  SELECT count(idcontrat) from contrat;
END $$

-- Retourne le nbr des localisation --
CREATE PROCEDURE countLocalisation()
BEGIN
  SELECT count(id) from localisation where location_status = 0;
END $$

-- Retourne un graphe --
CREATE PROCEDURE graphisme()
BEGIN
  SELECT * FROM villeDeParis;
END $$

-- Retourne le mdp de l'utilisateur --
CREATE PROCEDURE envoiEmail(in _email varchar(50) )
BEGIN
  SELECT mdp from utilisateur where mail = _email;
END $$

DELIMITER ;

-- FIN ROCEDURE Admin --

-- ------------------------------------------------------------------ --

-- ROCEDURE PROPRIETAIRE --

DELIMITER $$

-- Retourne un proprietaire par son id --
CREATE PROCEDURE getProprietaire(IN _idProprietaire Int)
BEGIN
  SELECT * from utilisateur u, proprietaire p
  WHERE 1=1
  AND u.idutilisateur = _idProprietaire
  AND p.idutilisateur = _idProprietaire;
END $$

-- Retourne la liste des biens par proprietaire --
CREATE PROCEDURE getAllBienByProprietaire(IN _idProprietaire INT) 
BEGIN
  SELECT * FROM bien
  where idproprietaire = _idProprietaire ;
END $$

-- Retourne la liste des locations par proprietaire --
CREATE PROCEDURE getAllLocationByProprietaire(IN _idProprietaire INT) 
BEGIN
  SELECT loc.*, mot.descriptionmotif, b.montantLoyer AS prixloyer 
			FROM location loc, motiflocation mot, bien b 
			where b.idproprietaire = _idProprietaire 
			AND b.idbien = loc.idbien
			AND loc.idmotiflocation = mot.idmotiflocation; 
END $$

-- Retourne la liste des demandes de locations par proprietaire --
CREATE PROCEDURE getAllDemandeLocationByProprietaire(IN _idProprietaire INT) 
BEGIN
  SELECT  * FROM demandelocation d, motiflocation m, bien b
			WHERE b.idproprietaire = _idProprietaire 
			AND d.idmotiflocation = m.idmotiflocation
			AND b.idbien = d.idbien; 
END $$

-- Retourne la liste des contrats par proprietaire --
CREATE PROCEDURE getAllContratByProprietaire(IN _idProprietaire INT) 
BEGIN
		SELECT * FROM contrat
		WHERE idproprietaire = _idProprietaire ;
END $$

-- Modifie un proprietaire --
CREATE PROCEDURE modifyProprietaire(
	IN _idutilisateur INT (5),
	IN _civiliteproprietaire varchar (10),
	IN _nom varchar  (50), 
	IN _prenom varchar  (50), 
	IN _datenaissance date, 
	IN _adresse varchar  (100), 
	IN _numtel varchar (10), 
	IN _mail varchar  (50),
	IN _mdp varchar (10),
	IN _assurance varchar (50),
	IN _ccp varchar (20)
)
BEGIN
	UPDATE utilisateur SET  civilite = _civiliteproprietaire,
	nom = _nom,
	prenom = _prenom,
	datenaissance = _datenaissance,
	adresse = _adresse,
	numtel =_numtel,
	mail = _mail,
	mdp = _mdp
	WHERE idutilisateur = _idutilisateur;
	
	UPDATE proprietaire
	SET assurance = _assurance,
		ccp = _ccp	
	WHERE idutilisateur = _idutilisateur;
  
END $$

-- Ajoute un bien -- 
CREATE PROCEDURE insertBien( 
	IN _idproprietaire int (5),
	IN _idtypebien varchar (3), 
	IN _titlebien varchar (50),
	IN _adresse varchar (50), 
	IN _codePostale varchar(50), 
	IN _nomquartier varchar(50),  
	IN _nomville varchar(50),
	IN _superficie float(50), 
	IN _nbpiece int  (50),
	IN _nbchambre int (50),
	IN _etagebien int  (50), 
	IN _descriptionbien varchar(50), 
	IN _chargeforfaitaire float(50),
	IN _montantLoyer float(50),
	IN _image varchar (50)
) 
BEGIN
	INSERT INTO bien VALUES (
		null,
		_idproprietaire,
		_idtypebien,
		_titlebien,
		_adresse,
		_codePostale,
		_nomquartier,
		_nomville,
		_superficie,
		_nbpiece,
		_nbchambre,
		_etagebien,
		_descriptionbien,
		_montantLoyer,
		_image
	);
	
	IF _chargeforfaitaire IS NOT NULL
	THEN
		INSERT INTO bienAvecCharges(idbien, chargeforfaitaire) 
		VALUES (LAST_INSERT_ID(), _chargeforfaitaire);
	END IF;
END $$

-- Modifie un bien --
CREATE PROCEDURE modifyBien(
	IN _idbien INT(5),
	IN _idtypebien  varchar(3),
	IN _titlebien varchar (50),
	IN _adresse varchar  (100), 
	IN _codePostale varchar(25), 
	IN _nomquartier varchar(50), 
	IN _nomville varchar  (50), 
	IN _superficie float (50), 
	IN _nbpiece int  (5),
	IN _nbchambre int (5),
	IN _etagebien int  (5), 
	IN _descriptionbien varchar (500), 
	IN _chargeforfaitaire float (50),
	IN _montantLoyer float (50),
	IN _image varchar(100)
)
BEGIN
	UPDATE bien 
	SET idtypebien = _idtypebien,
		titlebien = _titlebien,
		adresse = _adresse,
		codePostale =_codePostale,
		nomquartier = _nomquartier,
		nomville = _nomville,
		superficie = _superficie,
		nbpiece = _nbpiece,
		nbchambre = _nbchambre,
		etagebien = _etagebien,
		descriptionbien = _descriptionbien,
		montantLoyer = _montantLoyer
	WHERE idbien = _idbien ;

	IF _image <> '' 
	THEN
		UPDATE bien 
		SET image = _image 
		WHERE idbien = _idbien;
	END IF;
		
	UPDATE bienAvecCharges
	SET chargeforfaitaire = _chargeforfaitaire
	WHERE idbien = _idbien;
END $$

-- Ajoute un contrat --
CREATE PROCEDURE insertContrat( 
	IN _idproprietaire int (5),
	IN _idlocataire int (5),
	IN _idbien int (5),  
	IN _prixloyer varchar (50), 
	IN _datedebut date, 
	IN _datefin date 
) 
BEGIN
INSERT INTO contrat VALUES (
                    null,
                    _prixloyer,
                    _datedebut,
                    _datefin,
                    _idbien,
                    _idproprietaire,
                    _idlocataire
                    );
END $$

-- Ajoute un quartier --
CREATE PROCEDURE insertQuartier(IN _nomquartier varchar(125)) 
BEGIN
INSERT INTO quartier VALUES (
                    null,
                    _nomquartier
                    );
END $$

-- Ajoute une ville --
CREATE PROCEDURE insertVille(IN nomville varchar(50), IN codePostale int (5)) 
BEGIN
INSERT INTO ville VALUES (
                    null,
                    _nomville,
                    _codePostale
                    );
END $$

-- Valide une demande de location par son id --
CREATE PROCEDURE validerDemandeLocation(IN _idDemandeLocation INT) 
BEGIN
INSERT INTO location(datedebut, datefin, idbien, idlocataire, idmotiflocation) 
        SELECT
            d.datedebut,
            d.datefin,
            d.idbien,
            d.idlocataire,
            d.idmotiflocation
        FROM demandeLocation d
        WHERE d.idDemandeLocation = _idDemandeLocation;
END $$


-- Retourne le nbr de biens par proprietaire --
CREATE PROCEDURE countBiensByProprietaire(IN _idProprietaire INT) 
BEGIN
SELECT COUNT(idbien) AS nb FROM bien WHERE idproprietaire = _idProprietaire ;
END $$

-- Retourne le nbr de locations par proprietaire --
CREATE PROCEDURE countLocationsByProprietaire(IN _idProprietaire INT) 
BEGIN
	SELECT COUNT(idlocation) AS nb 
	FROM location l, bien b
	WHERE b.idproprietaire = _idProprietaire
	AND l.idbien = b.idbien;
END $$

-- Retourne le nbr de demandes de location par proprietaire --
CREATE PROCEDURE countDemandesLocationByProprietaire(IN _idProprietaire INT) 
BEGIN
SELECT COUNT(iddemandelocation) as nb 
                FROM demandelocation d,
                bien b
                WHERE b.idproprietaire = _idProprietaire 
                AND b.idbien = d.idbien;
END $$

-- Retourne le nbr de contrats par proprietaire --
CREATE PROCEDURE countAllContratByProprietaire(IN _idProprietaire INT) 
BEGIN
SELECT COUNT(idcontrat) as nb 
				FROM contrat c, bien b
				WHERE c.idbien = b.idbien
				AND b.idproprietaire =_idProprietaire;
END $$

-- Supprime un bien par son id --
CREATE PROCEDURE deleteBien(IN _idBien Int)
BEGIN
  DELETE FROM bien WHERE idbien =_idBien;
END $$

-- Supprime une location par son id --
CREATE PROCEDURE deleteLocation(IN _idLocation Int)
BEGIN
  DELETE FROM location WHERE idlocation = _idLocation  ;
END $$

-- Supprime une demande de location -- 
CREATE PROCEDURE deleteDemandeLocation(IN _idDemandeLocation INT) 
BEGIN
DELETE FROM demandeLocation WHERE idDemandeLocation = _idDemandeLocation ;
END $$

-- Supprime une demande de location personnalisée -- 
CREATE PROCEDURE deleteDemandeLocationPersonnalisee(IN _iddemandelocationsansbien INT) 
BEGIN
DELETE FROM demandelocationsansbien WHERE iddemandelocationsansbien = _iddemandelocationsansbien ;
END $$


-- FIN ROCEDURE PROPRIETAIRE --

-- ----------------------------------------------------------------- --


-- ROCEDURE LOCATAIRE --

-- Retourne un locataire par son id --
CREATE PROCEDURE getLocataire(IN _idLocataire Int)
BEGIN
  SELECT * from utilisateur u, locataire l
  WHERE 1=1
  AND u.idutilisateur = _idLocataire
  AND l.idutilisateur = _idLocataire;
END $$

-- Modifie un locataire --
CREATE PROCEDURE modifyLocataire(
	IN _idutilisateur int(5),
	IN _civilitelocataire varchar (10),
	IN _nom varchar  (50), 
	IN _prenom varchar  (50), 
	IN _datenaissance date, 
	IN _adresse varchar  (100), 
	IN _numtel varchar (10), 
	IN _mail varchar  (50),
	IN _mdp varchar (10),
	IN _ccp varchar(20)
)
BEGIN
	UPDATE utilisateur SET  civilite = _civilitelocataire,
	nom = _nom,
	prenom = _prenom,
	datenaissance = _datenaissance,
	adresse = _adresse,
	numtel =_numtel,
	mail = _mail,
	mdp = _mdp
	WHERE idutilisateur = _idutilisateur;
	
	UPDATE locataire
	SET ccp = _ccp	
	WHERE idutilisateur = _idutilisateur;
  
END $$

-- Retourne la liste des biens --
CREATE PROCEDURE getAllBien() 
BEGIN
	SELECT b.idbien, b.idproprietaire, b.idtypebien, b.nomville,b.adresse, b.superficie, b.nbpiece, b.etagebien, b.descriptionbien, b.montantLoyer, ba.chargeforfaitaire
	FROM bien b
	LEFT JOIN bienAvecCharges ba ON ba.idbien = b.idbien;

END $$

-- Retourne la liste des biens --
CREATE PROCEDURE catalogBien() 
BEGIN
	SELECT b.*,ba.chargeforfaitaire
	FROM bien b
	LEFT JOIN bienAvecCharges ba ON ba.idbien = b.idbien;

END $$

-- Retourne un bien par son id --
CREATE PROCEDURE getBien(IN _idBien Int)
BEGIN
  SELECT * from bien 
  WHERE idbien = _idBien;
END $$

-- Retourne un bien avec ou sans charges par son id --
CREATE PROCEDURE getIdBien(IN _idBien Int)
BEGIN
	SELECT b.*,ba.chargeforfaitaire
	FROM bien b
	LEFT JOIN bienAvecCharges ba ON ba.idbien = b.idbien
    WHERE b.idbien = _idBien;

END $$

-- Retourne la liste des locations par locataire --
CREATE PROCEDURE getAllLocationByLocataire(IN _idlocataire INT) 
BEGIN
  SELECT l.*, b.montantLoyer AS prixloyer
            FROM location l, bien b  
            WHERE idlocataire = _idlocataire 
            AND l.idbien = b.idbien;
END $$

-- Retourne la liste des demandes de locations par locataire --
CREATE PROCEDURE getAllDemandeLocationByLocataire(IN _idlocataire INT) 
BEGIN
  SELECT  d.*, m.descriptionmotif  
			FROM demandelocation d, motiflocation m
			where idlocataire = _idlocataire 
			AND d.idmotiflocation = m.idmotiflocation ;
END $$

-- Retourne la liste des demandes de location sans bien appartenant à un locataire --
CREATE PROCEDURE getListeDemandeLocationSansBien2(in _idlocataire int) 
BEGIN
  SELECT * from demandelocationsansbien where idlocataire = _idlocataire;
END $$

-- Ajoute une location php & java --
CREATE PROCEDURE insertLocation( 
    IN _datedebut date, 
	IN _datefin date, 
    IN _idbien int (5),  
	IN _idlocataire int (5),
	IN _idmotiflocation int (5)
) 
BEGIN
INSERT INTO location VALUES (
                    null,
                    _datedebut,
                    _datefin,
                    _idbien,
                    _idlocataire,
                    _idmotiflocation
                    );
END $$

-- Modifie une location --
CREATE PROCEDURE modifyLocation(
    IN _idlocation int(5),  
    IN _datedebut date, 
	IN _datefin date, 
    IN _idbien int (5),  
	IN _idlocataire int (5),
	IN _idmotiflocation int (5)
)
BEGIN
	UPDATE location 
	SET datedebut = _datedebut,
		datefin = _datefin,
		idbien = _idbien,
		idlocataire =_idlocataire,
		idmotiflocation = _idmotiflocation
	WHERE idlocation = _idlocation ;

END $$

-- Ajoute une demande de location --
CREATE PROCEDURE insertDemandeLocation( 
	IN _datedemande date,
	IN _datedebut date, 
	IN _datefin date, 
	IN _idbien int (5),
	IN _idlocataire int (5),
	IN _idmotiflocation int (5)
 ) 

BEGIN
INSERT INTO demandelocation VALUES (
					null,
					_datedemande,
					_datedebut,
                    _datefin,
                    _idbien,
                    _idlocataire,
                    _idmotiflocation
                    );
END $$

-- Ajoute une demande de location sans bien --
CREATE PROCEDURE insertDemandeLocationSansBien( 
	IN _libelletype varchar (50),
	IN _nbpiece int (5),
	IN _descriptionmotif varchar(50),
	IN _superficie float(50), 
	IN _prixLoyer float(50),
	IN _datedebut date, 
	IN _nomville varchar(50),
	IN _idlocataire int (5)
 ) 

BEGIN
INSERT INTO demandelocationsansbien VALUES (
					null,
					_libelletype,
					_nbpiece,
          _descriptionmotif,
          _superficie,
          _prixLoyer,
          _datedebut,
					_nomville,
					_idlocataire
                    );
END $$

-- Retourne le nbr des demandes de location par locataire -- 
CREATE PROCEDURE countDemandesLocationByLocataire(IN _idlocataire INT)
BEGIN
  SELECT count(iddemandelocation)AS nb
		  FROM demandelocation
		  WHERE idLocataire = _idLocataire;
END $$

-- Retourne le nbr des contrats par locataire --
CREATE PROCEDURE countAllContratByLocataire(IN _idlocataire INT)
BEGIN
  SELECT count(idcontrat)AS nb
		  FROM contrat
		  WHERE idLocataire = _idLocataire;
END $$

-- Supprime un contrat par son id --
CREATE PROCEDURE deleteContrat(IN _idcontrat Int)
BEGIN
  delete from contrat where idcontrat =_idcontrat;
END $$


-- FIN ROCEDURE LOCATAIRE --

-- Procédures pour JAVA --

-- Insere un admin --
CREATE PROCEDURE insertAdmin( 
	IN _civiliteproprietaire varchar (50),
	IN _nom varchar  (50), 
	IN _prenom varchar  (50), 
	IN _datenaissance date, 
	IN _adresse varchar  (50), 
	IN _numtel varchar (50), 
	IN _mail varchar  (50),
	IN _mdp varchar (50)
)
BEGIN
  INSERT into utilisateur 
  values(
		 null, _civiliteproprietaire,_nom, _prenom, _datenaissance, _adresse, _numtel, _mail, _mdp 
  ) ;

	INSERT INTO admin(idutilisateur) 
	VALUES( LAST_INSERT_ID()) ;
END $$


-- Insere un proprietaire php & java --
CREATE PROCEDURE insertProprietaire( 
	IN _civiliteproprietaire varchar (50),
	IN _nom varchar  (50), 
	IN _prenom varchar  (50), 
	IN _datenaissance date, 
	IN _adresse varchar  (50), 
	IN _numtel varchar (50), 
	IN _mail varchar  (50),
	IN _mdp varchar (50),
	IN _assurance varchar (50),
	IN _ccp varchar (20)
)
BEGIN
  INSERT into utilisateur 
  values(
		 null, _civiliteproprietaire,_nom, _prenom, _datenaissance, _adresse, _numtel, _mail, _mdp 
  ) ;

	INSERT INTO proprietaire(idutilisateur, assurance, ccp) 
	VALUES( LAST_INSERT_ID(), _assurance, _ccp) ;

END $$


-- Insere un locataire php & java --
CREATE PROCEDURE insertLocataire( 
	IN _civilitelocataire varchar (50),
	IN _nom varchar  (50), 
	IN _prenom varchar  (50), 
	IN _datenaissance date, 
	IN _adresse varchar  (50), 
	IN _numtel varchar (50), 
	IN _mail varchar  (50),
	IN _mdp varchar (50),
	IN _ccp VARCHAR(50)
)
BEGIN

  INSERT into utilisateur 
  values(
		 null, _civilitelocataire,_nom, _prenom, _datenaissance, _adresse, _numtel, _mail, _mdp 
  );
	
	INSERT INTO locataire(idutilisateur, ccp) 
	VALUES (LAST_INSERT_ID(), _ccp );
	
END $$

-- Vérifie si un email est dispo --
CREATE PROCEDURE isEmailDispo(In _email Varchar(50))
BEGIN
  SELECT count(idutilisateur) as dispo from utilisateur 
  WHERE mail = _email;
END $$

-- Retourne un admin par son id --
CREATE PROCEDURE getAdmin(IN _mailAdmin VARCHAR (50), IN _mdpAdmin VARCHAR (50))
BEGIN
  SELECT * from utilisateur u
  WHERE u.mail = _mailAdmin
  and u.mdp = _mdpAdmin;
END $$

-- retourn tout les id proprietaires -- 
CREATE PROCEDURE getIdProprietaires() 
BEGIN
SELECT idutilisateur FROM proprietaire;
END $$

-- retourn tout les id locataires -- 
CREATE PROCEDURE getIdLocataires() 
BEGIN
SELECT idutilisateur FROM locataire;
END $$

-- retourn tout les id biens -- 
CREATE PROCEDURE getIdBiens() 
BEGIN
SELECT idbien FROM bien;
END $$

-- Retourne les id typebien --
CREATE PROCEDURE getIdTypebien()
BEGIN
  SELECT DISTINCT idTypebien from bien; 
END $$

-- retourn tout les id typelocation -- 
CREATE PROCEDURE getIdMotifs() 
BEGIN
SELECT idmotiflocation FROM motiflocation;
END $$

-- Retourne les id et les types de location --
CREATE PROCEDURE getAllMotifLocation() 
BEGIN
SELECT * FROM motiflocation;
END $$

-- Retourne les id et les noms des proprietaires --
CREATE PROCEDURE getIdNomProprio() 
BEGIN
SELECT u.idutilisateur ,u.nom from utilisateur u, proprietaire p where p.idutilisateur =u.idutilisateur;
END $$

DELIMITER ;

-- END PROCEDURE --

-- Vue du graphe --
CREATE VIEW villeDeParis AS (SELECT nomville, count(iddemandelocationsansbien) as nb_demande FROM demandelocationsansbien group by nomville) ;


-- TRIGGERS => Déclencheur -- 

DELIMITER $$

CREATE TRIGGER trig_msgTraites AFTER DELETE ON messagesRecus FOR EACH ROW	
begin
	insert into messagesTraites (datereception,	nom ,prenom ,sujet ,mail ,numtel ,textemessage )
	values (old.datereception, old.nom , old.prenom , old.sujet , old.mail , old.numtel , old.textemessage );
END $$

DELIMITER ;

-- FIN TRIGGERS --


-- TESTS --

/*plaza*/
drop database if exists agence_plaza_v2 ;

create database agence_plaza_v2 ;

use agence_plaza_v2 ;

DROP TABLE IF EXISTS ville;
create table ville (
	idville int(5) not null auto_increment ,
	nomville varchar(50),
	codePostale int (5),
	primary key (idville)
) Engine=InnoDB;


DROP TABLE IF EXISTS quartier;
create table quartier (
	idquartier int(5) not null auto_increment ,
	nomquartier varchar(125),
	primary key (idquartier)
) Engine=InnoDB;


DROP TABLE IF EXISTS messagesRecus;
create table messagesRecus (
	idmessage int(5) not null auto_increment ,
	datereception DATETIME DEFAULT NOW(),
	nom varchar(50),
	prenom varchar(50),
	sujet varchar (50),
	mail varchar (50),
	numtel varchar (10),
	textemessage text(100),
	primary key (idmessage)
) Engine=InnoDB;

DROP TABLE IF EXISTS messagesTraites;
create table messagesTraites (
	idmessage int(5) not null auto_increment ,
	datetraitement DATETIME DEFAULT NOW(),
	datereception DATETIME DEFAULT NOW(),
	nom varchar(50),
	prenom varchar(50),
	sujet varchar (50),
	mail varchar (50),
	numtel varchar (10),
	textemessage text(100),
	primary key (idmessage)
) Engine=InnoDB;

DROP TABLE IF EXISTS messagesEnvoyes;
create table messagesEnvoyes (
	idmessage int(5) not null auto_increment ,
	datereception DATETIME DEFAULT NOW() NOT NULL,
	datereponse DATETIME DEFAULT NOW() NOT NULL,
	sujet varchar (50),
	mail varchar (50),
	textemessage text(100),
	primary key (idmessage)
) Engine=InnoDB;

DROP TABLE IF EXISTS motiflocation;
create table motiflocation (
	idmotiflocation int(5) not null,
	descriptionmotif varchar (100),
	primary key (idmotiflocation)
) Engine=InnoDB;

DROP TABLE IF EXISTS typebien;
create table typebien (
	idtypebien varchar(3) not null,
	libelletype varchar (50),
	primary key (idtypebien)
) Engine=InnoDB;

DROP TABLE IF EXISTS utilisateur;
create table utilisateur(
	idutilisateur int(5) not null auto_increment ,
	civilite varchar(5),
	nom varchar(50),
	prenom varchar(50),
	datenaissance date,
	adresse varchar(100),
	numtel varchar (10),
	mail varchar (50) not null,
	mdp varchar(10),
	primary key(idutilisateur),
	unique(mail)
) Engine=InnoDB;

DROP TABLE IF EXISTS admin;
create table admin(
	idutilisateur int(5) ,
	primary key(idutilisateur),
	unique(idutilisateur),
	foreign key (idutilisateur) references utilisateur (idutilisateur)
	ON DELETE CASCADE
)Engine=InnoDB;

DROP TABLE IF EXISTS proprietaire;
create table proprietaire (
	idutilisateur int(5) ,
	assurance varchar(50),
	ccp varchar(20),
    statut boolean,
	primary key(idutilisateur),
	unique(idutilisateur),
	foreign key (idutilisateur) references utilisateur (idutilisateur) ON DELETE CASCADE
) Engine=InnoDB;

DROP TABLE IF EXISTS locataire;
create table locataire (
	idutilisateur int(5),
	ccp varchar(20),
	primary key(idutilisateur),
	unique(idutilisateur),
	foreign key (idutilisateur) references utilisateur (idutilisateur)
	ON DELETE CASCADE
) Engine=InnoDB;

DROP TABLE IF EXISTS bien;
create table bien (
	idbien int(5) not null auto_increment ,
	idproprietaire int(5),
	idtypebien varchar(3),
	titlebien varchar(50),
	adresse varchar(100),
	codePostale varchar(25),
	nomquartier varchar(50),
	nomville varchar(50) not null,
	superficie float ,
	nbpiece int(5),
	nbchambre int(5),
	etagebien int(5),
	descriptionbien VARCHAR(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci,
	montantLoyer float,
	image varchar(100),
	primary key (idbien)
	,foreign key (idproprietaire) references proprietaire (idutilisateur) ON DELETE CASCADE
	,foreign key (idtypebien) references typebien (idtypebien) ON DELETE CASCADE
) Engine=InnoDB ;

DROP TABLE IF EXISTS bienSansCharges;
create table bienSansCharges (
	idbien int(5),
	primary key(idbien),
	unique(idbien),
	foreign key (idbien) references bien(idbien)
	ON DELETE CASCADE
) Engine=InnoDB;

DROP TABLE IF EXISTS bienAvecCharges;
create table bienAvecCharges (
	idbien int(5),
	chargeforfaitaire float,
	primary key(idbien),
	unique(idbien),
	foreign key (idbien) references bien(idbien)
	ON DELETE CASCADE
) Engine=InnoDB;

DROP TABLE IF EXISTS location;
create table location (
	idlocation int(5) not null auto_increment ,
	datedebut date,
	datefin date,
	idbien int(5) not null,
	idlocataire int(5) not null,
	idmotiflocation int(5) not null,
	primary key (idlocation)
	,foreign key (idmotiflocation) references motiflocation (idmotiflocation) ON DELETE CASCADE
	,foreign key (idbien) references bien (idbien) ON DELETE CASCADE
) Engine=InnoDB;

DROP TABLE IF EXISTS demandelocation;
create table demandelocation (
 	iddemandelocation int(5) not null auto_increment ,
	datedemande date,
	datedebut date,
	datefin date,
	idbien int(5) not null,
	idlocataire int(5) not null,
	idmotiflocation int(5) not null,
	primary key(iddemandelocation)
	,foreign key (idbien) references bien(idbien) ON DELETE CASCADE
	,foreign key (idlocataire) references locataire(idutilisateur) ON DELETE CASCADE
	,foreign key (idmotiflocation) references motiflocation (idmotiflocation) ON DELETE CASCADE
) Engine=InnoDB;

DROP TABLE IF EXISTS demandelocationsansbien;
create table demandelocationsansbien (
  iddemandelocationsansbien int(5) not null auto_increment ,
	libelletype varchar(50),
	nbpiece int(5),
	descriptionmotif varchar(100),
	superficie float,
	prixloyer float,
	datedebut date,
	nomville varchar(50),
	idlocataire int(5) not null,
	primary key(iddemandelocationsansbien),
	foreign key (idlocataire) references locataire (idutilisateur) ON DELETE CASCADE
) Engine=InnoDB;

DROP TABLE IF EXISTS contrat;
create table contrat (
	idcontrat int(5) not null auto_increment ,
	datedebut date,
	datefin date,
	prixloyer float,
	idlocataire int(5) not null,
	idproprietaire int(5) not null,
	idbien int(5) not null,
	idlocation int(5) not null,
	primary key (idcontrat)
	, foreign key (idlocataire) references locataire (idutilisateur) ON DELETE CASCADE
	, foreign key (idproprietaire) references proprietaire (idutilisateur)  ON DELETE CASCADE
	, foreign key (idbien) references bien (idbien)  ON DELETE CASCADE
	, foreign key (idlocation) references location (idlocation)  ON DELETE CASCADE
) Engine=InnoDB;

DROP TABLE IF EXISTS localisation;
create table localisation(
  `id` int(11) not null,
  `lat` float(10,6) not null,
  `lng` float(10,6) not null,
  `description` varchar(500) not null,
  `location_status` tinyint(1) default '0'
) ENGINE=InnoDB default CHARSET=utf8;

set SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
set time_zone = "+00:00";
alter table localisation
  add primary key (`id`);
alter table `localisation`
  modify `id` int(11) not null AUTO_INCREMENT;


use agence_plaza_v2  ;

insert into ville values (1, "Paris", 75000) ;

insert into quartier values (11, "Archives") ;
insert into quartier values (20, "Sorbonne") ;
insert into quartier values (62, "Muette") ;

insert into typebien values ("S", "studio") ;
insert into typebien values ("A", "appartement") ;
insert into typebien values ("M", "maison") ;

insert into motiflocation values (50, "location etudiant") ;
insert into motiflocation values (51, "location saisoniere") ;
insert into motiflocation values (52, "indifferent") ;


insert into utilisateur values (1,"Mme", "Fantazi", "Ahlem", "1988-03-11","rue de Paris", "0695424344","fantaziahlem@gmail.com", "123");
insert into admin values (1);

insert into utilisateur values (2,"Mme","Lau", "Catherine","1988-03-11","rue de Paris", "0695424344","LC@gmail.com", "123");
insert into admin values (2);

insert into utilisateur values (10, "Mr", "Laurens", "Lucas", "1988-03-11","rue de Paris", "0695424344", "ll@gmail.com", "111");
insert into proprietaire values (10, "T889912", "897", "1");

insert into utilisateur values (11, "Mme", "Delacroix", "Veronique", "1944-01-12","rue de Lyon", "0695450286", "dv@yahoo.com", "222");
insert into proprietaire values (11, "B889911", "899", "1");

insert into utilisateur values (12, "Mr", "Jacob", "Jaques", "1975-05-19","rue de Grenoble", "0695337794", "jf@live.com", "333");
insert into proprietaire values (12, "Z889910", "888", "1");

insert into utilisateur values (20, "Mme", "Paix", "Clemence", "1988-09-15","rue de Chateau", "0695529384", "pc@gmail.com", "111");
insert into locataire values (20, "BIC_20");

insert into utilisateur values (21, "Mr", "Fanta", "Jad", "1990-08-29","rue de Versailles", "0695484314", "fj@orange.com", "222");
insert into locataire values (21, "BIC_21");

insert into utilisateur values (22, "Mr", "Fanta", "Ryme", "1990-08-29","rue de Versailles", "0695484314", "fr@orange.com", "222");
insert into locataire values (22, "BIC_21");

insert into utilisateur values (23, "Mme", "Chantier", "Pauline", "1988-04-30","rue de Lille", "0695412399", "cp@outlook.com", "333");
insert into locataire values (23, "BIC_22");

insert into utilisateur values (24, "Mme", "Chantier", "Juliette", "1988-04-30","rue de Nantes", "0295412399", "cj@outlook.com", "444");
insert into locataire values (24, "BIC_23");

insert into bien values (30, 10, "A", "Appartement meublé 40m²", "7 rue de Paix",75000,"Sorbonne","Paris", 40 ,2,1,2,"Appartement meublé entièrement refait à neuf", 700, "bien1.jpg") ;
insert into bien values (31, 10, "A", "Appartement 90m²", "7 rue de Croix",75000,"Buttes Chaumont","Paris", 90 ,3,2,2,"Appartement lumineux dans un immeuble style haussmannien proche de toutes comodités",1000, "bien2.jpg") ;
insert into bien values (32, 11, "A", "Appartement 50m²","7 rue de Calme",75000,"Montparnasse","Paris",50,3,2,4,"Appartement neuf, spacieux, lumineux et agréable", 900, "bien3.jpg") ;
insert into bien values (33, 11, "M", "Maison 130m²", "7 rue de Joie",75000,"Montmartre", "Paris", 130, 4,3,0,"Maison spacieuse avec jardin, située dans un quartier calme et agréable proche de toutes comodités",1200, "bien4.jpg") ;
insert into bien values (34, 12, "S", "Studio 20m²", "10 Rue du Faubourg Poissonnière",75000,"Grands Boulevards","Paris", 20 ,1,0,2,"Studio bien agencé idéal pour une location étudiante", 600, "bien5.jpg") ;
insert into bien values (35, 12, "M", "Maison 120 m²", "100 Boulevard Sebastopol",75000,"Batignolles","Paris", 120 ,4,3,0, "Maison spacieuse et équipée avec divers objets high tech", 1200 , "bien6.jpg") ;
insert into bien values (36, 10, "S", "Studio 60m²", "10 Rue de la victoire",75000,"Grands Boulevards","Paris", 20 ,1,0,2,"Studio lumineux agencé idéal pour une location étudiante", 600, "bien9.jpg") ;
insert into bien values (37, 11, "M", "Maison 140 m²", "100 Boulevard Aqua",75000,"Batignolles","Paris", 320 ,4,3,0, "Maison moderne équipée avec divers objets high tech", 2000 , "bien_10.jpg") ;

insert into bienAvecCharges values (30, 51.3);
insert into bienAvecCharges values (31, 41.3);
insert into bienAvecCharges values (32, 31.3);

insert into bienSansCharges values (33);
insert into bienSansCharges values (34);
insert into bienSansCharges values (35);
insert into bienSansCharges values (36);
insert into bienSansCharges values (37);

insert into location values (40, "2019-01-01", "2018-12-30", 30, 20, 50) ;
insert into location values (41, "2019-02-01", "2019-12-31", 32, 21, 51) ;
insert into location values (42, "2017-01-10", "2018-12-31", 34, 22, 50) ;
insert into location values (43, "2014-01-03", "2020-04-30", 35, 23, 51) ;
insert into location values (44, "2014-01-03", "2020-04-30", 36, 23, 51) ;
insert into location values (45, "2014-01-03", "2020-04-30", 37, 23, 51) ;

/* insert into location values (null, "2018-06-12", "2019-08-11", 34, 22, 51) ; */

insert into demandelocation values (50, "2018-06-12", "2018-06-12", "2019-06-12", 31, 21, 50 );
insert into demandelocation values (51, "2018-08-12", "2018-10-15", "2020-10-14", 34, 23, 51 );

insert into demandelocationsansbien values (60,"Appartement",2,"location saisoniere",40, 700, "2018-07-12", "Paris", 20 );
insert into demandelocationsansbien values (61,"Studio",1,"location etudiant",35, 1000, "2019-09-10", "Paris", 21 );
insert into demandelocationsansbien values (62,"Studio",1,"indifferent",35, 1000, "2019-09-10", "Paris", 22 );
insert into demandelocationsansbien values (63,"Maison",3,"location saisoniere",35, 500, "2019-10-11", "15e Arrondissement", 23 );
insert into demandelocationsansbien values (64,"Appartement",2,"location etudiant",35, 800, "2019-11-12", "15e Arrondissement", 24 );
insert into demandelocationsansbien values (65,"Studio",1,"location saisoniere",35, 500, "2019-12-13", "18e Arrondissement", 20 );
insert into demandelocationsansbien values (67,"Appartement",2,"location etudiant",35, 900, "2019-09-14", "18e Arrondissement", 21 );
insert into demandelocationsansbien values (68,"Studio",1,"location saisoniere",35, 500, "2020-01-15", "2e Arrondissement", 22 );
insert into demandelocationsansbien values (69,"Studio",1,"location etudiant",35, 400, "2020-02-16", "2e Arrondissement", 23 );
insert into demandelocationsansbien values (70,"Appartement",2,"location saisoniere",35, 600, "2020-02-16", "5e Arrondissement", 24 );
insert into demandelocationsansbien values (71,"Studio",1,"indifferent",35, 500, "2020-02-16", "5e Arrondissement", 20 );



insert into messagesRecus (nom, prenom, sujet, mail, numtel, textemessage)
values
	("Diaz", "Cameron", "renseignement", "dc@gail.com","0695768943" ,"Hello admin, j'ai une question pour vous!"),
	("Fabien", "Alfred","demande","fa@gail.com","0698768946" ,"Hello, pouvez-vous m'informer sur la procedure a suivre pour louer un appartement? ")
;

insert into contrat values (70, "2018-06-12", "2019-06-12", 700, 20, 10, 34, 40) ;
insert into contrat values (71, "2018-06-12", "2019-08-11", 600, 22, 11, 30, 41) ;

insert into localisation values (Null, 48.844862, 2.354015 , "21 rue Las Cases" ,1);
insert into localisation values (Null, 48.888159, 2.334319 , "5 Passage de la Sorcière" ,1);
insert into localisation values (Null, 48.881441, 2.341084, "134 Rue de Courcelles" ,1);
insert into localisation values (Null, 48.845953, 2.343915 , "6 Avenue Mozart" ,1);

<?php

class UserService {
    private function __construct() {}

    public static function saveUserInfo($userInfo) {
        $dureeValiditeCookie = time() + 60 * 60 * 24;
        foreach($userInfo as $key => $value) {
            self::storeCookie($key, $value, $dureeValiditeCookie);
        }
    }

    public static function updateUser($nom, $prenom) {
        self::saveUserInfo(array("user_nom" => $nom, "user_prenom" => $prenom));
    }

    private static function storeCookie ($key, $value, $dureeValiditeCookie) {
        setcookie($key, $value, $dureeValiditeCookie , '/');
        $_COOKIE[$key] = $value;
    }   
}

?>
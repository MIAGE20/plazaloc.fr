drop database if exists agence_plaza ;
create database agence_plaza CHARACTER SET utf8 COLLATE utf8_general_ci;

use agence_plaza ;

create table admin(
	idadmin int(5) not null auto_increment ,
	nom varchar(50),
	prenom varchar(50),
	mail varchar (50),
	mdp varchar(10),
	primary key(idadmin),
	unique(mail)
	);

create table proprietaire (
	idproprietaire int(5) not null auto_increment ,
	civiliteproprietaire varchar(10),
	nom varchar(50),
	prenom varchar(50),
	datenaissance date,
	adresse varchar(100),
	numtel varchar (10),
	mail varchar (50),
	mdp varchar(10),
	primary key(idproprietaire),
	unique(mail)
);

create table locataire (
	idlocataire int(5) not null auto_increment ,
	civilitelocataire varchar(10),
	nom varchar(50),
	prenom varchar(50),
	datenaissance date,
	adresse varchar(100),
	numtel varchar (10),
	mail varchar (50),
	mdp varchar(10),
	primary key(idlocataire),
	unique(mail)
);

create table bien (
	idbien int(5) not null auto_increment ,
	idproprietaire int(5),
	idtypebien char(3),
	titlebien varchar(50),
	adresse varchar(100),
	codePostale varchar(25),
	nomquartier varchar(50),
	nomville varchar(50) not null,
	superficie float ,
	nbpiece int(5),
	nbchambre int(5),
	etagebien int(5),
	descriptionbien varchar(500),
	chargeforfaitaire float ,
	montantLoyer float,
	image varchar(100),
	primary key (idbien),
	foreign key (idproprietaire) references proprietaire (idproprietaire) ON DELETE CASCADE,
	foreign key (idtypebien) references typebien (idtypebien) ON DELETE CASCADE
);

create table location (
	idlocation int(5) not null auto_increment ,
	datedebut date,
	datefin date,
	idbien int(5) not null,
	idlocataire int(5) not null,
	idmotiflocation int(5) not null,
	primary key (idlocation),
	foreign key (idmotiflocation) references motiflocation (idmotiflocation) ON DELETE CASCADE,
	foreign key (idbien) references bien (idbien) ON DELETE CASCADE
);

create table demandelocation (
 	iddemandelocation int(5) not null auto_increment ,
	datedemande date,
	datedebut date,
	datefin date,
	idbien int(5) not null,
	idlocataire int(5) not null,
	idmotiflocation int(5) not null,
	primary key(iddemandelocation),
	foreign key (idbien) references bien(idbien) ON DELETE CASCADE,
	foreign key (idlocataire) references locataire(idlocataire) ON DELETE CASCADE,
	foreign key (idmotiflocation) references motiflocation (idmotiflocation) ON DELETE CASCADE
);

create table demandelocationsansbien (
    iddemandelocationsansbien int(5) not null auto_increment ,
	libelletype varchar(50),
	nbpiece int(5),
	descriptionmotif varchar(100),
	superficie float,
	prixloyer float,
	datedebut date,
	nomville varchar(50),
	idlocataire int(5) not null,
	primary key(iddemandelocationsansbien),
	foreign key (idlocataire) references locataire (idlocataire) ON DELETE CASCADE
);


create table motiflocation (
	idmotiflocation int(5) not null,
	descriptionmotif varchar (100),
	primary key (idmotiflocation)
	);

create table typebien (
	idtypebien varchar(3) not null,
	libelletype varchar (50),
	primary key (idtypebien)
	);

create table quartier (
	idquartier int(5) not null auto_increment ,
	nomquartier varchar(125),
	primary key (idquartier)
);

create table ville (
	idville int(5) not null auto_increment ,
	nomville varchar(50),
	codePostale int (5),
	primary key (idville)
);

create table contrat (
	idcontrat int(5) not null auto_increment ,
	datedebut date,
	datefin date,
	prixloyer float,
	idlocataire int(5) not null,
	idproprietaire int(5) not null,
	idbien int(5) not null,
	idlocation int(5) not null,
	primary key (idcontrat),
	foreign key (idlocataire) references locataire (idlocataire) ON DELETE CASCADE,
	foreign key (idproprietaire) references proprietaire (idproprietaire)  ON DELETE CASCADE,
	foreign key (idbien) references bien (idbien)  ON DELETE CASCADE,
	foreign key (idlocation) references location (idlocation)  ON DELETE CASCADE
);

create table messages (
	idmessage int(5) not null auto_increment ,
	nom varchar(50),
	prenom varchar(50),
	sujet varchar (50),
	mail varchar (50),
	numtel varchar (10),
	textemessage text(100),
	primary key (idmessage)
);

create table messagesEnvoyes (
	idmessage int(5) not null auto_increment ,
	sujet varchar (50),
	mail varchar (50),
	textemessage text(100),
	primary key (idmessage)
);


insert into admin values (1, "Fantazi", "Ahlem","FA", "123");
insert into admin values (2, "Lau", "Catherine","LC", "123");

insert into proprietaire values (10, "Mr", "Laurens", "Lucas", "1988-03-11","rue de Paris", "0695424344", "ll@gmail.com", "111");
insert into proprietaire values (11, "Mme", "Delacroix", "Veronique", "1944-01-12","rue de Lyon", "0695450286", "dv@yahoo.com", "222");
insert into proprietaire values (12, "Mr", "Jacob", "Jaques", "1975-05-19","rue de Grenoble", "0695337794", "jf@live.com", "333");

insert into locataire values (20, "Mme", "Paix", "Clemence", "1988-09-15","rue de Chateau", "0695529384", "pc@gmail.com", "111");
insert into locataire values (21, "Mr", "Fanta", "Jad", "1990-08-29","rue de Versailles", "0695484314", "fj@orange.com", "222");
insert into locataire values (22, "Mme", "Chantier", "Pauline", "1988-04-30","rue de Lille", "0695412399", "cp@outlook.com", "333");
insert into locataire values (23, "Mme", "Chantier", "Juliette", "1988-04-30","rue de Nantes", "0295412399", "cj@outlook.com", "444");

insert into bien values (30, 10, "A", "Appartement meublé 40m²", "7 rue de Paix",75000,"Sorbonne","Paris", 40 ,2,1,2,"Appartement meublé entièrement refait à neuf", 50.8, 700, "bien1.jpg") ;
insert into bien values (31, 10, "B", "Appartement 90m²", "7 rue de Croix",75000,"Buttes Chaumont","Paris", 90 ,3,2,2,"Appartement lumineux dans un immeuble style haussmannien proche de toutes comodités", 55.6, 1000, "bien2.jpg") ;
insert into bien values (32, 11, "B", "Appartement 50m²","7 rue de Calme",75000,"Montparnasse","Paris",50,3,2,4,"Appartement neuf, spacieux, lumineux et agréable", 100.70, 900, "bien3.jpg") ;
insert into bien values (33, 11, "C", "Maison 130m²", "7 rue de Joie",75000,"Montmartre", "Paris", 130, 4,3,0,"Maison spacieuse avec jardin, située dans un quartier calme et agréable proche de toutes comodités", 70.99, 1200, "bien4.jpg") ;
insert into bien values (34, 12, "A", "Studio 20m²", "10 Rue du Faubourg Poissonnière",75000,"Grands Boulevards","Paris", 20 ,1,0,2,"Studio bien agencé idéal pour une location d'étudiant", 51.3, 600, "bien5.jpg") ;
insert into bien values (35, 12, "C", "Maison 120 m²", "100 Boulevard Sebastopol",75000,"Batignolles","Paris", 120 ,4,3,0,"Maison spacieuse et équipée avec divers objets high tech", 90.8, 1100, "bien6.jpg") ;

insert into typebien values ("A", "studio") ;
insert into typebien values ("B", "appartement") ;
insert into typebien values ("C", "maison") ;

insert into quartier values (11, "Archives") ;
insert into quartier values (20, "Sorbonne") ;
insert into quartier values (62, "Muette") ;

insert into ville values (1, "Paris", 75000) ;

insert into location values (40, "2018-06-12", "2019-06-12", 30, 20, 50 ) ;
insert into location values (41, "2018-06-12", "2019-08-11", 34, 22, 51) ;

insert into demandelocation values (null, "2018-06-12", "2018-06-12", "2019-06-12", 31, 21, 50 );
insert into demandelocation values (null, "2018-08-12", "2018-10-15", "2020-10-14", 34, 23, 51 );

insert into demandelocationsansbien values (null,"Appartement",2,"Appartement entierement refait a neuf",40, 700, "2018-07-12", "Paris", 20 );
insert into demandelocationsansbien values (null,"studio",1,"studio cozy",35, 500, "2018-09-12", "Paris", 20 );

insert into motiflocation values (50, "location etudiant") ;
insert into motiflocation values (51, "location saisoniere") ;
insert into motiflocation values (52, "indifferent") ;

insert into messages values (null, "Diaz", "Cameron", "renseignement", "dc@gail.com","0695768943" ,"Hello admin, j'ai une question pour vous!") ;
insert into messages values (null, "Fabien", "Alfred","demande","fa@gail.com","0698768946" ,"Hello, pouvez-vous m'informer sur la procedure a suivre pour louer un appartement? ") ;


insert into contrat values (70, "2018-06-12", "2019-06-12", 700, 20, 10, 34, 40) ;
insert into contrat values (71, "2018-06-12", "2019-08-11", 600, 22, 11, 30, 41) ;



insert into bien values (36, 12, "A", "Appartement meublé 40m²", "7 rue de Paix",75000,"Sorbonne","Paris", 40 ,2,1,2,"Appartement meublé entièrement refait à neuf", 50.8, 700, "bien1.jpg") ;
insert into bien values (37, 13, "B", "Appartement 90m²", "7 rue de Croix",75000,"Buttes Chaumont","Paris", 90 ,3,2,2,"Appartement lumineux dans un immeuble style haussmannien proche de toutes comodités", 55.6, 1000, "bien2.jpg") ;

CREATE TABLE villeDeParis (
  villes VARCHAR( 60 ) ,
  demandes VARCHAR( 15 )  
 );
insert into villeDeParis values ("Paris", 15) ;
insert into villeDeParis values ("15e Arrondissement", 9) ;
insert into villeDeParis values ("18e Arrondissement", 14) ;
insert into villeDeParis values ("14e Arrondissement", 8) ;
insert into villeDeParis values ("9e Arrondissement", 10) ;
insert into villeDeParis values ("3e Arrondissement", 6) ;
insert into villeDeParis values ("13e Arrondissement", 5) ;


CREATE TABLE locations(
  `id` int(11) NOT NULL,
  `lat` float(10,6) NOT NULL,
  `lng` float(10,6) NOT NULL,
  `description` varchar(500) NOT NULL,
  `location_status` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
ALTER TABLE locations
  ADD PRIMARY KEY (`id`);
ALTER TABLE `locations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
  
    
CREATE TABLE IF NOT EXISTS `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `color` varchar(7) DEFAULT NULL,
  `start` datetime NOT NULL,
  `end` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) 
ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
INSERT INTO `events` (`id`, `title`, `color`, `start`, `end`) VALUES
(1, 'All Day Event', '#40E0D0', '2016-01-01 00:00:00', '0000-00-00 00:00:00'),
(2, 'Long Event', '#FF0000', '2016-01-07 00:00:00', '2016-01-10 00:00:00'),
(3, 'Repeating Event', '#0071c5', '2016-01-09 16:00:00', '0000-00-00 00:00:00'),
(4, 'Conference', '#40E0D0', '2016-01-11 00:00:00', '2016-01-13 00:00:00'),
(5, 'Meeting', '#000', '2016-01-12 10:30:00', '2016-01-12 12:30:00'),
(6, 'Lunch', '#0071c5', '2016-01-12 12:00:00', '0000-00-00 00:00:00'),
(7, 'Happy Hour', '#0071c5', '2016-01-12 17:30:00', '0000-00-00 00:00:00'),
(8, 'Dinner', '#0071c5', '2016-01-12 20:00:00', '0000-00-00 00:00:00'),
(9, 'Birthday Party', '#FFD700', '2016-01-14 07:00:00', '2016-01-14 07:00:00'),
(10,'Double click to change', '#008000', '2016-01-28 00:00:00', '0000-00-00 00:00:00');
	
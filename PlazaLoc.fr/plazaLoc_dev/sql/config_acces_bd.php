<?php

class DBConfig {
    public static $host = "localhost";
    public static $database = "agence_plaza_v2";
    public static $user = "root";
    public static $password = "";

    public static $options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');

    private function __construct(){}
    
}

?>
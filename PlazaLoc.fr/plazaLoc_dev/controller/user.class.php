<?php
/*
    cette classe modelise un utilisateur.
*/

require_once (dirname(__FILE__).'/../services/user_services.class.php');

class UserController
{
    private $acces_bd;
    
    private $mail;
    private $mdp;
    private $profile;
    private $id;
    private $nom;
    private $prenom;

    public function __construct ($mail, $mdp, $acces_bd)
    {
        $this->mail = $mail;
        $this->mdp = $mdp;
        $this->acces_bd = $acces_bd;

    }

    public function getMail() {
        return $this->mail;
    }

    public function getNom() {
        return $this->nom;
    }

    public function getPrenom() {
        return $this->prenom;
    }
    
    public function getProfile() {
         return $this->profile;
    }
    
  
    /*
    * Verifie la validité de la connexion de l'utilisateur.
    * Retourne : true si le login/mot de passse de l'utilisateur sont corrects, false sinon.
    */
    public function validerConnexion() {

        $infoUser = $this->acces_bd->getUserInfo($this->mail, $this->mdp);
        if ($infoUser != null) {

            $this->nom = $infoUser["nom"];
            $this->prenom = $infoUser["prenom"];
            
            $this->mettreAjourProfileId($infoUser); 
            $this -> enregistreCookie();

            return true;
        } else {
            
            return false;
        }
    }
    
    private function mettreAjourProfileId($infoUser){
        if ($infoUser["admin"] !=null)
        {
            $this->id = $infoUser["admin"];
            $this->profile = "admin";
        } else if($infoUser["proprietaire"] != null)
        {
            $this->id = $infoUser["proprietaire"];
            $this->profile = "proprietaire";
        } else if ($infoUser["locataire"] !=null) {
            $this->id = $infoUser["locataire"];
            $this->profile = "locataire";
        } else {
            var_dump($infoUser);
            die ("Profile innatendu");
        }
    }

    private function enregistreCookie() {

        UserService::saveUserInfo(array(
            "user_profil" => $this->profile,
            "user_id" => $this->id,
            "user_nom" =>  $this->nom,
            "user_prenom" => $this->prenom
        ));
    }

    private function storeCookie ($key, $value, $dureeValiditeCookie ) {
        setcookie($key, $value, $dureeValiditeCookie , '/');
        $_COOKIE[$key] = $value;
    }
}
?>

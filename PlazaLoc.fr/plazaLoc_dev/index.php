<?php


session_start();
require_once 'model/dao_locataire.class.php';
$acces_bd = new dao_locataire();
$unBien = $acces_bd->selectOnBien (isset($_GET['page'])?$_GET['page']:0);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="layout.css">
    <link href="node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <script type="text/javascript" language="JavaScript" src="lib/jquery-2.2.4.min.js"></script>

    <script type="text/javascript" language="JavaScript" src="js/formLocation.js"></script>
    <script type="text/javascript" language="JavaScript" src="js/dropdown-connexion.js"></script>

    <script type="text/javascript" language="JavaScript" src="slide/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" language="JavaScript" src="slide/js/jssor.slider-27.1.0.min.js"></script>
    <script type="text/javascript" language="JavaScript" src="slide/slide.js"></script>
    <title>Plaza Loc</title>
</head>
<body>

<div class="row" style="margin-left: 0; margin-right: 0">
    <header class="col-lg-12">

        <div class="container-header">
            <div class="col-lg-5 logo-title">
                <a href="index.php"><img src="images/logo.png" alt="" title=""></a>
                <h1><a href="index.php">PLAZA LOC</a></h1>
            </div>
            <nav class="col-lg-7">
                <ul>
                    <li><a href="index.php?page=1" class="test">Location</a></li>
                    <li><a href="index.php?page=2">Gestion locative</a></li>
                    <li><a href="index.php?page=8">Catalogue des biens</a></li>  
                    <li><a href="http://www.lemonde.fr/immobilier">Actualité Immobilère</a></li>
                </ul>
            </nav>
        </div>
        <div class="connexion">
            <div>
                <div class="link-connexion">
                    <span class="glyphicon glyphicon-log-in"></span><p>Accéder à mon espace</p>
                </div>
                <div class="dropdown-connexion">
                    <form method="post" action="view/main/viewMain.php">
                        <span class="glyphicon glyphicon-remove"></span>
                        <p>Se connecter</p>
                        <p>Email:</p>
                        <input type="text" name="email" class="form-control">
                        <p>Mot de passe:</p>
                        <input type="password" name="mdp" class="form-control">
                        <button type="submit" name="seconnecter" class="button">Se connecter</button>
                        <p><a href="index.php?page=11" class="link">MDP oublié !</a> </p>
                    </form>
                </div>

            </div>
            <a href="index.php?page=5" class="link">Pas encore inscrit?</a>
        </div>
    </header>
</div>
    <?php
    if (isset($_GET['page'])) {
        $page = $_GET['page'];
    }
    else {
        $page=  0;
    }


   
    switch($page)
    {
        case 0:
            require_once("view/page/viewHome.php");
            break;
        case 1:
            require_once("view/page/viewLocation.php");
            break;
        case 2:
            require_once("view/page/viewGestion.php");
            break;
        case 3:
            require_once("view/page/viewAgence.php");
            break;
        case 4:
            require_once("view/page/viewContact.php");
            break;
        case 5:
            require_once("view/form/viewNewUser.php");
            break;
        case 6:
            require_once("view/form/viewNewLocataire.php");
            break;
        case 7:
            require_once("view/form/viewNewOwner.php");
            break;
        case 8:
            require_once ("view/page/viewCatalogue.php");
            break;
        case 9:
            require_once ("view/admin/cgu&confi/viewCGU.php");
            break;
        case 10:
            require_once ("view/admin/cgu&confi/viewConfidentialite.php");
            break;
        case 11:
            require_once ("view/admin/gestion/mdpOublie.php");
            break;
        case 12:
            require_once ("view/admin/gestion/mdpInit.php");
            break;
        default:     
            ?>
            <div class="container-bien container-catalogue">
                <div class="col-lg-6">
                    <img src="images/<?= $unBien['image'] ?>">
                </div>
                <div class="col-lg-6 page-bien-desc">
                    <h3><span class="glyphicon glyphicon-home"></span> <?= $unBien['titlebien'] ?></h3>
                    <p><?= $unBien['descriptionbien'] ?></p>

                    <h4 class="page-bien-title">Détails</h4>
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <td>Réf. du bien</td>
                            <td><?= $unBien['idbien'] ?></td>
                        </tr>
                        <tr>
                            <td>Nombre de pièce(s)</td>
                            <td><?= $unBien['nbpiece'] ?></td>
                        </tr>
                        <tr>
                            <td>Surface</td>
                            <td><?= $unBien['superficie'] ?> m²</td>
                        </tr>
                        <tr>
                            <td>Etage</td>
                            <td><?= $unBien['etagebien'] ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="container-button-form">
                    <a href="index.php?page=5" class="button">Effectuer une demande de location</a>
                </div>
            </div>
            <?php

            break;
    }


    ?>

<footer>
    <nav>
        <ul>         
            <li><a href="index.php?page=3">Qui sommes-nous?</a></li>
            <li><a href="index.php?page=4">Contactez-nous</a></li>
            <li><a href="index.php?page=10">Confidentialité</a></li>
            <li><a href="index.php?page=9">CGU</a></li>

        </ul>
    </nav>
    <div class="infos">
        <p>Plaza Loc &copy;</p>
    </div>
</footer>
</body>
</html>
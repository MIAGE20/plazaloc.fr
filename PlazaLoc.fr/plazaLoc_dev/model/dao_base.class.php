<?php
/*
    Classe de base d'accès à la BD
*/

require_once(dirname(__FILE__)."/../sql/config_acces_bd.php");

class dao_base
{
    protected $connexion_bd; //instance de la classe PDO

    public function __construct ()
    {
        $this->connexion_bd = null ;
        try{
            $urlBD = "mysql:host=".DBConfig::$host.";dbname=".DBConfig::$database ;
            $this->connexion_bd = new PDO($urlBD,  DBConfig::$user,  DBConfig::$password, DBConfig::$options);
        }
        catch (Exception $exp)
        {
            var_dump($exp);
            echo "Erreur de connexion à la BDD ! ";
        }
    }

    protected function printErrorExit($sqlQuery) {
        echo '<pre>';
        echo (str_repeat('-', 100)).("<br/>ECHEC SQL<br/>").(str_repeat('-', 100)).'<br/>';

        var_dump($sqlQuery -> errorInfo());
        $sqlQuery->debugDumpParams();
        echo (str_repeat('-', 100)).'<br/>CALL STACK: <br/>'.(str_repeat('=', 12)).'<br/>';
        debug_print_backtrace();
        die ("QUIT.");
        echo '</pre>';
    }

    protected function doSelect($request, $params) {
        if ($this->connexion_bd != null) {
            $statement = $this->connexion_bd->prepare ($request) ;
            $statement->execute ($params) or $this->printErrorExit($statement);
            return $statement->fetchAll();
        } else {
            return null;
        }
    }

    protected function doUpdateOrDelete($request, $params) {
        if ($this->connexion_bd != null) {
            $statement = $this->connexion_bd->prepare ($request) ;
            $statement->execute ($params) or $this->printErrorExit($statement);
        } 
    }

}
?>
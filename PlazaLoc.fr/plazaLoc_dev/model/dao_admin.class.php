<?php
/*
    cette classe Modele permet d'accceder à la BDD avec
    différentes méthodes : connexion, select, delete, update etc.
*/
require_once(dirname(__FILE__)."/dao_base.class.php");
class dao_admin extends dao_base
{


    /**
     * Recupère toutes les données d'un utilisateur à partir de son mail, mot de passe et profile
     */


    public function getUserInfo($mail, $mdp)
    {
        $requete = "CALL getUserInfo(:mail, :mdp);";

        if ($this->connexion_bd == null) {
            return null;
        } else {
            $donnees = array(":mail" => $mail, ":mdp" => $mdp);
            $select = $this->connexion_bd->prepare($requete);
            $select->execute($donnees);

            $resultats = $select->fetchAll();
            if ($resultats == array()) {
                return null;

            } else  return $resultats[0];
        }
    }


//insertion
    public function insertProprietaire($civiliteproprietaire, $nom, $prenom, $datenaissance, $adresse, $numtel, $mail, $assurance, $ccp, $mdp)
    {
        $requete = "CALL insertProprietaire(:civiliteproprietaire, :nom, :prenom, :datenaissance, :adresse, :numtel, :mail, :assurance, :ccp ,:mdp);";
        if ($this->connexion_bd == null) {
            return null;
        } else {
            $donnees = array(
                ":civiliteproprietaire" => $civiliteproprietaire,
                ":nom" => $nom,
                ":prenom" => $prenom,
                ":datenaissance" => $datenaissance,
                ":adresse" => $adresse,
                ":numtel" => $numtel,
                ":mail" => $mail,
                ":assurance" => $assurance,
                ":ccp" => $ccp,
                ":mdp" => $mdp
            );
            $select = $this->connexion_bd->prepare($requete);
            $select->execute($donnees);
        }
    }


    public function insertLocataire($civilitelocataire, $nom, $prenom, $datenaissance, $adresse, $numtel, $mail, $mdp, $ccp)
    {
        $requete = "CALL insertLocataire(:civilitelocataire, :nom, :prenom, :datenaissance, :adresse, :numtel, :mail, :mdp, :ccp);";
        if ($this->connexion_bd == null) {
            return null;
        } else {
            $donnees = array(
                ":civilitelocataire" => $civilitelocataire,
                ":nom" => $nom,
                ":prenom" => $prenom,
                ":datenaissance" => $datenaissance,
                ":adresse" => $adresse,
                ":numtel" => $numtel,
                ":mail" => $mail,
                ":mdp" => $mdp,
                ":ccp" => $ccp
            );
            $select = $this->connexion_bd->prepare($requete);
            $select->execute($donnees);
        }
    }

    public function insertMessage($nom, $prenom, $sujet, $mail, $numtel, $textemessage)
    {
        $requete = "Call insertMessage( :nom, :prenom, :sujet, :mail, :numtel, :textemessage) ;";
        $donnees = array(
            ":nom" => $nom,
            ":prenom" => $prenom,
            ":sujet" => $sujet,
            ":mail" => $mail,
            ":numtel" => $numtel,
            ":textemessage" => $textemessage
        );
        $this->doUpdateOrDelete($requete, $donnees);
    }

    public function insertMessageEnvoye($mail, $sujet, $textemessage)
    {
        $requete = "CALL insertMessageEnvoye(:mail, :sujet, :textemessage);";
        if ($this->connexion_bd == null) {
            return null;
        } else {
            $donnees = array(
                ":mail" => $mail,
                ":textemessage" => $textemessage,
                ":sujet" => $sujet
            );
            $select = $this->connexion_bd->prepare($requete);
            $select->execute($donnees);
        }
    }

    public function repondre($idmessage, $mail, $sujet, $textemessage)
    {
        $this->doUpdateOrDelete("CALL repondre(:idmessage, :mail, :sujet, :textemessage);",
            array(
                ":idmessage" => $idmessage,
                ":mail" => $mail,
                ":sujet" => $sujet,
                ":textemessage" => $textemessage
            ));
    }


//selection

    public function selectAllProprietaire()
    {
        $requete = "CALL getListeProprietaire();";
        if ($this->connexion_bd == null) {
            return null;
        } else {
            $select = $this->connexion_bd->prepare($requete);
            $select->execute();
            $resultats = $select->fetchAll();
            return $resultats;
        }
    }

    public function selectAllProprietaire2()
    {
        $requete = "CALL getListeProprietaire2();";
        if ($this->connexion_bd == null) {
            return null;
        } else {
            $select = $this->connexion_bd->prepare($requete);
            $select->execute();
            $resultats = $select->fetchAll();
            return $resultats;
        }
    }

    public function selectAllLocataire()
    {
        $requete = "CALL getListeLocataire() ;";
        if ($this->connexion_bd == null) {
            return null;
        } else {
            $select = $this->connexion_bd->prepare($requete);
            $select->execute();
            $resultats = $select->fetchAll();
            return $resultats;
        }
    }

    public function selectAllBien()
    {
        $requete = "CALL getAllBien() ;";
        if ($this->connexion_bd == null) {
            return null;
        } else {
            $select = $this->connexion_bd->prepare($requete);
            $select->execute();
            $resultats = $select->fetchAll();
            return $resultats;
        }
    }

    public function selectAllLocation()
    {
        $requete = "CALL getListeLocation() ;";

        return $this->doSelect($requete, array());
    }

    public function selectAllDemandeLocation()
    {
        $requete = "CALL getListeDemandeLocation ;";
        if ($this->connexion_bd == null) {
            return null;
        } else {
            $select = $this->connexion_bd->prepare($requete);
            $select->execute();
            $resultats = $select->fetchAll();
            return $resultats;
        }
    }

    public function selectAllDemandeLocationSansBien()
    {
        $requete = " CALL getListeDemandeLocationSansBien();";
        if ($this->connexion_bd == null) {
            return null;
        } else {
            $select = $this->connexion_bd->prepare($requete);
            $select->execute();
            $resultats = $select->fetchAll();
            return $resultats;
        }
    }

    public function selectAllmessages()
    {
        $requete = "CALL getListeMessage() ;";
        if ($this->connexion_bd == null) {
            return null;
        } else {
            $select = $this->connexion_bd->prepare($requete);
            $select->execute();
            $resultats = $select->fetchAll();
            return $resultats;
        }
    }

    public function getListeMessageEnvoye()
    {
        $requete = "CALL getListeMessageEnvoye();";
        if ($this->connexion_bd == null) {
            return null;
        } else {
            $select = $this->connexion_bd->prepare($requete);
            $select->execute();
            $resultats = $select->fetchAll();
            return $resultats;
        }
    }

    public function selectOnmessages($idmessage)
    {
        $requete = "CALL getListeOnmessage(:idmessage);";
        if ($this->connexion_bd == null) {
            return null;
        } else {
            $donnee = array(":idmessage" => $idmessage);
            $select = $this->connexion_bd->prepare($requete);
            $select->execute($donnee);
            $resultat = $select->fetch();
            return $resultat;
        }
    }

    public function selectAllContrat()
    {
        $requete = "CALL getListeContrat();";
        if ($this->connexion_bd == null) {
            return null;
        } else {
            $select = $this->connexion_bd->prepare($requete);
            $select->execute();
            $resultats = $select->fetchAll();
            return $resultats;
        }
    }

//compteur
    public function countProprietaires()
    {
        $requete = "CALL countProprietaires();";
        if ($this->connexion_bd == null) {
            return null;
        } else {
            $select = $this->connexion_bd->prepare($requete);
            $select->execute();
            $resultats = $select->fetchColumn();
            return $resultats;
        }
    }

    public function countLocataires()
    {
        $requete = "CALL countLocataires();";
        if ($this->connexion_bd == null) {

            return null;
        } else {
            $select = $this->connexion_bd->prepare($requete);
            $select->execute();
            $resultats = $select->fetchColumn();
            return $resultats;
        }
    }

    public function countBiens()
    {
        $requete = "CALL countBiens();";
        if ($this->connexion_bd == null) {
            return null;
        } else {
            $select = $this->connexion_bd->prepare($requete);
            $select->execute();
            $resultats = $select->fetchColumn();
            return $resultats;
        }
    }

    public function countDemandesLocation()
    {
        $requete = "CALL countDemandesLocation();";
        if ($this->connexion_bd == null) {
            return null;
        } else {
            $select = $this->connexion_bd->prepare($requete);
            $select->execute();
            $resultats = $select->fetchColumn();
            return $resultats;
        }
    }

    public function countLocations()
    {
        $requete = "CALL countLocations();";
        if ($this->connexion_bd == null) {
            return null;
        } else {
            $select = $this->connexion_bd->prepare($requete);
            $select->execute();
            $resultats = $select->fetchColumn();
            return $resultats;
        }
    }

    public function countMessages()
    {
        $requete = "CALL countMessages();";
        if ($this->connexion_bd == null) {
            return null;
        } else {
            $select = $this->connexion_bd->prepare($requete);
            $select->execute();
            $resultats = $select->fetchColumn();
            return $resultats;
        }
    }

    public function countContrats()
    {
        $requete = "CALL countContrats();";
        if ($this->connexion_bd == null) {
            return null;
        } else {
            $select = $this->connexion_bd->prepare($requete);
            $select->execute();
            $resultats = $select->fetchColumn();
            return $resultats;
        }
    }

    public function graphisme()
    {
        $requete = "CALL graphisme() ;";
        return $this->doSelect($requete, array());
    }

    public function countLocalisation()
    {
        $requete = "CALL countLocalisation() ;";
        if ($this->connexion_bd == null) {
            return null;
        } else {
            $select = $this->connexion_bd->prepare($requete);
            $select->execute();
            $resultats = $select->fetchColumn();
            return $resultats;
        }
    }

//suppression	
    public function deleteProprietaire($idproprietaire)
    {
        $requete = "CALL deleteProprietaire(:idproprietaire);";

        if ($this->connexion_bd == null) {
            return null;
        } else {
            $donnee = array(":idproprietaire" => $idproprietaire);
            $select = $this->connexion_bd->prepare($requete);
            $select->execute($donnee);
        }
    }

    public function desactiveProprietaire($idproprietaire)
    {
        $requete = "CALL desactiveProprietaire(:idproprietaire);";

        if ($this->connexion_bd == null) {
            return null;
        } else {
            $donnee = array(":idproprietaire" => $idproprietaire);
            $select = $this->connexion_bd->prepare($requete);
            $select->execute($donnee);
        }
    }

    public function deleteLocataire($idlocataire)
    {
        $requete = "CALL deleteLocataire(:idlocataire);";
        if ($this->connexion_bd == null) {
            return null;
        } else {
            $donnee = array(":idlocataire" => $idlocataire);
            $select = $this->connexion_bd->prepare($requete);
            $select->execute($donnee);
        }
    }

    public function deleteMessageEnvoye($idmessage)
    {
        $requete = " CALL deleteMessageEnvoye(:idmessage);";
        $this->doUpdateOrDelete($requete, array(":idmessage" => $idmessage));
    }

    public function deleteDemandeLocationPersonnalisee($iddemandelocationsansbien)
    {
        $requete = "CALL deleteDemandeLocationPersonnalisee(:iddemandelocationsansbien);";
        $this->doUpdateOrDelete($requete, array(':iddemandelocationsansbien' => $iddemandelocationsansbien));
    }

    public function deleteContrat($idcontrat)
    {
        $requete = "CALL deleteContrat(:idcontrat);";
        $this->doUpdateOrDelete($requete, array(':idcontrat' => $idcontrat));
    }


    public function envoiEmail($email)
    {
        $requete = "CALL envoiEmail(:email);";
        if ($this->connexion_bd == null) {
            return null;
        } else {
            $donnee = array(":email" => $email);
            $select = $this->connexion_bd->prepare($requete);
            $select->execute($donnee);
            $resultat = $select->fetch();
            return $resultat[0];
        }


    }


    public function modifierMdp($email, $newMdp)
    {
        $requete = "CALL modifierMdp(:email, :newMdp);";
        if ($this->connexion_bd == null) {
            return null;
        } else {
            $donnees = array(
                ":email" => $email,
                ":newMdp" => $newMdp
            );
            $select = $this->connexion_bd->prepare($requete);
            $select->execute($donnees);
        }
    }

    public function selectAllDemandeLocationSansBien2($idlocataire)
    {
        $requete =" CALL getListeDemandeLocationSansBien2(:idlocataire);" ;
        $donnee = array(":idlocataire"=>$idlocataire);
        return $this->doSelect($requete, $donnee);
    }


    /* TP BTS */
    public function trouveBienM2()
    {
        $requete =" CALL trouveBienM2();" ;
        return $this->doSelect($requete, array());
    }

    public function trouveBienAn()
    {
        $requete =" CALL trouveBienAn();" ;
        return $this->doSelect($requete, array());
    }

}
?>


<?php
/*
    cette classe Modele permet d'accceder à la BDD avec
    différentes méthodes : connexion, select, delete, update etc.
*/

require_once(dirname(__FILE__)."/dao_base.class.php");
class dao_proprietaire extends dao_base
{

    public function selectOnProprietaire ($idproprietaire)
    {
        $requete ="CALL getProprietaire(:idproprietaire);" ;
        if ($this->connexion_bd == null)
        {
            return null;
        } else {
            $donnee = array(":idproprietaire"=>$idproprietaire);
            $select = $this->connexion_bd->prepare ($requete) ;
            $select->execute ($donnee);
            $resultat = $select->fetch();
            return $resultat ;
        }
    }

    public function selectOnBien ($idbien)
    {
        $requete ="CALL getIdBien(:idbien);" ;
        if ($this->connexion_bd == null)
        {
            return null;
        } else {
            $donnee = array(":idbien"=>$idbien);
            $select = $this->connexion_bd->prepare ($requete) ;
            $select->execute ($donnee);
            $resultat = $select->fetch();
            return $resultat ;
        }
    }
 
    public function modifyProprietaire($tab)
    {
        $requete ="CALL modifyProprietaire(:idproprietaire, :civiliteproprietaire, :nom, :prenom, :datenaissance, :adresse, :numtel, :mail, :mdp, :assurance , :ccp);";
        if ($this->connexion_bd == null)
        {
            return null;
        } else {
            $donnees = array(
                ":civiliteproprietaire"=>$tab['civiliteproprietaire'],
                ":nom"=>$tab['nom'],
                ":prenom"=>$tab['prenom'],
                ":datenaissance"=>$tab['datenaissance'],
                ":adresse"=>$tab['adresse'],
                ":numtel"=>$tab['numtel'],
                ":mail"=>$tab['mail'],
                ":mdp"=>$tab['mdp'],
				":assurance"=>$tab['assurance'],
                ":ccp"=>$tab['ccp'],
                ":idproprietaire" => $tab['idproprietaire']
            );

            $update = $this->connexion_bd->prepare ($requete) ;
            $update->execute ($donnees) or die("Noooooonnnnnnn!!!");
        }
    }

    public function modifyBien($tab)
    {
        
     
       
        $requete ="Call modifyBien(:idbien,
					:idtypebien,
					:titlebien,
					:adresse,
					:codePostale,
					:nomquartier,
					:nomville,
					:superficie,
					:nbpiece,
					:nbchambre,
					:etagebien,
					:descriptionbien,
					:chargeforfaitaire,
					:montantLoyer, 
					:image);";
					
		$donnees = array(
			":idtypebien"=>$tab['idtypebien'],
			":titlebien"=>$tab['titlebien'],
			":adresse"=>$tab['adresse'],
			":codePostale"=>$tab['codePostale'],
			":nomquartier"=>$tab['nomquartier'],
			":nomville"=>$tab['nomville'],
			":superficie"=>$tab['superficie'],
			":nbpiece"=>$tab['nbpiece'],
			":nbchambre"=>$tab['nbchambre'],
			":etagebien"=>$tab['etagebien'],
			":descriptionbien"=>$tab['descriptionbien'],
			":chargeforfaitaire"=> isset($tab['chargeforfaitaire'])? $tab['chargeforfaitaire'] : null,
			":montantLoyer"=>$tab['montantLoyer'],
			":idbien" => $tab["idbien"],
			":image" => $tab["image"]
		);
/*
		if ($hasImage) {
			$donnees = array_merge($donnees, array(":image"=>$tab['image']));
		}
		*/
		$update = $this->connexion_bd->prepare ($requete) ;
		$update->execute ($donnees) or $this->printErrorExit($update) ;
	
    }

    public function deleteBien ($idbien)
    {
        $requete ="CALL deleteBien(:idbien);" ;
        if ($this->connexion_bd == null)
        {
            return null;
        } else {
            $donnee = array(":idbien"=>$idbien);
            $select = $this->connexion_bd->prepare ($requete) ;
            $select->execute ($donnee);
        }
    }

    public function deleteContrat ($idcontrat)
    {
        
		$requete ="CALL deleteContrat(:idcontrat);" ;
        if ($this->connexion_bd == null)
        {
            return null;
        } else {
            $donnee = array(":idcontrat"=>$idcontrat);
            $select = $this->connexion_bd->prepare ($requete) ;
            $select->execute ($donnee);
        }
    }

    public function deleteLocation ($idlocation)
    {
        $requete ="CALL deleteLocation(:idlocation);" ;
        if ($this->connexion_bd == null)
        {
            return null;
        } else {
            $donnee = array(":idlocation"=>$idlocation);
            $delete = $this->connexion_bd->prepare ($requete) ;
            $delete->execute ($donnee) or printErrorExit($delete);
        }
    }

    public function selectAllBienByProprietaire ($idProprietaire)
    {
        $requete ="CALL getAllBienByProprietaire(:idProprietaire);" ;
        if ($this->connexion_bd == null)
        {
            return null;
        } else {
            $donnee = array(":idProprietaire"=>$idProprietaire);
            $select = $this->connexion_bd->prepare ($requete) ;
            $select->execute ($donnee);
            $resultats = $select->fetchAll();
            return $resultats ;
        }
    }


    public function selectAllLocationByProprietaire ($idProprietaire)
    {
        
		$requete ="CALL getAllLocationByProprietaire(:idProprietaire);";
       
        // if ($this->connexion_bd == null)
        // {
        //     return null;
        // } else {
        $donnee = array(":idProprietaire"=>$idProprietaire);

        return $this->doSelect($requete, $donnee);

        //     $select = $this->connexion_bd->prepare ($requete) ;
        //     $select->execute ($donnee);
        //     $resultats = $select->fetchAll();
        //     return $resultats ;
        // }
    }


    public function selectAllDemandeLocationByProprietaire ($idProprietaire)
    {
		$requete ="CALL getAllDemandeLocationByProprietaire(:idProprietaire);";

        if ($this->connexion_bd == null)
        {
            return null;
        } else {
            $donnee = array(":idProprietaire"=>$idProprietaire);
            $resultats = $this->doSelect($requete, $donnee);

            return $resultats ;
        }
    }

    public function validerDemandeLocation($idDemandeLocation) {
        $requeteValidation = "CALL validerDemandeLocation( :idDemandeLocation);";

        $requeteSuppression = "CALL deleteDemandeLocation( :idDemandeLocation);";

        if ($this->connexion_bd == null)
        {
            return null;
        } else {
            $donnee = array(":idDemandeLocation"=>$idDemandeLocation);

            $insert = $this->connexion_bd->prepare ($requeteValidation) ;
            $insert->execute($donnee) or $this->printErrorExit($insert);
            
            $delete = $this->connexion_bd->prepare ($requeteSuppression) ;
            $delete->execute($donnee) or $this->printErrorExit($delete);

        }


    }

    public function selectAllContratByProprietaire ($idProprietaire)
    {
        $requete ="CALL getAllContratByProprietaire(:idProprietaire);";
        if ($this->connexion_bd == null)
        {
            return null;
        } else {
            $donnee = array(":idProprietaire"=>$idProprietaire);
            $select = $this->connexion_bd->prepare ($requete) ;
            $select->execute ($donnee) or die(__FILE__.'::: SQL ERROR');
            $resultats = $select->fetchAll();
            return $resultats ;
        }
    }

 

    public function countBiensByProprietaire($idProprietaire)
    {
            $requete ="CALL countBiensByProprietaire(:idProprietaire);";
            if ($this->connexion_bd == null)
            {
                return null;
            } else {
                $donnee = array(":idProprietaire"=>$idProprietaire);
                $select = $this->connexion_bd->prepare ($requete) ;
                $select->execute ($donnee);
                $resultats = $select->fetchAll();
                return $resultats[0]['nb'] ;
            }
    }


    public function countLocationsByProprietaire($idProprietaire)
    {
            $requete ="CALL countLocationsByProprietaire(:idProprietaire);" ;

            $resultats = $this->doSelect($requete, array(":idProprietaire" => $idProprietaire));
            return $resultats[0]['nb'];
    }


    public function countDemandesLocationByProprietaire($idProprietaire)
    {
            $requete ="CALL countDemandesLocationByProprietaire(:idProprietaire);" ;
            if ($this->connexion_bd == null)
            {
                return null;
            } else {
                $donnee = array(":idProprietaire"=>$idProprietaire);
                $select = $this->connexion_bd->prepare ($requete) ;
                $select->execute ($donnee) or printErrorExit($select);
                $resultats = $select->fetchAll();
                return $resultats[0]['nb'] ; ;
            }
    }

    public function countAllContratByProprietaire($idProprietaire)
    {
            $requete ="CALL countAllContratByProprietaire(:idProprietaire);" ;
            if ($this->connexion_bd == null)
            {
                return null;
            } else {
                $donnee = array(":idProprietaire"=>$idProprietaire);
                $select = $this->connexion_bd->prepare ($requete) ;
                $select->execute ($donnee);
                $resultats = $select->fetchAll();
                return $resultats[0]['nb'] ; ;
            }
    }

    public function insertBien($tab)
    {
        $requete = "CALL insertBien(:idproprietaire,:idtypebien,:titlebien,:adresse,:codePostale,:nomquartier,
                                    :nomville,:superficie,:nbpiece,:nbchambre,:etagebien,:descriptionbien,:chargeforfaitaire,:montantLoyer,:image
                                    );" ;

        if ($this->connexion_bd == null) {
            return null;
        } else {
            $donnees = array(
                ":idproprietaire" => $tab['idproprietaire'],
                ":idtypebien" => $tab['idtypebien'],
                ":titlebien" => $tab['titlebien'],
                ":adresse" => $tab['adresse'],
                ":codePostale" => $tab['codePostale'],
                ":nomquartier" => $tab['nomquartier'],
                ":nomville" => $tab['nomville'],
                ":superficie" => $tab['superficie'],
                ":nbpiece" => $tab['nbpiece'],
                ":nbchambre" => $tab['nbchambre'],
                ":etagebien" => $tab['etagebien'],
                ":descriptionbien" => $tab['descriptionbien'],
                ":chargeforfaitaire" => $tab['chargeforfaitaire'],
                ":montantLoyer" => $tab['montantLoyer'],
                ":image" => $tab['image']
            );

            $insert = $this->connexion_bd->prepare($requete);
            $insert->execute($donnees) or $this->printErrorExit($insert);
        }
    }

    public function insertLocation($tab)
    {
        $requete = "CALL insertLocation(:prixloyer,:datedebut,:datefin,:idbien,:idproprietaire,:idlocataire,:idmotiflocation);";

        if ($this->connexion_bd == null)
        {
            return null;
        } else {
            $donnees = array(
                ":prixloyer"=>$tab['prixloyer'],
                ":datedebut"=>$tab['datedebut'],
                ":datefin"=>$tab['datefin'],
                ":idbien"=>$tab['idbien'],
                ":idproprietaire"=>$tab['idproprietaire'],
                ":idlocataire"=>$tab['idlocataire'],
                ":idmotiflocation"=>$tab['idmotiflocation']
            );
            $select = $this->connexion_bd->prepare ($requete) ;
            $select->execute ($donnees);
        }
    }

    public function modifymodifyContrat($tab)
    {
        $requete = "CALL insertContrat (:datedebut,:datefin,:prixloyer,:idlocataire,:idproprietaire,:idbien);";

        if ($this->connexion_bd == null)
        {
            return null;
        } else {
            $donnees = array(
                ":datedebut"=>$tab['datedebut'],
                ":datefin"=>$tab['datefin'],
                ":prixloyer"=>$tab['prixloyer'],
                ":idlocataire"=>$tab['idlocataire'],
                ":idproprietaire"=>$tab['idproprietaire'],
                ":idbien"=>$tab['idbien']
            );
            $select = $this->connexion_bd->prepare ($requete) ;
            $select->execute ($donnees);
        }
    }

    public function insertQuartier($tab)
    {
        $requete = "CALL insertQuartier(:nomquartier);";

        if ($this->connexion_bd == null)
        {
            return null;
        } else {
            $donnees = array(
                ":nomQuartier"=>$tab['nomQuartier']
            );
            $select = $this->connexion_bd->prepare ($requete) ;
            $select->execute ($donnees);
        }
    }

    public function insertVille($tab)
    {
        $requete = "CALL insertVille(:nomville,:codePostale);";

        if ($this->connexion_bd == null)
        {
            return null;
        } else {
            $donnees = array(
                ":nomville"=>$tab['nomville'],
                ":codePostale"=>$tab['codePostale']
            );
            $select = $this->connexion_bd->prepare ($requete) ;
            $select->execute ($donnees);
        }
    }
}
?>
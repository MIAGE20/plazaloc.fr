<?php

/*
    cette classe Modele permet d'accceder à la BDD avec
    différentes méthodes : connexion, select, delete, update etc.
*/

require_once(dirname(__FILE__)."/dao_base.class.php");
class dao_locataire extends dao_base
{

    public function selectOnLocataire ($idlocataire)
    {
        $requete ="CALL getLocataire(:idlocataire);" ;
        if ($this->connexion_bd == null)
        {
            return null;
        } else {
            $donnee = array(":idlocataire"=>$idlocataire);
            $select = $this->connexion_bd->prepare ($requete) ;
            $select->execute ($donnee);
            $resultat = $select->fetch();
            return $resultat ;
        }
    }

    public function modifyLocataire($tab)
    {
        $requete ="CALL modifyLocataire(:idlocataire, :civilitelocataire, :nom, :prenom, :datenaissance, :adresse, :numtel, :mail, :mdp, :ccp);";
        $donnees = array(
            ":civilitelocataire"=>$tab['civilitelocataire'],
            ":nom"=>$tab['nom'],
            ":prenom"=>$tab['prenom'],
            ":datenaissance"=>$tab['datenaissance'],
            ":adresse"=>$tab['adresse'],
            ":numtel"=>$tab['numtel'],
            ":mail"=>$tab['mail'],
            ":mdp"=>$tab['mdp'],
			":ccp"=>$tab['ccp'],
            ":idlocataire" => $tab['idlocataire']
        );

        $this->doUpdateOrDelete($requete, $donnees);
    
    }

    public function deleteContrat ($idcontrat)
    {
        $requete ="CALL deleteContrat(:idcontrat) ;" ;
        $this->doUpdateOrDelete($requete, array(":idcontrat" => $idcontrat));
    }

    public function deleteLocation ($idlocation)
    {
        $requete ="CALL deleteLocation(:idlocation) ;" ;
        $this->doUpdateOrDelete($requete, array(":idlocation" => $idlocation));
    }


    public function catalogBien ()
    {
        $requete ="CALL  catalogBien() ;" ;
        if ($this->connexion_bd == null)
        {
            return null;
        } else {
            $select = $this->connexion_bd->prepare ($requete) ;
            $select->execute ();
            $resultats = $select->fetchAll();
            return $resultats ;
        }
    }

    public function selectAllMotifLocation() {
        return $this->doSelect("CALL getAllMotifLocation()", array());
    }

    public function selectOnBien ($idbien)
    {
        $requete ="Call getBien(:idbien) ;" ;
        if ($this->connexion_bd == null)
        {
            return null;
        } else {
            $donnee = array(":idbien"=>$idbien);
            $select = $this->connexion_bd->prepare ($requete) ;
            $select->execute ($donnee);
            $resultat = $select->fetch();
            return $resultat ;
        }
    }

    public function selectAllDemandeLocationByLocataire ($idlocataire)
    {
        $requete ="CALL getAllDemandeLocationByLocataire(:idlocataire);" ;
        $donnee = array(":idlocataire"=>$idlocataire);
        
        return $this->doSelect($requete, $donnee);
    }


    public function selectAllLocationsByLocataire ($idlocataire)
    {
        $requete ="CALL getAllLocationByLocataire(:idlocataire) ;" ;
        if ($this->connexion_bd == null)
        {
            return null;
        } else {
            $donnees = array(":idlocataire"=>$idlocataire);
            return $this->doSelect($requete, $donnees) ;
        }
    }


    public function countDemandesLocationByLocataire($idLocataire)
         {
            $requete ="CALL countDemandesLocationByLocataire(:idLocataire) ;" ;
            if ($this->connexion_bd == null)
            {
                return null;
            } else {
                $donnee = array(":idLocataire"=>$idLocataire);
                $select = $this->connexion_bd->prepare ($requete) ;
                $select->execute ($donnee);
                $resultats = $select->fetchAll();
                return $resultats[0]['nb'] ; ;
            }
        }


        public function countBiens()
    {
        $requete = "CALL countBiens();";
        if ($this->connexion_bd == null)
        {
            return null;
        } else {
            $select = $this->connexion_bd->prepare ($requete) ;
            $select->execute ();
            $resultats = $select->fetchColumn();
            return $resultats;
        }
    }

    public function countAllContratByLocataire($idlocataire)
    {
        $requete ="CALL countAllContratByLocataire(:idlocataire);" ;
        if ($this->connexion_bd == null)
        {
            return null;
        } else {
            $donnee = array(":idlocataire"=>$idlocataire);
            $select = $this->connexion_bd->prepare ($requete) ;
            $select->execute ($donnee);
            $resultats = $select->fetchColumn();
            return $resultats;
        }
    }

    public function deleteDemandeLocation($iddemandelocation) {
        $requete = "Call deleteDemandeLocation(:iddemandelocation);";
        $this->doUpdateOrDelete($requete, array(':iddemandelocation' => $iddemandelocation));
    }
    
    public function insertDemandeLocation($tab)
    {
        $requete = " CALL insertDemandeLocation (:datedemande, :datedebut, :datefin, 
												:idbien, :idlocataire, :idmotiflocation) ;";

        $donnees = array(
            ":datedemande"=>$tab['datedemande'],
            ":datedebut"=>$tab['debutLocation'],
            ":datefin"=>$tab['finLocation'],
            ":idbien"=>$tab['idbien'],
            ":idlocataire"=>$tab['idlocataire'],
            ":idmotiflocation"=>$tab['motiflocation'],
        );

        

        $this->doUpdateOrDelete($requete, $donnees);
    }

    public function insertDemandeLocationSansBien($tab)
    {



        $requete = "CALL insertDemandeLocationSansBien(:libelletype, :nbpiece, :descriptionmotif, :superficie, :prixloyer, :datedebut, :nomville, :idlocataire);";

   
        $donnees = array(
            ":libelletype"=>$tab['libelletype'],
            ":nbpiece"=>$tab['nbpiece'],
            ":descriptionmotif"=>$tab['descriptionmotif'],
            ":superficie"=>$tab['superficie'],
            ":prixloyer"=>$tab['prixloyer'],
            ":datedebut"=>$tab['datedebut'],
            ":nomville"=>$tab['nomville'],
            ":idlocataire"=>$tab['idlocataire']
        );

        $this->doUpdateOrDelete($requete, $donnees);
        
    }
	
		
	public function selectAllDemandeLocationSansBien2($idlocataire)
    {
        $requete =" CALL getListeDemandeLocationSansBien2(:idlocataire);" ;
        $donnee = array(":idlocataire"=>$idlocataire);
        return $this->doSelect($requete, $donnee);
    }

	public function deleteDemandeLocationPersonnalisee($iddemandelocationsansbien)
	{
        $requete = "delete from demandelocationsansbien where iddemandelocationsansbien = :iddemandelocationsansbien";
        $this->doUpdateOrDelete($requete, array(':iddemandelocationsansbien' => $iddemandelocationsansbien));
    }




    public function trouveBienByQuartier($quartier)
    {
        $requete =" CALL getTrouveBienByQuartier(:quartier);" ;
        $donnee = array(":quartier"=>$quartier);

        $listeBiens = $this->doSelect($requete, $donnee);

        return new ResultatBiens($listeBiens);
    }

}
class ResultatBiens
{
    private $biens;

    public function __construct ($biens)
    {
        $this->biens = $biens;
    }

    public function getBiens() {
        return $this->biens;
    }

    public function getNbBiens() {
        return count($this->biens);
    }

    public function estVide() {
        return $this->getNbBiens() == 0;
    }

}

?>

<?php

require_once(dirname(__FILE__)."/dao_base.class.php");
class dao_localisation extends dao_base
{
    
	public function ajouter_localisation($latitude, $longitude, $description)
    {
		if ($this->connexion_bd == null)
        {
            return null;
        } else {
		
            $requete = "insert into localisation (lat, lng, description)
                    VALUES (:lat, :lng, :desc );";
            
            $data = array(
                ":lat" => $latitude,
                ":lng" => $longitude,
                ":desc" => $description,
            );
            
            $select = $this->connexion_bd->prepare ($requete) ;
            $select->execute ($data) or die("Echec de l'ajout ".__FILE__);
            echo "Bien localisé";
        }
    }

	function confirm_location($id, $confirmed)
    {
        // update location with confirm if admin confirm.
			
		if ($this->connexion_bd == null)
        {
            return null;
        } else {
            $requete = "update localisation set location_status = $confirmed WHERE id = $id ;";
            $select = $this->connexion_bd->prepare ($requete);
            $select->execute () or die("Echec de la confirmation ".__FILE__);
            echo "Modification Réussie";
        }
    }    
        

	function get_confirmed_locations()
    {
        // update location with location_status if admin location_status.
	   $requete = "select id ,lat,lng,description,location_status as isconfirmed
	               from localisation WHERE  location_status = 1; ";
		
        if ($this->connexion_bd == null)
        {
            return null;
        } else {
		
		 $select = $this->connexion_bd->prepare ($requete) ;
	     $select->execute();
		 $rows = $select->fetchAll(PDO::FETCH_NUM);

		$indexed = array_map('array_values', $rows);
		//  $array = array_filter($indexed);

		echo json_encode($indexed);
		if (!$rows) {
			return null;
            }
        }
	}

    
    function get_all_locations()
    {
        // update location with location_status if admin location_status.
        $requete = "select id ,lat, lng, description,location_status as isconfirmed from localisation;";
        if ($this->connexion_bd == null)
        {
            return null;
        } else {
            $select = $this->connexion_bd->prepare ($requete) ;
            $select->execute();
            $rows = $select->fetchAll(PDO::FETCH_NUM);
            
            $indexed = array_map('array_values', $rows);
            echo json_encode($indexed);
            if (!$rows) {
                return null;
                }
            }
	}
		
	function array_flatten($array) {
		if (!is_array($array)) {
			return FALSE;
		}
		$result = array();
		foreach ($array as $key => $value) {
			if (is_array($value)) {
				$result = array_merge($result, array_flatten($value));
			}
			else {
				$result[$key] = $value;
			}
		}
		return $result;
	}
}

// Gets data from URL parameters.
if(isset($_GET['add_location'])) {
    $dao = new dao_localisation();
    $dao -> ajouter_localisation($_GET['lat'], $_GET['lng'], $_GET['description']);
}
if(isset($_GET['confirm_location'])) {
    $dao = new dao_localisation();
    $dao -> confirm_location($_GET['id'], $_GET['confirmed']);
}





?>

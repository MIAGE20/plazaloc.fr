<?php
require_once '../../model/dao_admin.class.php';
$acces_bd = new dao_admin();

if (isset($_POST['delete'])) {
    $deleteProprietaire = $acces_bd->deleteProprietaire($_POST['idproprietaire']);
    echo '<script>alert ("Propriétaire supprimé avec succès!!!"); </script>';
}

if (isset($_POST['desactiver'])) {
    $desactiveProprietaire = $acces_bd->desactiveProprietaire($_POST['idproprietaire']);
    echo '<script>alert ("Propriétaire désactivé avec succès!!!"); </script>';
}

?>
<div class="row box" style="margin: 10px">
    <h3>LISTE DES PROPRIETAIRES</h3>
    <script type="text/javascript">
        function confirmSuppression() {
            if (confirm("Souhaitez-vous vraiment supprimer ce propriétaire?")) {
                return true;
            } else {
                return false;
            }
        }

        function setSelectedRow(idproprietaire) {
            var selectedHiddenInput = document.forms["deleteProprietaireForm"]["idproprietaire" + idproprietaire];
            selectedHiddenInput.value = idproprietaire;
            selectedHiddenInput.name = 'idproprietaire';
        }

    </script>
    <script type="text/javascript">
        function confirmDesactivation() {
            if (confirm("Souhaitez-vous vraiment desactiver ce propriétaire?")) {
                return true;
            } else {
                return false;
            }
        }

        function setSelectedRow2(idproprietaire) {
            var selectedHiddenInput = document.forms["desactiveProprietaireForm"]["idproprietaire" + idproprietaire];
            selectedHiddenInput.value = idproprietaire;
            selectedHiddenInput.name = 'idproprietaire';
        }

    </script>
    <div class="">
        <form method="post" action="" name="desactiveProprietaireForm" onsubmit='return confirmDesactivation()'>
            <table class="table table-striped">
                <tr>
                    <td> Id Propriétaire </td>
                    <td> Civilité </td>
                    <td> Nom </td>
                    <td> Prénom </td>
                    <td> Date de Naissance </td>
                    <td> Adresse </td>
                    <td> Téléphone </td>
                    <td> Email </td>
                    <td> Assurance </td>
                    <td> CCP </td>
                    <td> Désactiver </td>
                </tr>

                <?php

                $AllProprietaire = $acces_bd->selectAllProprietaire();

                foreach ($AllProprietaire as $prop) {
                    $idproprietaire=$prop['idutilisateur'];
                    echo "
                    <input type='hidden' name='idproprietaire".$idproprietaire."'/>
                        <tr>
                        <td>".$idproprietaire."</td>
						<td>".$prop['civilite']."</td>
						<td>".$prop['nom']."</td>
						<td>".$prop['prenom']."</td>
						<td>".$prop['datenaissance']."</td>
						<td>".$prop['adresse']."</td>
						<td>".$prop['numtel']."</td>
						<td>".$prop['mail']."</td>
						<td>".$prop['assurance']."</td>
						<td>".$prop['ccp']."</td>
						<td><button type='submit' name='desactiver' onclick='setSelectedRow2(".$idproprietaire.");' class='button-noborder'><span class='glyphicon glyphicon-remove'></span></button></td>
				  </tr>";
                }
                ?>
            </table>
        </form>
    </div>
    <div class="">
        <form method="post" action="" name="deleteProprietaireForm" onsubmit='return confirmSuppression()'>
            <table class="table table-striped">
                <tr>
                    <td> Id Propriétaire </td>
                    <td> Civilité </td>
                    <td> Nom </td>
                    <td> Prénom </td>
                    <td> Date de Naissance </td>
                    <td> Adresse </td>
                    <td> Téléphone </td>
                    <td> Email </td>
                    <td> Assurance </td>
                    <td> CCP </td>
                    <td> Supprimer </td>
                </tr>

                <?php

            $AllProprietaire = $acces_bd->selectAllProprietaire2();

            foreach ($AllProprietaire as $prop) {
                $idproprietaire=$prop['idutilisateur'];
                echo "
                    <input type='hidden' name='idproprietaire".$idproprietaire."'/>
                        <tr>
                        <td>".$idproprietaire."</td>
						<td>".$prop['civilite']."</td>
						<td>".$prop['nom']."</td>
						<td>".$prop['prenom']."</td>
						<td>".$prop['datenaissance']."</td>
						<td>".$prop['adresse']."</td>
						<td>".$prop['numtel']."</td>
						<td>".$prop['mail']."</td>
						<td>".$prop['assurance']."</td>
						<td>".$prop['ccp']."</td>
						<td><button type='submit' name='delete' onclick='setSelectedRow(".$idproprietaire.");' class='button-noborder'><span class='glyphicon glyphicon-remove'></span></button></td>
				  </tr>";
                      }
                ?>
            </table>
        </form>
    </div>
</div>

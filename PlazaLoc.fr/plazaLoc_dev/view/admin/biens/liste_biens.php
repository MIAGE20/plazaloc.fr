<?php
require_once '../../model/dao_admin.class.php';
$acces_bd = new dao_admin;
?>

<div class="row box" style="margin: 10px">
    <h3>LISTE DES BIENS</h3>
    <div class="box-content">
        <table class="table table-striped">
            <tr>
                <td> Id </td>
                <td> Type de bien </td>
                <td> Ville </td>
				<td> Adresse </td>
                <td> Superficie </td>
                <td> Nombre de pièce </td>
                <td> Etage  </td>
                <td> Description </td>
                <td> Montant du loyer </td>
				<td> Charge forfaitaire </td>
            </tr>
            <?php
            $bien = $acces_bd->selectAllBien();

            foreach ($bien as $bien) {
                echo "<tr> 
                    	<td>".$bien['idbien']."</td>
						<td>".$bien['idtypebien']."</td>
						<td>".$bien['nomville']."</td>
						<td>".$bien['adresse']."</td>
						<td>".$bien['superficie']."</td>
						<td>".$bien['nbpiece']."</td>
						<td>".$bien['etagebien']."</td>
						<td>".$bien['descriptionbien']."</td>
						<td>".$bien['montantLoyer']."</td>
						<td>".$bien['chargeforfaitaire']."</td>
				 </tr>";
            }
            ?>
        </table>
    </div>
</div>
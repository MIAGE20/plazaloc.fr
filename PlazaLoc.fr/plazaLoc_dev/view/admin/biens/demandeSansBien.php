<?php
require_once '../../model/dao_admin.class.php';
$acces_bd = new dao_admin();
if (isset($_POST['supprimer'])) {
    $acces_bd->deleteDemandeLocationPersonnalisee($_POST['iddemandelocationsansbien']);
}
?>
<div class="row box" style="margin: 30px 10px 10px 10px">
    <h3>DEMANDES PERSONNALISEES</h3>
    <div class="box-content">
        <form method="post" action="" class="col-lg-12" name="suppressionDemandeLocationForm" onsubmit="return confirmerSuppressionDemande();">
            <table class="table table-striped">
                <tr>
                    <td> N° Demande </td>
                    <td> Type de bien </td>
                    <td> Nombre de pièces </td>
                    <td> Motif de la location </td>
                    <td> Superficie </td>
                    <td> Montant du loyer </td>
                    <td> Date de début de location </td>
                    <td> Ville souhaitée </td>
                    <td> Supprimer </td>
                </tr>
                <?php

                $idlocataire = $_COOKIE["user_id"];
                $demandes = $acces_bd->selectAllDemandeLocationSansBien();

                foreach ($demandes as $demande) {
                    $iddemandelocationsansbien = $demande['iddemandelocationsansbien'];
                    echo "
                        <input type='hidden' name='iddemandelocationsansbien".$iddemandelocationsansbien."'/>
                        <tr> 	
                            <td>".$demande['iddemandelocationsansbien']."</td>
                            <td>".$demande['libelletype']."</td>
                            <td>".$demande['nbpiece']."</td>
                            <td>".$demande['descriptionmotif']."</td>
                            <td>".$demande['superficie']."</td>
                            <td>".$demande['prixloyer']."</td>
                            <td>".$demande['datedebut']."</td>
                            <td>".$demande['nomville']."</td>
                            <td><button type='submit' name='supprimer' onclick='setSelectedRow(".$iddemandelocationsansbien.");' class='button-noborder'><span class='glyphicon glyphicon-remove'></span></button></td>
                          </tr>";
                }
                ?>
            </table>
        </form>
    </div>
</div>

<script type="text/javascript">
    function confirmerSuppressionDemande() {
        if (confirm("Etes-vous sur de vouloir supprimer cette demande?")) {
            return true;
        } else {
            return false;
        }
    }

    function setSelectedRow(iddemandelocationsansbien) {
        var selectedHiddenInput = document.forms["suppressionDemandeLocationForm"]["iddemandelocationsansbien" + iddemandelocationsansbien];
        selectedHiddenInput.value = iddemandelocationsansbien;
        selectedHiddenInput.name = 'iddemandelocationsansbien';
    }

</script>

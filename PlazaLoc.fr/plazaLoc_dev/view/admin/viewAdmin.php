<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../../layout.css">
    <link href="../../node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>Plaza Loc</title>
</head>
<body>

<div class="logo-title-space">
    <a href=""><img src="../../images/logo.png" alt="" title=""></a>
    <h1><a href="viewAdmin.php?page=200">PLAZA LOC</a></h1>
</div>

<div class="banner-space">
    <h4>Bienvenue dans votre Espace Administrateur :
   <?php
            echo $_COOKIE['user_nom']." ".$_COOKIE['user_prenom'];
    ?>
    </h4>
</div>

<div class="row container-space" style="margin-left:0; margin-right: 0">
    <nav class="col-lg-2">
        <ul>
            <li><a href="../admin/viewAdmin.php?page=200"><span class="glyphicon glyphicon-dashboard"></span> Tableau de bord</a></li>
            <li><a href="../admin/viewAdmin.php?page=201"><span class="glyphicon glyphicon-user"></span> Liste des propriétaires</a></li>
            <li><a href="../admin/viewAdmin.php?page=202"><span class="glyphicon glyphicon-user"></span> Liste des locataires</a></li>
            <li><a href="../admin/viewAdmin.php?page=203"><span class="glyphicon glyphicon-home"></span> Liste des biens</a></li>
            <li><a href="../admin/viewAdmin.php?page=204"><span class="glyphicon glyphicon-file"></span> Nos locations</a></li>
            <li><a href="../admin/viewAdmin.php?page=207"><span class="glyphicon glyphicon-briefcase"></span> Nos contrats</a></li>
            <!-- <li><a href="../admin/viewAdmin.php?page=205"><span class="glyphicon glyphicon-folder-open"></span> Demandes de location</a></li> -->
            <li><a href="../admin/viewAdmin.php?page=206"><span class="glyphicon glyphicon-folder-open"></span> Demandes personnalisées</a></li>
            <li><a href="../admin/viewAdmin.php?page=208"><span class="glyphicon glyphicon-envelope"></span> Nos messages</a></li>
            <li><a href="../admin/viewAdmin.php?page=209"><span class="glyphicon glyphicon-stats"></span> Graphique ville Paris</a></li>
            <li><a href="../admin/viewAdmin.php?page=300"><span class="glyphicon glyphicon-map-marker"></span> Valider un bien sur la map </a></li>
            <li><a href="../admin/viewAdmin.php?page=999"><span class="glyphicon glyphicon-stats"></span> Vue BTS </a></li>
		</ul>
    </nav>
    
    <a href="../../index.php" class="button"><span class="glyphicon glyphicon-log-out"></span> Se déconnecter</a>
    <div class="col-lg-10" style="padding-left: 0; padding-right: 0;">
        <div class="row">
            <h3 class="title-space"><span class="glyphicon glyphicon-cog"></span>Gestion des propriétaires, locataires, biens</h3>
        </div>
        <div class="row container-boxes">

            <?php
            if (isset($_GET['page'])) {
                $page = $_GET['page'];
            }
            else {
                $page =  200;
            }

            switch ($page) {
                case 200:
                    include("tableauDeBord.php");
                    break;

                case 201:
                    include 'biens/liste_proprietaires.php';
                    break;

                case 202:
                    include 'locations/liste_locataires.php';
                    break;

                case 203:
                    include 'biens/liste_biens.php';
                    break;

                case 204:
                    include 'locations/liste_locations.php';
                    break;

                case 205:
                    include 'locations/liste_demandes_location.php';
                    break;
					
                case 206:
                    include 'biens/demandeSansBien.php';
                    break;
					
                case 207:
                    include 'contrats/liste_contrats.php';
                    break;
           
                case 208:
                    include 'gestion/liste_messages.php';
                    break;

                case 209:
                    include 'gestion/graphisme.php';
                    break;

                case 300:  
                    include 'gestion/validerLocalisation.php'; 
                    break;
                case 999:
                    include 'gestion/vusBTS.php';
                    break;


                default:
                    break;
            }
            ?>
        </div>
    </div>
</div>

</body>
</html>

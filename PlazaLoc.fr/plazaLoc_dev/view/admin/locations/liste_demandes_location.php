<?php
require_once '../../model/dao_admin.class.php';
$acces_bd = new dao_admin("localhost", "agence_plaza", "root", "");
?>

<div class="row box" style="margin: 10px">
    <h3>DEMANDES EN ATTENTE DE VALIDATION</h3>
    <div class="box-content">
        <table class="table table-striped">
            <tr>
                <td> Id Demande </td>
                <td> Date de la demande </td>
                <td> Id Bien </td>
                <td> Id Locataire </td>
            </tr>
            <?php
            $demandes = $acces_bd->selectAllDemandeLocation();

            foreach ($demandes as $demande) {
                echo "<tr> 	
                        <td>".$demande['iddemandelocation']."</td>
						<td>".$demande['datedemande']."</td>
						<td>".$demande['idbien']."</td>
						<td>".$demande['idlocataire']."</td>
				      </tr>";
            }
            ?>
        </table>
    </div>
</div>
<?php


require_once '../../model/dao_admin.class.php';

$acces_bd = new dao_admin();


if (isset($_POST['delete'])) {
    $deleteLocataire = $acces_bd->deleteLocataire($_POST['idlocataire']);
    echo '<script>alert ("Locataire supprimé avec succès!"); </script>';
}

?>

<div class="row box" style="margin: 10px">
    <h3>LISTE DES LOCATAIRES</h3>

    <script type="text/javascript">
        function confirmSuppression() {
            if (confirm("Etes-vous sur de vouloir supprimer ce client?")) {
                return true;
            } else {
                return false;
            }
        }

        function setSelectedRow(idlocataire) {
            var selectedHiddenInput = document.forms["deleteLocataireForm"]["idlocataire_" + idlocataire];
            selectedHiddenInput.value = idlocataire;
            selectedHiddenInput.name = 'idlocataire';
        }

    </script>

    <div class="">
        <form method="post" action="" name="deleteLocataireForm" onsubmit='return confirmSuppression()'>
            <table class="table table-striped">
                <tr>
                    <td> Id Locataire </td>
                    <td> Civilité </td>
                    <td> Nom </td>
                    <td> Prénom </td>
                    <td> Date de naissance </td>
                    <td> Adresse </td>
                    <td> Téléphone </td>
                    <td> Email </td>
                    <td> CCP </td>
                    <td> Supprimer </td>
                </tr>

                <?php

                $allLocataires = $acces_bd->selectAllLocataire();

                foreach ($allLocataires as $loc) {
                    $idlocataire=$loc['idutilisateur'];
                    echo "
                        <input type='hidden' name='idlocataire_".$idlocataire."'/>
                        <tr> 	
                            <td>".$idlocataire."</td>
    						<td>".$loc['civilite']."</td>
    						<td>".$loc['nom']."</td>
    						<td>".$loc['prenom']."</td>
    						<td>".$loc['datenaissance']."</td>
    						<td>".$loc['adresse']."</td>
    						<td>".$loc['numtel']."</td>
    						<td>".$loc['mail']."</td>
							<td>".$loc['ccp']."</td>
    						<td><button type='submit' name='delete' onclick='setSelectedRow(".$idlocataire.")' class='button-noborder'><span class='glyphicon glyphicon-remove'></span></button></td>
    				  </tr>";
                }
                ?>
            </table>
        </form>
    </div>
</div>

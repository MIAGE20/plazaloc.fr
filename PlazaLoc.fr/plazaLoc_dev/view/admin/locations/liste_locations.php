<?php
require_once '../../model/dao_admin.class.php';
$acces_bd = new dao_admin();
?>

<div class="row box" style="margin: 10px">
    <h3>LISTE DES LOCATIONS</h3>
    <div class="box-content">
          <table class="table table-striped">
                <tr>
                    <td> Id Location </td>
                    <td> Début </td>
                    <td> Fin </td>
                    <td> Montant </td>
                    <td> Description bien </td>
                    <td> Motif de la location </td>
                </tr>
                
                <?php
                $locations = $acces_bd->selectAllLocation();

                foreach ($locations as $location) {
                    $idlocation = $location['idlocation'];
                    echo "
                        <input type='hidden' name='idlocation".$idlocation."'/>
                        <tr> 	
                        <td>".$location['idlocation']."</td>
						<td>".$location['datedebut']."</td>
						<td>".$location['datefin']."</td>
						<td>".$location['montantLoyer']."</td>
                        <td>".$location['descriptionbien']."</td>
                        <td>".$location['descriptionmotif']."</td>
                        </tr>";
                }
                ?>
            </table>
        </form>
    </div>
</div>



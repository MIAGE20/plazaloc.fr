<?php
require_once '../../model/dao_admin.class.php';
$acces_bd = new dao_admin("localhost", "agence_plaza", "root", "");
?>

<div class="row">
    <h3 class="title-page">TABLEAU DE BORD</h3>
    <div class=" col-lg-3 box">
        <div class="container-icon">
            <span class="glyphicon glyphicon-user"></span>
        </div>
        <div class="box-content">
        <span class="number">
            <?php
            $nbProprietaires = $acces_bd->countProprietaires();
            echo $nbProprietaires;
            ?>
        </span>
            <div class="desc-box">
                <h4>PROPRIETAIRES ENREGISTRES</h4>
                <a href="../admin/viewAdmin.php?page=201">Visualisez tous les propriétaires</a>
            </div>
        </div>
    </div>
    <div class=" col-lg-3 box">
        <div class="container-icon">
            <span class="glyphicon glyphicon-user"></span>
        </div>
        <div class="box-content">
        <span class="number">
            <?php
            $nbLocataires = $acces_bd->countLocataires();
            echo $nbLocataires;
            ?>
        </span>
            <div class="desc-box">
                <h4>LOCATAIRES ENREGISTRES</h4>
                <a href="../admin/viewAdmin.php?page=202">Visualisez tous les locataires</a>
            </div>
        </div>
    </div>


    <div class=" col-lg-3 box">
        <div class="container-icon">
            <span class="glyphicon glyphicon-home"></span>
        </div>
        <div class="box-content">
        <span class="number">
            <?php
            $nbBiens = $acces_bd->countBiens();
            echo $nbBiens;
            ?>
        </span>
            <div class="desc-box">
                <h4>BIENS ENREGISTRES</h4>
                <a href="../admin/viewAdmin.php?page=203">Visualisez tous les biens</a>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class=" col-lg-3 box">
        <div class="container-icon">
            <span class="glyphicon glyphicon-file"></span>
        </div>
        <div class="box-content">
        <span class="number">
            <?php
            $nbLocations = $acces_bd->countLocations();
            echo $nbProprietaires;
            ?>
        </span>
            <div class="desc-box">
                <h4>LOCATIONS ENREGISTREES</h4>
                <a href="../admin/viewAdmin.php?page=204">Visualisez toutes les locations</a>
            </div>
        </div>
    </div>
    <div class=" col-lg-3 box">
        <div class="container-icon">
            <span class="glyphicon glyphicon-briefcase"></span>
        </div>
        <div class="box-content">
        <span class="number">
            <?php
            $nbContrat = $acces_bd->countContrats();
            echo $nbContrat;
            ?>
        </span>

            <div class="desc-box">
                <h4>CONTRATS ENREGISTRES</h4>
                <a href="../admin/viewAdmin.php?page=207">Visualisez tous les contrats signés</a>
            </div>
        </div>
    </div>


    <div class=" col-lg-3 box">
        <div class="container-icon">
            <span class="glyphicon glyphicon-folder-open"></span>
        </div>
        <div class="box-content">
        <span class="number">
            <?php
            $nbContrat = $acces_bd->countContrats();
            echo $nbContrat;
            ?>
        </span>

            <div class="desc-box">
                <h4>DEMANDES PERSONNALISÉES</h4>
                <a href="../admin/viewAdmin.php?page=206">Visualisez toutes les demande personnalisées</a>
            </div>
        </div>
    </div>

    <div class=" col-lg-3 box">
        <div class="container-icon">
            <span class="glyphicon glyphicon-envelope"></span>
        </div>
        <div class="box-content">
        <span class="number">
            <?php
            $nbMessages = $acces_bd->countMessages();
            echo $nbMessages;
            ?>
        </span>
            <div class="desc-box">
                <h4>MESSAGES RECUS</h4>
                <a href="../admin/viewAdmin.php?page=208">Accédez et répondez à vos messages</a>
            </div>
        </div>
    </div>
    
    <div class=" col-lg-3 box">
        <div class="container-icon">
            <span class="glyphicon glyphicon-envelope"></span>
        </div>
        <div class="box-content">
    <span class="number">
            <?php
            $nbLocalisation = $acces_bd->countLocalisation();
            echo $nbLocalisation;
            ?>
    </span>
            <div class="desc-box">
                <h4>LOCALISATION A VALIDER</h4>
                <a href="../admin/viewAdmin.php?page=300">Accédez et valider des biens</a>
            </div>
        </div>
    </div>

    <div class=" col-lg-3 box">
        <div class="container-icon">
            <span class="glyphicon glyphicon-envelope"></span>
        </div>
        <div class="box-content">
    <span class="number">
            <?php
            $nbLocations = $acces_bd->countLocations();
            echo $nbLocations;
            ?>
    </span>
            <div class="desc-box">
                <h4>STATISTIQUES PLAZA</h4>
                <a href="../admin/viewAdmin.php?page=999"> Vues BTS</a>
            </div>
        </div>
    </div>

 </div>

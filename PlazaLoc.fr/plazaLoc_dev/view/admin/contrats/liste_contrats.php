<?php
require_once '../../model/dao_admin.class.php';
$acces_bd = new dao_admin();
?>

<div class="row box" style="margin: 10px">
    <h3>LISTE DES CONTRATS</h3>
    <div class="box-content">
        <table class="table table-striped">
            <tr>
                <td> Id contrat </td>
                <td> Date debut contrat </td>
                <td> Date fin contrat </td>
                <td> Prix loyer </td>
                <td> Mail locataire </td>
                <td> Mail proprietaire </td>
            </tr>
            
            <?php
            
            $contrats = $acces_bd->selectAllContrat();

            foreach ($contrats as $contrat) {
                $idcontrat = $contrat['idcontrat'];
                echo "
                    <input type='hidden' name='idcontrat".$idcontrat."'/>
                    <tr> 
                    	<td>".$contrat['idcontrat']."</td>
						<td>".$contrat['datedebut']."</td>
						<td>".$contrat['datefin']."</td>
						<td>".$contrat['prixloyer']."</td>
						<td>".$contrat['locMail']."</td>
						<td>".$contrat['proMail']."</td>
				 </tr>";
            }
            ?>
        </table>
    </div>
</div>


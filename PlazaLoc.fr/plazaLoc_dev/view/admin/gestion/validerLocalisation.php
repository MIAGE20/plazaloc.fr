<?php 
require_once ('../../model/dao_localisation.class.php');
?>


<div id="map"></div>

<!------ Include the above in your HEAD tag ---------->
<script>
    var map;
    var marker;
    var infowindow;
    var red_icon =  'http://maps.google.com/mapfiles/ms/icons/red-dot.png' ;
    var purple_icon =  'http://maps.google.com/mapfiles/ms/icons/purple-dot.png' ;
    
    var locations =  <?php echo ((new dao_localisation())->get_all_locations()) ?>;
    

    function initMap() {
        var paris = {lat: 48.8461, lng: 2.3548};
        infowindow = new google.maps.InfoWindow();
        map = new google.maps.Map(document.getElementById('map'), {
            center: paris,
            zoom: 12
        });


        var i ; var confirmed = 0;
        for (i = 0; i < locations.length; i++) {

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map,
                icon :   locations[i][4] === '1' ?  red_icon  : purple_icon,
                html: document.getElementById('form')
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    confirmed =  locations[i][4] === '1' ?  'checked'  :  0;
                    $("#confirmed").prop(confirmed,locations[i][4]);
                    $("#id").val(locations[i][0]);
                    $("#description").val(locations[i][3]);
                    $("#form").show();
                    infowindow.setContent(marker.html);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }
    }

    function saveData() {
        var id = document.getElementById('id').value;
        var confirmed = document.getElementById('confirmed').checked ? 1 : 0;
       
        var url = '../../model/dao_localisation.class.php?confirm_location&id=' + id + '&confirmed=' + confirmed ;
        downloadUrl(url, function(data, responseCode) {
            if (responseCode === 200  && data.length > 1) {
                alert(data);
                infowindow.close();
                window.location.reload(true);
            }else{
                alert("ERROR::: " + responseCode);
                infowindow.setContent("<div style='color: purple; font-size: 25px;'>Erreur d'insertion</div>");
            }
        });
    }


    function downloadUrl(url, callback) {
        var request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function() {
            if (request.readyState == 4) {
                callback(request.responseText, request.status);
            }
        };

        request.open('GET', url, true);
        request.send(null);
    }


</script>

<div style="display: none" id="form">
    <table class="map1">
        <tr>
            <input name="id" type='hidden' id='id' />
            <td><a>Déscription:</a></td>
            <td><textarea disabled id='description' placeholder='Description'></textarea></td>
        </tr>
        <tr>
            <td><b>Confirmer ? :</b></td>
            <td><input id='confirmed' type='checkbox' name='confirmed'></td>
        </tr>

        <tr>
            <td></td>
            <td><input type='button' value='Sauvegarder' onclick='saveData()' /></td>
        </tr>
    </table>
</div>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5aCHMhPs-mlL7JTlQpGAlQq0I2m2hODI&callback=initMap">
</script>

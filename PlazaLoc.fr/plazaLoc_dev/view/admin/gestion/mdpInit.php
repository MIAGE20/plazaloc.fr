<?php
require_once 'model/dao_admin.class.php';
$acces_bd = new dao_admin("localhost", "agence_plaza", "root", "");
?>


<div class="row banner-title banner-title-contact" style="margin-left: 0; margin-right: 0">
    <h1> Renitialisez votre mot de passe</h1>
</div>

<div class="row" style="margin-left: 0; margin-right: 0">
    <div class="container-form">
        <form method="post" action="" class="col-lg-6" name="mdpForm" onsubmit="return validateMdpForm()">
            <div class="form-group">
                <label>Saisir votre adresse Email *:</label>
                <input type="email" name="email" class="form-control">
            </div>
            <div class="form-group">
                <label>Saisir votre nouveau MDP *:</label>
                <input type="mdp1" name="mdp1" class="form-control">
            </div>
            <div class="form-group">
                <label>Veuillez le confirmer SVP *:</label>
                <input type="mdp2" name="mdp2" class="form-control">
            </div>
            <div class="container-button-form">
                <button type="submit" class="button" name='valider'>Renitialiser</button>
            </div>
        </form>
        <script type="text/javascript">
            function validateMdpForm() {
                var email = document.forms["mdpForm"]["email"].value;
                var mdp1 = document.forms["mdpForm"]["mdp1"].value;
                var mdp2 = document.forms["mdpForm"]["mdp2"].value;
                if (email == "" ||mdp1 == "" || mdp2 == "") {
                    alert ("Veuillez remplir tous les champs du formulaire");
                    return false;
                } else {
                    return true;
                }
            }
        </script>
        <?php
        if (isset($_POST['valider'])) {
            if ($_POST['mdp1'] != $_POST['mdp2'])
            {
                echo '<script>
                     alert ("Mots de passe non identiques!");
                         </script>';
            }else{
                $acces_bd->modifierMdp($_POST["email"], $_POST["mdp1"]);
                echo '<script>
                            alert("MDP renitialise avec succes");
                          </script>';
            }
        }
        ?>

    </div>
    ?>
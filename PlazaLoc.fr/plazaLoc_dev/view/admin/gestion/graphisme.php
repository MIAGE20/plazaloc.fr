<?php 
require_once("../../model/dao_admin.class.php");
$acces_bd = new dao_admin();
$stat_demandes = $acces_bd->graphisme();

/*
echo '<pre>';
foreach ($stat_demandes as $stat) {
	print_r ($stat);
	echo '<br/><br/>';
}

// print_r ($res);
echo '</pre>';
*/



?>

    <html>
      <head>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">
          google.charts.load('current', {'packages':['corechart']});
          google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
          var data = google.visualization.arrayToDataTable([
            ['nomville', 'nb_demande'],
            
            <?php 
            foreach ($stat_demandes as $stat)
            {
                echo "['".$stat['nomville']."',".$stat['nb_demande']."],";
            }
            ?>

          ]);
          var options = {
           title: 'Arrondissements de Paris les plus demandés',
            is3D:true,
           };

          var chart = new google.visualization.PieChart(document.getElementById('piechart'));
          chart.draw(data, options);
      }
      
    </script>
  </head>
  <body>
    <div id="piechart" style="width: 900px; height: 500px;"></div>
  </body>
</html>
<?php
require_once '../../model/dao_admin.class.php';
$acces_bd = new dao_admin;
?>

<div class="row box" style="margin: 10px">
    <h3>Liste  des biens loues ordonnee par m2</h3>
    <div class="box-content">
        <table class="table table-striped">
            <tr>
                <td><b>Ville</b></td>
				<td><b>Quartier</b></td>
                <td><b>Prix loyer</b></td>
                <td><b>Superficie</b></td>
                <td><b>Description du bien </b></td>
                <td><b>MontantLoyer</b></td>
                <td><b>Prix moyen du loyer au M2</b></td>
            </tr>
            <?php
            $bien = $acces_bd->trouveBienM2();

            foreach ($bien as $bien) {
                echo "<tr> 
                       	<td>".$bien['titlebien']."</td>
						<td>".$bien['nomville']."</td>
						<td>".$bien['nomquartier']."</td>
						<td>".$bien['montantLoyer']."</td>
						<td>".$bien['superficie']."</td>
						<td>".$bien['montantLoyer']."</td>
						<td>".$bien['prixMoyen']."</td>
				 </tr>";
            }
            ?>
        </table>
    </div>
</div>
<div class="row box" style="margin: 10px">
    <h3>Liste des biens loues par an</h3>
     <div class="box-content">
        <table class="table table-striped">
            <tr>
                <td><b>Nombre de biens loues </b></td>
                <td><b>Ville </b></td>
                <td><b>Par an </b></td>
            </tr>
            <?php
            $bien = $acces_bd->trouveBienAn();

            foreach ($bien as $bien) {
                echo "<tr> 
                       	<td>".$bien['nb']."</td>
                       	<td>".$bien['nomville']."</td>
  						<td>".$bien['parAn']."</td>
			         </tr>";
            }
            ?>
        </table>
    </div>
</div>
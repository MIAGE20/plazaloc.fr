<?php
require_once '../../model/dao_admin.class.php';
$acces_bd = new dao_admin();

function showListeMessages($messages) {
    foreach ($messages as $message) {
        $idMessage = $message['idmessage'];
        echo "
        <input type='hidden' name='idmessage_".$idMessage."' value='".$idMessage."' />
        <tr>    
            <td>".$idMessage."</td>
            <td>".$message['nom']."</td>
            <td>".$message['prenom']."</td>
            <td>".$message['sujet']."</td>
            <td>".$message['mail']."</td>
            <td>".$message['numtel']."</td>
            <td>".$message['textemessage']."</td>
            <td><button type='submit' name='repondre' onclick='setSelectedRow(".$idMessage.", \"deleteReplyForm\")' class='button-noborder'><span class='glyphicon glyphicon-edit'></span></button></td>
        </tr>
        ";
    }
}
        if (isset($_POST['deleteMessage'])) {
            $deleteMessage = $acces_bd->deleteMessageEnvoye($_POST['idmessage']);
            echo '<script>alert ("Message supprimé avec succès!"); </script>';
        }

?>

<div class="row box" style="margin: 10px">
    <h3>MESSAGERIE</h3>

    <div class="container-messagerie">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#reception"><span class="glyphicon glyphicon-envelope"></span> Boîte de réception</a></li>
            <li><a data-toggle="tab" href="#envoyes"><span class="glyphicon glyphicon-pencil"></span> Messages envoyés</a></li>
        </ul>

        <div class="tab-content">
            <div id="reception" class="tab-pane fade in active">
                <script type="text/javascript">
                    function setSelectedRow(messageID, formName) {
                        var selectedHiddenInput = document.forms[formName]["idmessage_" + messageID];
                        selectedHiddenInput.value = messageID;
                        selectedHiddenInput.name = 'idmessage';
                    }

                </script>
                <form method="post" action="" name="deleteReplyForm">
                    <table class="table table-striped">
                        <tr>
                            <td> N° </td>
                            <td> Nom </td>
                            <td> Prénom </td>
                            <td> Sujet </td>
                            <td> Email </td>
                            <td> Téléphone </td>
                            <td> Message </td>
                            <td> Répondre </td>
                        </tr>
                        <?php

                        $messages = $acces_bd->selectAllmessages();
                        showListeMessages($messages);

                        
                        ?>
                    </table>
                </form>

                <?php
                    if (isset($_POST['repondre'])) {
                        $unMessage = $acces_bd->selectOnmessages($_POST['idmessage']);
                        echo "
                            <form method='post' action='' name='replyForm'>
                                <input type='hidden' name='idmessage' value='".$unMessage['idmessage']."' />
                                <div class='form-row' style='margin-top: 30px'>
                                    <div class='form-center'>
                                        <div class='row'>
                                            <div class='form-group'>
                                                <label>Destinataire:</label>
                                                <input type='text' name='mail' value='".$unMessage['mail']."' class='form-control'>
                                            </div>
                                        </div>
                                         <div class='row'>
                                            <div class='form-group'>
                                                <label>Sujet:</label>
                                                <input type='text' name='sujet' value='".$unMessage['sujet']."' class='form-control'>
                                            </div>
                                        </div>
                                        <div class='row'>
                                            <div class='form-group'>
                                                <textarea rows='7' class='form-control' name='textemessage'></textarea>
                                            </div>
                                        </div>
                                        <div class='row container-button-form'>
                                            <button type='submit' name='envoyerReponse' class='button' style='position: relative; margin-bottom: 30px'>Envoyer</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        ";
                    }


                    if (isset($_POST['envoyerReponse'])) {

                        require '../../PHPMailerAutoload.php';
                        require '../../../credential.php';

                        $mail = new PHPMailer;

                        $mail->SMTPDebug = 0;                               // Enable verbose debug output

                        $mail->isSMTP();                                      // Set mailer to use SMTP
                        $mail->Host = 'smtp.gmail.com';                      // Specify main and backup SMTP servers
                        $mail->SMTPAuth = true;                               // Enable SMTP authentication
                        $mail->Username = EMAIL;                           // SMTP username
                        $mail->Password = PASS;                           // SMTP password
                        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                        $mail->Port = 587;                                    // TCP port to connect to


                        $mail->setFrom(EMAIL, 'agence_plaza');
                        $mail->addAddress($_POST['mail']);     // Add a recipient

                        $mail->addReplyTo(EMAIL);
                        //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments

                        $mail->isHTML(true);                                  // Set email format to HTML

                        $mail->Subject = "No reply:".$_POST['sujet'];
                        //$mail->Subject = "No reply: Demande d'information";
                        $mail->Body    = $_POST['textemessage']; 

                        if(!$mail->send()) {
                            echo '<pre>Message could not be sent.';
                            echo 'Mailer Error: ' . $mail->ErrorInfo;
                            die ("QUIT: ".__FILE__.'</pre>');
                        } else {
                            $acces_bd->repondre($_POST['idmessage'], $_POST['mail'], $_POST['sujet'], $_POST['textemessage']);
                            echo '<script>
                                    alert("Message envoyé avec succès");
                                    window.location.href="viewAdmin.php?page=208";
                                </script>

                            ';
                        }
                    }      

                ?>
            </div>

            <div id="envoyes" class="tab-pane fade">
                <form method='post' action='' name='deleteMessageLuForm'>
                    <table class="table table-striped">
                        <tr>
                            <td> N° </td>
                            <td> Date de reception </td>
                            <td> Date de reponse </td>
                            <td> Sujet </td>
                            <td> Mail </td>
                            <td> Message envoyé </td>
                            <td> Supprimer </td>
                        </tr>
                        <?php
                        $messages = $acces_bd->getListeMessageEnvoye();

                        foreach ($messages as $message) {
                            $idMessage = $message['idmessage'];
                            echo "
                            <input type='hidden' name='idmessage_".$idMessage."' value='".$idMessage."' />
                            <tr> 	
                                <td>".$message['idmessage']."</td>
                                <td>".$message['datereception']."</td>
                                <td>".$message['datereponse']."</td>
                                <td>".$message['sujet']."</td>
								<td>".$message['mail']."</td>
								<td>".$message['textemessage']."</td>
                                <td><button type='submit' name='deleteMessageLu' onclick='setSelectedRow(".$idMessage.", \"deleteMessageLuForm\")' class='button-noborder'><span class='glyphicon glyphicon-remove'></span></button></td>
                             </tr>";

                            if (isset($_POST['deleteMessageLu'])) {
                                $deleteMessage = $acces_bd->deleteMessageEnvoye($message['idmessage']);
                                echo '<script>alert ("Message supprimé avec succès!");
											  window.location.href="viewAdmin.php?page=208";
									  </script>';
                                return true;
                            }
                        }
                        ?>
                    </table>
                </form>

            </div>
        </div>
    </div>
</div>

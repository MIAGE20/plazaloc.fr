<?php
    require_once '../../model/dao_locataire.class.php';
    $acces_bd = new dao_locataire();
    $unBien = $acces_bd->selectOnBien ($_GET['idbien']);
?>

<div class="container-bien">
    <div class="col-lg-6">
        <img src="../../images/<?= $unBien['image'] ?>">
    </div>
    <div class="col-lg-6 page-bien-desc">
        <h3><span class="glyphicon glyphicon-home"></span>
            <?= $unBien['titlebien'] ?>
        </h3>
        <p>
            <?= $unBien['descriptionbien'] ?>
        </p>

        <h4 class="page-bien-title">Détails</h4>
        <table class="table table-striped">
            <tbody>
                <tr>
                    <td>Réf. du bien</td>
                    <td><input type="text" name="idbien" value="<?= $unBien['idbien'] ?>" class="table-input"></td>
                </tr>
                <tr>
                    <td>Nombre de pièce(s)</td>
                    <td>
                        <?= $unBien['nbpiece'] ?>
                    </td>
                </tr>
                <tr>
                    <td>Surface</td>
                    <td>
                        <?= $unBien['superficie'] ?> m²</td>
                </tr>
                <tr>
                    <td>Etage</td>
                    <td>
                        <?= $unBien['etagebien'] ?>
                    </td>
                </tr>
                <tr>
                    <td>Montant Loyer</td>
                    <td>
                        <?= $unBien['montantLoyer'] ?> € 
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
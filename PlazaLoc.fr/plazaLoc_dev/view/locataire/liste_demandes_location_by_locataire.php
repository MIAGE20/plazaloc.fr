<?php
    require_once ("../../model/dao_locataire.class.php");

    $acces_bd = new dao_locataire();
    if (isset($_POST['supprimer'])) {
        $acces_bd->deleteDemandeLocation($_POST['iddemandelocation']);
    }
?>

<div class="row box" style="margin: 10px">
    <h3>DEMANDES EN ATTENTE DE VALIDATION</h3>
    
    <div class="box-content">
        <script type="text/javascript">

                function confirmerSuppressionDemande (){
                    if (confirm("Etes-vous sur de vouloir supprimer cette demande?")) {
                        return true;
                    } else {
                        return false;
                    }
                }

                function setSelectedRow(iddemandelocation) {
                    var selectedHiddenInput = document.forms["suppressionDemandeLocationForm"]["iddemandelocation"+iddemandelocation];
                    selectedHiddenInput.value = iddemandelocation;
                    selectedHiddenInput.name = 'iddemandelocation';
                }
        </script>
    
        <form method="post" action="" class="col-lg-12" name="suppressionDemandeLocationForm" onsubmit="return confirmerSuppressionDemande();">
            <table class="table table-striped">
                <tr>
                    <td> Référence du bien </td>
                    <td> Date de la demande </td>
                    <td> Date début location</td>
                    <td> Date fin location</td>
                    <td> Motif location </td>
                    <td> Supprimer </td>
                </tr>
                <?php
            
                $idLocataire = $_COOKIE["user_id"];
                $demandes = $acces_bd->selectAllDemandeLocationByLocataire($idLocataire);
                
                foreach ($demandes as $demande) {
                    $iddemandelocation = $demande['iddemandelocation'];
                    $idbien = $demande['idbien'];
                    echo "
                        <input type='hidden' name='iddemandelocation".$iddemandelocation."'/>
                        <tr> 	
                           
                            <td>
                                <a href='viewLocataire.php?page=306&idbien=".$idbien."'>".$idbien."</a>
                            </td>
                            <td>".$demande['datedemande']."</td>
                            <td>".$demande['datedebut']."</td>
                            <td>".$demande['datefin']."</td>
                            <td>".$demande['descriptionmotif']."</td>
                            <td><button type='submit' name='supprimer' onclick='setSelectedRow(".$iddemandelocation.");' class='button-noborder'><span class='glyphicon glyphicon-remove'></span></button></td>
                        </tr>";
                }
                ?>
            </table>
        </form>
    </div>
</div>


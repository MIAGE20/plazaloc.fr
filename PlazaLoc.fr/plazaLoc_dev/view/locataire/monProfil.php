<?php
require_once '../../model/dao_locataire.class.php';
require_once '../../services/user_services.class.php';
$acces_bd = new dao_locataire();

if (isset($_POST['valider'])) {
    $profilLocataire = array_merge($_POST, array("idlocataire" => $_COOKIE['user_id']));
    $acces_bd->modifyLocataire($profilLocataire);
    
    UserService::updateUser($_POST['nom'], $_POST['prenom']);

    echo '<script>alert ("Votre profil a été modifié avec succès!"); 
        refresViewOnProfileUpdate();
    </script>';
}


$unLocataire = $acces_bd->selectOnLocataire($_COOKIE['user_id']);
?>

<h3 class="title-page">MODIFIER MON PROFIL</h3>

<div class="row">
    <form method="post" action="" class="modif-profil container-form">
        <div class="form-row">
            <div class="row">
                <div class="col-lg-12">
                    <label class="civilite">Civilité:</label>
                    <select name="civilitelocataire" class="civilite">
                        <option selected value="<?= $unLocataire['civilite'] ?>"><?= $unLocataire['civilite'] ?></option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Nom:</label>
                    <input type="text" name="nom" class="form-control" value="<?= $unLocataire['nom'] ?>">
                </div>
                <div class="form-group col-md-6">
                    <label>Prénom:</label>
                    <input type="text" name="prenom" class="form-control" value="<?= $unLocataire['prenom'] ?>">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Date de naissance:</label>
                    <input type="text" name="datenaissance" class="form-control" value="<?= $unLocataire['datenaissance'] ?>">
                </div>
                <div class="form-group col-md-6">
                    <label>Adresse:</label>
                    <input type="text" name="adresse" class="form-control" value="<?= $unLocataire['adresse'] ?>">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Téléphone:</label>
                    <input type="text" name="numtel" class="form-control" value="<?= $unLocataire['numtel'] ?>">
                </div>
                <div class="form-group col-md-6">
                    <label>Email:</label>
                    <input type="text" name="mail" class="form-control" value="<?= $unLocataire['mail'] ?>">
                </div>
            </div>
			<div class="form-group col-md-6">
				<label>CCP:</label>
				<input type="text" name="ccp" class="form-control" value="<?= $unLocataire['ccp'] ?>">
			</div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Mot de passe:</label>
                    <input type="password" name="mdp" class="form-control" value="<?= $unLocataire['mdp'] ?>">
                </div>
            </div>
            <div class="container-button-form">
                <button type="submit" class="button" name="valider">Modifier</button>
            </div>
        </div>
    </form>
</div>
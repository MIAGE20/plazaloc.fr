<?php
require_once '../../model/dao_locataire.class.php';
$acces_bd = new dao_locataire();
?>

<div class="row">
    <h3 class="title-page">TABLEAU DE BORD</h3>
   
    <div class=" col-lg-3 box">
        <div class="container-icon">
            <span class="glyphicon glyphicon-home"></span>
        </div>
        <div class="box-content">
            <div class="desc-box desc-box-locataire desc-box-biens">
                <h4>CATALOGUE DES BIENS</h4>
                <a href="../locataire/viewLocataire.php?page=302">Visualisez tous les biens disponibles à la location</a>
            </div>
        </div>
    </div>

    <div class=" col-lg-3 box">
        <div class="container-icon">
            <span class="glyphicon glyphicon-folder-open"></span>
        </div>
        <div class="box-content">
          <!-- <span class="number">

            <?php
            $idLocataire = $_COOKIE["user_id"]; // a corriger plus tard
            $nbDemandesLocations = $acces_bd->countDemandesLocationByLocataire($idLocataire);
            echo $nbDemandesLocations;
            ?>

        </span> -->
           <div class="desc-box desc-box-locataire desc-box-biens">
                <h4>MES DEMANDES ENVOYEES </h4>
                <a href="../locataire/viewLocataire.php?page=303">Visualisez toutes vos demandes de location</a>
            </div>
        </div>
    </div>

    <div class=" col-lg-3 box">
        <div class="container-icon">
            <span class="glyphicon glyphicon-edit"></span>
        </div>
        <div class="box-content">
            <div class="desc-box desc-box-locataire desc-box-new-demand">
                <h4>RECHERCHER UNE LOCATION</h4>
                <a href="../locataire/viewLocataire.php?page=304">Effectuez une demande de location personnalisée</a>
            </div>
        </div>
    </div>
</div>
 <div class="row">
   <!-- <div class=" col-lg-3 box">
        <div class="container-icon">
            <span class="glyphicon glyphicon-briefcase"></span>
        </div>
        <div class="box-content">
        <span class="number">
            <?php
            $idLocataire = $_COOKIE["user_id"];
            $nbContratByLocataire = $acces_bd->countAllContratByLocataire($idLocataire);
            echo $nbContratByLocataire;
            ?>
        </span>
            <div class="desc-box">
                <h4>DEMANDES ENVOYEES</h4>
                <a href="../locataire/viewLocataire.php?page=303">Visualisez toutes vos demandes de location</a>
            </div>
        </div>
    </div> -->
</div>

    



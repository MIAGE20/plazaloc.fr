<?php
require_once ("../../model/dao_locataire.class.php");

    if (isset($_POST['delete'])) {
    $acces_bd->deleteLocation($_POST['idlocation']);
    echo '<script>alert ("Contrat résilié avec succès!"); </script>';
}

$acces_bd = new dao_locataire();
?>
<div class="row box" style="margin: 10px">
    <h3>LOCATIONS EFFECTUEES</h3>
    <div class="box-content">
        <script type="text/javascript">

            function confirmerResiliation (){
                if (confirm("Etes-vous sur de vouloir résilier cette location?")) {
                    return true;
                } else {
                    return false;
                }
            }

            function setSelectedRow(idlocation) {
                var selectedHiddenInput = document.forms["resiliationLocationForm"]["idlocation"+idlocation];
                selectedHiddenInput.value = idlocation;
                selectedHiddenInput.name = 'idlocation';
            }
        </script>
    
        <form method="post" action="" class="col-lg-12" name="resiliationLocationForm" onsubmit="return confirmerResiliation();">
            <table class="table table-striped">
                <tr>
                    <td> Id contrat </td>
                    <td> Date debut contrat </td>
                    <td> Date fin contrat </td>
                    <td> Prix loyer </td>
                    <td> Id bien </td>
                    <td> Résilier </td>
                </tr>
                <?php
                $idLocataire = $_COOKIE["user_id"];
                $locations = $acces_bd->selectAllLocationsByLocataire($idLocataire);

                foreach ($locations as $location) {
                    $idlocation = $location['idlocation'];
                    echo "
                        <input type='hidden' name='idlocation".$idlocation."' />
                        <tr> 
                            <td>".$idlocation."</td>
                            <td>".$location['datedebut']."</td>
                            <td>".$location['datefin']."</td>
                            <td>".$location['prixloyer']."</td>
                            <td>".$location['idbien']."</td>
                            <td><button type='submit' name='delete' onclick='setSelectedRow(".$idlocation.");' class='button-noborder'><span class='glyphicon glyphicon-remove'></span></button></td>
                    </tr>";
                }
                ?>
            </table>
        </form>
    </div>
</div>
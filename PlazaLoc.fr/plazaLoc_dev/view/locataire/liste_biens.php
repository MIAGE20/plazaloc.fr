<?php
require_once '../../model/dao_locataire.class.php';
$acces_bd = new dao_locataire();
?>

<div class="row box" style="margin: 10px">
    <h3>BIENS DISPO</h3>
    <div class="box-content">
        <table class="table table-striped">
            <tr>
                <td> Id bien </td>
                <td> Id type de bien </td>
                <td> Adresse </td>
                <td> Code postal </td>
                <td> N° quartier </td>
                <td> Id ville </td>
                <td> Superficie </td>
                <td> Nombre de piece </td>
                <td> Etage  </td>
                <td> Descriptionbien </td>
                <td> Charge forfaitaire </td>
            </tr>
            <?php
            $bien = $acces_bd->selectAllBien();

            foreach ($bien as $bien) {
                echo "<tr> 
                    	<td>".$bien['idbien']."</td>
						<td>".$bien['idtypebien']."</td>
						<td>".$bien['adresse']."</td>
						<td>".$bien['codePostale']."</td>
						<td>".$bien['idquartier']."</td>
						<td>".$bien['idville']."</td>
						<td>".$bien['superficie']."</td>
						<td>".$bien['nbpiece']."</td>
						<td>".$bien['etagebien']."</td>
						<td>".$bien['descriptionbien']."</td>
						<td>".$bien['chargeforfaitaire']."</td>
				 </tr>";
            }
            ?>
        </table>
    </div>
</div>
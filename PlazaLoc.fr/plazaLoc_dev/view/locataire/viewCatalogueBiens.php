<?php
require_once '../../model/dao_locataire.class.php';
$acces_bd = new dao_locataire();
$lesBiens = $acces_bd->catalogBien() ;
?>

<div class="container-biens">
        <?php
        foreach ($lesBiens as $unBien) {
            ?>
            <div class="box-bien">
                <a href="viewLocataire.php?page=307&idbien=<?= $unBien['idbien'] ; ?>">
                    <img src="../../images/<?= $unBien['image'] ?>">
                    <div class="desc-bien">
                        <h4 class="title-bien"><?= $unBien['titlebien'] ?></h4>
                        <h4><?= $unBien['superficie'] ?> m²<span class="price"><?= $unBien['montantLoyer'] ?> € / mois</span></h4>
                    </div>
                </a>
            </div>
            <?php
        }
        ?>

</div>

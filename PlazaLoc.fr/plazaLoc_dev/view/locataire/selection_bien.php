<?php
    require_once '../../model/dao_locataire.class.php';

    $acces_bd = new dao_locataire();
// Manage location...
    if (isset($_POST["valider"])) {
        $donnees = array_merge($_POST, array(
            'idlocataire' => $_COOKIE['user_id'], 
            'idbien' => $_GET['idbien'],
            'datedemande' => date('Y-m-d')
        ));

        $acces_bd->insertDemandeLocation($donnees);
        echo '<script>
            window.location.href="../locataire/viewLocataire.php?page=303"; 
        </script>';
        return;
    }

// Manage form demande location

    $motifsLocation = $acces_bd->selectAllMotifLocation();
    $strSelectMotifOptions = "";
    $selected = "selected";
    foreach($motifsLocation as $motif) {
        $strSelectMotifOptions .= "
            <option ".$selected." value='".$motif['idmotiflocation']."'> ".$motif['descriptionmotif']." </option>
        ";
        $selected = "";
    }

    require_once '../locataire/detail_bien.php';

?>
<div class="row">
    <form method="post" action="" class="container-form">
        <div class="form-row">
            <div class="row">
                <div class="col-lg-12">
                    <label class="civilite">Motif de la location:</label>
                    <select name="motiflocation">
                        <?= $strSelectMotifOptions ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Date de début:</label>
                    <input type="date" name="debutLocation" class="form-control">
                </div>
                <div class="form-group col-md-6">
                    <label>Date de fin:</label>
                    <input type="date" name="finLocation" class="form-control" >
                </div>
            </div>

            <div class="row">
                <div class="container-button-form">    
                    <button type="submit" name="valider" class="button">Réserver cette location</button>
                </div>
            </div>
        </div>
    </form>
</div>
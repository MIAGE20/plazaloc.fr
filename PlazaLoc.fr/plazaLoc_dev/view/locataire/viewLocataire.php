<?php
require_once '../../model/dao_locataire.class.php';
$acces_bd = new dao_locataire();
$unBien = $acces_bd->selectOnBien (isset($_GET['page'])?$_GET['page'] : 0);
ini_set('display_errors', 1);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../../layout.css">
    <link href="../../node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <script type="text/javascript" language="JavaScript" src="../../lib/jquery-2.2.4.min.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../js/formLocation.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../js/manage-profile.js"></script>
    <title>Plaza Loc</title>
</head>
<body>

<div class="logo-title-space">
    <a href=""><img src="../../images/logo.png" alt="" title=""></a>
    <h1><a href="viewLocataire.php?page=300">PLAZA LOC</a></h1>
</div>

<div class="banner-space">
    <h4>Bienvenue dans votre Espace Locataire :
        <?php
            echo $_COOKIE['user_nom']." ".$_COOKIE['user_prenom'];
        ?>

    </h4>
</div>

<div class="row container-space" style="margin-left:0; margin-right: 0">
    <nav class="col-lg-2">
        <ul>
            <li><a href="../locataire/viewLocataire.php?page=300"><span class="glyphicon glyphicon-dashboard"></span> Tableau de bord</a></li>
            <li><a href="../locataire/viewLocataire.php?page=301"><span class="glyphicon glyphicon-user"></span> Mon profil </a></li>
            <li><a href="../locataire/viewLocataire.php?page=302"><span class="glyphicon glyphicon-home"></span> Catalogue des biens</a></li>
            <li><a href="../locataire/viewLocataire.php?page=303"><span class="glyphicon glyphicon-folder-open"></span> Demandes envoyées</a></li>
            <li><a href="../locataire/viewLocataire.php?page=304"><span class="glyphicon glyphicon-edit"></span> Effectuer une demande personnalisée</a></li>
			<li><a href="../locataire/viewLocataire.php?page=308"><span class="glyphicon glyphicon-folder-open"></span> Demande personnalisée envoyée</a></li>
			<li><a href="../locataire/viewLocataire.php?page=305"><span class="glyphicon glyphicon-briefcase"></span> Locations enregistrées</a></li>
        </ul>
    </nav>
    <a href="../../index.php" class="button"><span class="glyphicon glyphicon-log-out"></span> Se déconnecter</a>
    <div class="col-lg-10" style="padding-left: 0; padding-right: 0;">
        <div class="row">
            <h3 class="title-space"><span class="glyphicon glyphicon-cog"></span>Gestion de vos demandes de location, choisir un bien</h3>
        </div>
        <div class="row container-boxes">

            <?php
                if (isset($_GET['page'])) {
                    $page = $_GET['page'];
                }
                else {
                    $page =  300;
                }

                switch ($page) {
                    case 300:
                        include '../locataire/tableauDeBordLocataire.php';
                        break;

                    case 301:
                        include '../locataire/monProfil.php';  
                        break;

                    case 302:
                        include '../locataire/viewCatalogueBiens.php';
                        break;

                    case 303:
                        include '../locataire/liste_demandes_location_by_locataire.php';
                        break;

                    case 304:
                        include '../form/viewDemandeLocationPersonalisee.php';
                        break;

                    case 305:
                        include '../locataire/liste_locations_by_locataire.php';
                        break;
					
                    case 306:
                        include '../locataire/detail_bien.php';
                        break;
                    case 307:
                        include '../locataire/selection_bien.php';
                        break;
						
					case 308:	
					    include '../locataire/demandeSansBien.php';
                        break;		

                    default:
                        break;
                }
            ?>
        </div>
    </div>
</div>

</body>
</html>

<?php
require_once '../../model/dao_locataire.class.php';
$acces_bd = new dao_locataire();
?>

<h3 class="title-page">DEMANDE DE LOCATION PERSONNALISEE</h3>

<form method="post" action="" class="form-location">
    <div class="row">
        <h4>Vous recherchez:</h4>
        <div class="radios1">
            <div class="form-check">
                <span class="button-type"><img src="../../images/city.png" alt="" title=""><input class="form-check-input radio radio1" type="radio" name="libelletype" id="appartement" value="appartement"></span>
                <label class="form-check-label" for="libelletype">
                    Un appartement
                </label>
            </div>
            <div class="form-check">
                <span class="button-type"><img src="../../images/house.png" alt="" title=""><input class="form-check-input radio radio1" type="radio" name="libelletype" id="maison" value="maison"></span>
                <label class="form-check-label" for="libelletype">
                    Une maison
                </label>
            </div>
            <div class="form-check">
                <span class="button-type"><img src="../../images/armchair.png" alt="" title=""><input class="form-check-input radio radio1" type="radio" name="libelletype" id="studio" value="studio"></span>
                <label class="form-check-label" for="libelletype">
                    Un studio
                </label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 hidden-select">
            <h4>Nombre de chambres minimum souhaité:</h4>
            <select name="nbchambre">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
            </select>
        </div>
        <div class="col-lg-6">
            <div class="hidden-select-appart">
                <h4>Type d'appartement souhaité:</h4>
                <select name="nbpiece">
                    <option value="1">T1</option>
                    <option value="2">T2</option>
                    <option value="3">T3</option>
                    <option value="4">T4</option>
                </select>
            </div>
            <div class="hidden-select-maison">
                <h4>Type de maison souhaité:</h4>
                <select name="nbpiece">
                    <option value="1">F1</option>
                    <option value="2">F2</option>
                    <option value="3">F3</option>
                    <option value="4">F4</option>
                </select>
            </div>
        </div>
    </div>
    <div class="hidden-section1">
        <div class="row">
            <h4>Ce logement sera:</h4>
            <div class="radios2">
                <div class="form-check">
                    <span class="button-type"><img src="../../images/home.png" alt="" title=""><input class="form-check-input radio radio2" type="radio" name="descriptionmotif" id="principal" value="principal"></span>
                    <label class="form-check-label" for="descriptionmotif">
                        Une habitation
                        principale
                    </label>
                </div>
                <div class="form-check">
                    <span class="button-type"><img src="../../images/students-cap.png" alt="" title=""><input class="form-check-input radio radio2" type="radio" name="descriptionmotif" id="etudiant" value="etudiant"></span>
                    <label class="form-check-label" for="descriptionmotif">
                        Un logement pour étudiant
                    </label>
                </div>
                <div class="form-check">
                    <span class="button-type"><img src="../../images/resort.png" alt="" title=""><input class="form-check-input radio radio2" type="radio" name="descriptionmotif" id="saisonnier" value="saisonnier"></span>
                    <label class="form-check-label" for="descriptionmotif">
                        Une location saisonnière
                    </label>
                </div>
            </div>
        </div>
        <div class="hidden-section2">
            <div class="row">
                <h4>Superficie minimum souhaitée:</h4>
                <div class="row">
                    <div class="col-md-6" style="padding-left: 0">
                        <input type="text" name="superficie" class="form-control" placeholder="m²">
                    </div>
                </div>
            </div>
            <div class="row">
                <h4>Budget & disponibilité:</h4>
                <div class="row">
                    <div class="form-group col-md-6" style="padding-left: 0">
                        <label>Loyer maximum charges comprises:</label>
                        <input type="text" name="prixloyer" class="form-control" placeholder="€ / mois">
                    </div>
                    <div class="form-group col-md-6">
                        <label>Date limite d'emménagement:</label>
                        <input type="text" name="datedebut" class="form-control" placeholder="AAAA-MM-JJ">
                    </div>
                </div>
            </div>
            <div class="row">
                <h4>Secteur:</h4>
                <div class="row">
                    <div class="col-md-6" style="padding-left: 0">
                        <input type="text" name="nomville" placeholder="saisir une ville ou un code postal" class="form-control">
                    </div>
                </div>
            </div>
            <input type="hidden" name="idlocataire" value="<?= $_COOKIE['user_id'] ?>" class="form-control">
            <div class="container-button-form">
                <button type="submit" class="button" name="valider">Envoyer la demande</button>
            </div>
        </div>
    </div>
</form>

<?php
if (isset($_POST['valider'])) {

    $donnees = array_merge($_POST, array('idlocataire' => $_COOKIE['user_id']));

    $demandelocationsansbien = $acces_bd->insertDemandeLocationSansBien($donnees);

    echo '<script>alert ("Votre demande a été envoyée avec succès!"); </script>';
}
?>
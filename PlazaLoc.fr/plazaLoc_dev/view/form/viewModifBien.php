<?php
require_once '../../model/dao_proprietaire.class.php';
$acces_bd = new dao_proprietaire();

if (isset($_POST['valider'])) {

    $hasNewImage = $_FILES['fileToUpload']['name'] !== '';

    if ($hasNewImage) {
        $target_dir = "../../images/";
        $target_file = '';
        $imgType = strtolower(pathinfo($_FILES['fileToUpload']['name'],PATHINFO_EXTENSION));
        $index = 1;
        do {
            $target_file = $target_dir . 'bien_'.($index++).'.'.$imgType;
        } while (file_exists($target_file));

        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        if (!move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
            echo ("Sorry, Erreur de chargement!");
            return;
        }
    }
    $data = array_merge($_POST, array(
        "idproprietaire" => $_COOKIE["user_id"],
        "idbien" => $_GET['idbien'],
        "image" => $hasNewImage? pathinfo($target_file, PATHINFO_BASENAME) : ""
    ));
    $modifBien = $acces_bd->modifyBien($data);
    echo '<script>alert ("Bien modifié avec succès!"); window.location.href="viewProprietaire.php?page=499&idbien='.$_GET['idbien'].'"</script>';
}

$unBien = $acces_bd->selectOnBien($_GET['idbien']);

?>

<h3 class="title-page">MODIFIER UN BIEN</h3>

<div class="row">
    <form method="post" action="" class="modif-profil container-form-wide" enctype="multipart/form-data">
        <div class="form-row">
            <!--<input type="hidden" name="idbien" value="<?= $unBien['idbien'] ?>">
            <input type="hidden" name="idproprietaire" value="<?= $_COOKIE['user_id'] ?>">-->
            <div class="row">
                <div class="col-lg-12">
                    <label class="civilite">Type de bien:</label>
                    <select name="idtypebien" class="civilite">
                        <option value="S" <?=$unBien['idtypebien']=='S' ? 'selected' :'' ?> >Studio</option>
                        <option value="A" <?=$unBien['idtypebien']=='A' ? 'selected' :'' ?> >Appartement</option>
                        <option value="M" <?=$unBien['idtypebien']=='M' ? 'selected' :'' ?> >Maison</option>
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-4">
                    <label>Libellé du bien:</label>
                    <input type="text" name="titlebien" class="form-control" value="<?= $unBien['titlebien'] ?>">
                </div>
                <div class="form-group col-md-4">
                    <label>Adresse:</label>
                    <input type="text" name="adresse" class="form-control" value="<?= $unBien['adresse'] ?>">
                </div>
                <div class="form-group col-md-4">
                    <label>Code postal:</label>
                    <input type="text" name="codePostale" class="form-control" value="<?= $unBien['codePostale'] ?>">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label>Quartier:</label>
                    <input type="text" name="nomquartier" class="form-control" value="<?= $unBien['nomquartier'] ?>">
                </div>
                <div class="form-group col-md-4">
                    <label>Ville:</label>
                    <input type="text" name="nomville" class="form-control" value="<?= $unBien['nomville'] ?>">
                </div>
                <div class="form-group col-md-4">
                    <label>Superficie:</label>
                    <input type="text" name="superficie" class="form-control" value="<?= $unBien['superficie'] ?>">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label>Nombre de pièce:</label>
                    <input type="text" name="nbpiece" class="form-control" value="<?= $unBien['nbpiece'] ?>">
                </div>
                <div class="form-group col-md-4">
                    <label>Nombre de chambre:</label>
                    <input type="text" name="nbchambre" class="form-control" value="<?= $unBien['nbchambre'] ?>">
                </div>
                <div class="form-group col-md-4">
                    <label>Etage:</label>
                    <input type="text" name="etagebien" class="form-control" value="<?= $unBien['etagebien'] ?>">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label>Description:</label>
                    <input type="text" name="descriptionbien" class="form-control" value="<?= $unBien['descriptionbien'] ?>">
                </div>
                <?php
                    if (isset($unBien['chargeforfaitaire'])) {
                        echo '
                            <div class="form-group col-md-4">
                                <label>Charge Forfaitaire:</label>
                                <input type="text" name="chargeforfaitaire" class="form-control" value="'
                                . $unBien['chargeforfaitaire']
                                . '"></div>
                        ';
                    }
                ?>
                <div class="form-group col-md-4">
                    <label>Montant du loyer:</label>
                    <input type="text" name="montantLoyer" class="form-control" value="<?= $unBien['montantLoyer'] ?>">
                </div>
            </div>
            <div class="row">
                <div class="row" style="margin-bottom: 0">
                    <div class="form-group col-md-4" style="margin-bottom: 0">
                        <label>Photo:</label>
                        <p>
                            <img src="../../images/<?= $unBien['image'] ?>" alt="" title="Photo" class="img-form">
                        </p>
                    </div>
                    <div class="form-group col-md-4" style="margin-bottom: 0">
                        <label>Importer une nouvelle photo:</label>
                        <input type="file" name="fileToUpload">
                    </div>
                </div>
            </div>
            <div class="container-button-form" style="margin-top: 0">
                <button type="submit" class="button" name="valider">Valider</button>
            </div>
        </div>
    </form>
</div>
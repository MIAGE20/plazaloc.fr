<?php
require_once '../../model/dao_proprietaire.class.php';
$acces_bd = new dao_proprietaire();
$unProprietaire = $acces_bd->selectOnProprietaire($_COOKIE['user_id']);
$uneLocation = $acces_bd->selectOnDemandeLocation ($_GET['idlocataire']);
?>

<h3 class="title-page">AJOUTER UNE LOCATION</h3>

<div class="row">
    <form method="post" action="" class="modif-profil container-form">
        <div class="form-row">
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Prix du loyer:</label>
                    <input type="text" name="prixLoyer" class="form-control">
                </div>
                <div class="form-group col-md-6">
                    <label>Date de début de la location:</label>
                    <input type="text" name="datedebut" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Date de fin de la location:</label>
                    <input type="text" name="datefin" class="form-control">
                </div>
                <div class="form-group col-md-6">
                    <label class="civilite">Motif de la location:</label>
                    <select name="idmotiflocation" class="civilite">
                        <option value="50">Location d'étudiant</option>
                        <option value="51">Location saisonnière</option>
                        <option value="52">Indifférent</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Référence du bien:</label>
                    <input type="text" name="idbien" class="form-control">
                </div>
            </div>
            <div class="container-button-form">
                <button type="submit" class="button" name="valider">Valider</button>
            </div>
        </div>
    </form>

    <?php
    if (isset($_POST['valider'])) {
        $profilProprietaire = $acces_bd->modifyProprietaire($_POST);

        echo '<script>alert ("Votre profil a été modifié avec succès!"); </script>';
    }
    ?>
</div>
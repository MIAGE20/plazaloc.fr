<?php
require_once '../../model/dao_proprietaire.class.php';
$acces_bd = new dao_proprietaire();
//$unProprietaire = $acces_bd->InsertBien($_COOKIE['user_id']);


if (isset($_POST['valider'])) {
    $target_dir = "../../images/";
    $target_file = '';
    $imgType = strtolower(pathinfo($_FILES['fileToUpload']['name'],PATHINFO_EXTENSION));
    $index = 1;
    do {
        $target_file = $target_dir . 'bien_'.($index++).'.'.$imgType;
    } while (file_exists($target_file));

    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    if (!move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo '<script>alert("Veuillez remplir tous les champs du formulaire SVP"); window.location.href="viewProprietaire.php?page=406"</script>';
        return;
    }

    $data = array_merge($_POST, array(
        "idproprietaire" => $_COOKIE["user_id"],
        "image" => pathinfo($target_file, PATHINFO_BASENAME)
    ));
    $modifBien = $acces_bd->insertBien($data);
    echo '<script>alert ("Bien ajouté avec succès!"); window.location.href="viewProprietaire.php?page=402" </script>';
}

?>

<h3 class="title-page">AJOUTER UN BIEN</h3>

<div class="row">
    <form method="post" action="" class="modif-profil container-form-wide" enctype="multipart/form-data">
        <div class="form-row">
            <div class="row">
                <input type="hidden" name="idproprietaire" value="<?= $_COOKIE['user_id'] ?>">
                <div class="col-lg-12">
                    <label class="civilite">Type de bien:</label>
                    <select name="idtypebien" class="civilite">
                        <option value="A">Studio</option>
                        <option value="B">Appartement</option>
                        <option value="C">Maison</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label>Libellé du bien:</label>
                    <input type="text" name="titlebien" class="form-control">
                </div>
                <div class="form-group col-md-4">
                    <label>Adresse:</label>
                    <input type="text" name="adresse" class="form-control">
                </div>
                <div class="form-group col-md-4">
                    <label>Code postal:</label>
                    <input type="text" name="codePostale" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label>Quartier:</label>
                    <input type="text" name="nomquartier" class="form-control">
                </div>
                <div class="form-group col-md-4">
                    <label>Ville:</label>
                    <input type="text" name="nomville" class="form-control">
                </div>
                <div class="form-group col-md-4">
                    <label>Superficie:</label>
                    <input type="text" name="superficie" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label>Nombre de pièce:</label>
                    <input type="text" name="nbpiece" class="form-control">
                </div>
                <div class="form-group col-md-4">
                    <label>Nombre de chambre:</label>
                    <input type="text" name="nbchambre" class="form-control">
                </div>
                <div class="form-group col-md-4">
                    <label>Etage:</label>
                    <input type="text" name="etagebien" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label>Description:</label>
                    <input type="text" name="descriptionbien" class="form-control">
                </div>
                <div class="form-group col-md-4">
                    <label>Charge Forfaitaire:</label>
                    <input type="text" name="chargeforfaitaire" class="form-control">
                </div>
                <div class="form-group col-md-4">
                    <label>Montant du loyer:</label>
                    <input type="text" name="montantLoyer" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="row" style="margin-bottom: 0">
                    <div class="form-group col-md-4" style="margin-bottom: 0">
                        <label>Importer une photo:</label>
                        <input type="file" name="fileToUpload">
                    </div>
                </div>
            </div>
            <div class="container-button-form" style="margin-top: 0">
                <button type="submit" class="button" name="valider">Valider</button>
            </div>
        </div>
    </form>
</div>
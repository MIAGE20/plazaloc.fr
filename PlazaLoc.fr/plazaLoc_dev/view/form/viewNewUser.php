<div class="container-location container-new-user">
    <div class="column-location column-left">
        <h3><a href="index.php?page=6">Je cherche une location</a></h3>
    </div>
    <div class="column-location column-right">
        <h3><a href="index.php?page=7">Je propose une location</a></h3>
    </div>
</div>

<div class="temoignages">
    <h1>94% d'avis favorables!</h1>
    <div class="row">
        <div class="col-lg-3">
            <div class="stars">
                <img src="images/star.png" alt="" title="">
                <img src="images/star.png" alt="" title="">
                <img src="images/star.png" alt="" title="">
                <img src="images/star.png" alt="" title="">
            </div>
            <h3>Catherine, locataire</h3>
            <p>Réseau efficace et sûr.<br />
                Bons conseils et demandes ciblées.<br />
                Je recommande!
            </p>
        </div>
        <div class="col-lg-3">
            <div class="stars">
                <img src="images/star.png" alt="" title="">
                <img src="images/star.png" alt="" title="">
                <img src="images/star.png" alt="" title="">
                <img src="images/star-empty.png" alt="" title="">
            </div>
            <h3>Ahlem, propriétaire</h3>
            <p>Site intuitif qui m'a permis de prendre rapidement contact avec de potentiels locataires.<br />
                Agents immobiliers disponibles et à l'écoute.
            </p>
        </div>
        <div class="col-lg-3">
            <div class="stars">
                <img src="images/star.png" alt="" title="">
                <img src="images/star.png" alt="" title="">
                <img src="images/star.png" alt="" title="">
                <img src="images/star-empty.png" alt="" title="">
            </div>
            <h3>Jean, locataire</h3>
            <p>De nombreuses offres sont proposées,<br />
                j'ai pu rapidement trouvé mon bonheur!<br />
            Merci Plaza Loc!</p>
        </div>
        <div class="col-lg-3">
            <div class="stars">
                <img src="images/star.png" alt="" title="">
                <img src="images/star.png" alt="" title="">
                <img src="images/star.png" alt="" title="">
                <img src="images/star.png" alt="" title="">
            </div>
            <h3>Paul, propriétaire</h3>
            <p>Agence réactive qui s'occupe de tout dans la gestion locative, ce qui permet de gagner un temps fou.<br />
                Un petit bijou!</p>
        </div>
    </div>
    <h1>Nos partenaires</h1>
    <div class="logos">
        <img src="images/ZewUjdo8_400x400.jpg" alt="" title="">
        <img src="images/Logo_Seloger_2017.png" alt="" title="">
        <img src="images/logo_ERA.jpg" alt="" title="">
    </div>
</div>
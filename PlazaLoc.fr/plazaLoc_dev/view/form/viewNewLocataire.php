<?php
require_once 'model/dao_admin.class.php';
$acces_bd = new dao_admin();
?>

<div class="row banner-title banner-title-new-owner" style="margin-left: 0; margin-right: 0">
    <h1>Création de mon Espace Locataire</h1>
</div>

<section class="new-owner">
    <div class="title-new-ower">
        <h1>Créer gratuitement mon Espace Locataire</h1>
        <h4>Consultez gratuitement tous nos biens, et trouvez rapidement un bien à louer!</h4>
    </div>

    <div class="container-form">
        <form method="post" action="" class="col-lg-12" name="locataireForm" onsubmit="return validateLocataireForm()">

            <label class="civilite"> Civilité:</label>
            <select name="civiliteLocataire">
                <option value="Mr">Mr</option>
                <option value="Mme">Mme</option>
                <option value="Mlle">Mlle</option>
            </select>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Nom *:</label>
                    <input type="text" name="nom" class="form-control">
                </div>
                <div class="form-group col-md-6">
                    <label>Prénom *:</label>
                    <input type="text" name="prenom" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Date de naissance *:</label>
                    <input type="text" name="dateNaissanceLocataire" class="form-control" placeholder="AAAA/MM/JJ">
                </div>
                <div class="form-group col-md-6">
                    <label>Adresse *:</label>
                    <input type="text" name="adresseLocataire" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Email *:</label>
                    <input type="text" name="email" class="form-control">
                </div>
                <div class="form-group col-md-6">
                    <label>Téléphone :</label>
                    <input type="text" name="tel" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-3">
                    <label>N° CCP :</label>
                    <input type="text" name="ccp" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Mot de passe *:</label>
                    <input type="password" name="mdp" class="form-control">
                </div>
                <div class="form-group col-md-6">
                    <label>Confirmez le mot de passe *:</label>
                    <input type="password" name="mdpConfirmation" class="form-control">
                </div>
            </div>
            <label>En vous inscrivant, vous acceptez nos <a href="view/viewCGU.php?page=9" target="_blank"> <b>CGU</b></a> et nos clauses de <a href="view/viewConfidentialite.php?page=10" target="_blank"><b>confidentialité </b></a></label>
      
            <div class="container-button-form">
                <button type="submit" class="button" name = 'valider'>Créer mon profil </button>
            </div>
             
        </form>

            <script type="text/javascript">
                function validateLocataireForm() {
                    var nom = document.forms["locataireForm"]["nom"].value;
                    var prenom = document.forms["locataireForm"]["prenom"].value;
                    var date = document.forms["locataireForm"]["dateNaissanceLocataire"].value;
                    var adresse = document.forms["locataireForm"]["adresseLocataire"].value;
                    var email = document.forms["locataireForm"]["email"].value;
                    var ccp = document.forms["locataireForm"]["ccp"].value;
                    var mdp = document.forms["locataireForm"]["mdp"].value;
                    var confirm = document.forms["locataireForm"]["mdpConfirmation"].value;
                    if (nom == "" || prenom == "" || date == "" || adresse == "" || email == "" || ccp == "" || mdp == "" || confirm == "") {
                        alert ("Veuillez saisir tous les champs obligatoires");
                        return false;
                    } else {
                        return true;
                    }
                }
            </script>


        <?php
        if (isset($_POST['valider'])) {
            if ($_POST['mdp'] == $_POST['mdpConfirmation'])
            {
                $proprietaire = $acces_bd->insertLocataire(
                    $_POST['civiliteLocataire'],
                    $_POST['nom'],
                    $_POST['prenom'],
                    $_POST['dateNaissanceLocataire'],
                    $_POST['adresseLocataire'],
                    $_POST['tel'],
                    $_POST['email'],
                    $_POST['mdp'],
                    $_POST['ccp']
                );

                echo '<script>alert ("Votre profil a été crée"); </script>';
            } else { echo '<script>alert ("erreur mot de passe différent "); </script>';}
        }
        ?>
    </div>
</section>
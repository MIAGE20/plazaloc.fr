<?php
require_once 'model/dao_admin.class.php';
$acces_bd = new dao_admin();
?>

<div class="row banner-title banner-title-new-owner" style="margin-left: 0; margin-right: 0">
    <h1>Création de mon Espace Propriétaire</h1>
</div>

<section class="new-owner">
    <div class="title-new-ower">
        <h1>Créer gratuitement mon Espace Propriétaire</h1>
        <h4>Consultez gratuitement les demandes de locataires, et louez rapidement votre bien!</h4>
    </div>
    <div class="container-form">
    <form method="post" action="" class="col-lg-12" name="proprietaireForm" onsubmit="return validateProprietaireForm()">
            <label class="civilite"> Civilité:</label>
            <select name="civiliteproprietaire">
                <option value="Mr">Mr</option>
                <option value="Mme">Mme</option>
                <option value="Mlle">Mlle</option>
            </select>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Nom *:</label>
                    <input type="text" name="nom" class="form-control">
                </div>
                <div class="form-group col-md-6">
                    <label>Prénom *:</label>
                    <input type="text" name="prenom" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Date de naissance *:</label>
                    <input type="text" name="dateNaissanceProprietaire" class="form-control" placeholder="AAAA/MM/JJ">
                </div>
                <div class="form-group col-md-6">
                    <label>Adresse *:</label>
                    <input type="text" name="adresseProprietaire" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Email *:</label>
                    <input type="text" name="email" class="form-control">
                </div>
                <div class="form-group col-md-6">
                    <label>Téléphone *:</label>
                    <input type="text" name="tel" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>N° Assurance *:</label>
                    <input type="text" name="assurance" class="form-control">
                </div>
                <div class="form-group col-md-6">
                    <label>N° CCP *:</label>
                    <input type="text" name="ccp" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Mot de passe *:</label>
                    <input type="password" name="mdp" class="form-control">
                </div>
                <div class="form-group col-md-6">
                    <label>Confirmez le mot de passe *:</label>
                    <input type="password" name="mdpConfirmation" class="form-control">
                </div>
            </div>
            <label>En vous inscrivant, vous acceptez nos <a href="view/viewCGU.php?page=9" target="_blank"> <b>CGU</b></a> et nos clauses de <a href="view/viewConfidentialite.php?page=10" target="_blank"><b>confidentialité </b></a></label>
      
            <div class="container-button-form">
                <button type="submit" class="button" name = 'valider'>Créer mon profil </button>
            </div>
             
        </form>
            <script type="text/javascript">
                function validateProprietaireForm() {
                    var nom = document.forms["proprietaireForm"]["nom"].value;
                    var prenom = document.forms["proprietaireForm"]["prenom"].value;
                    var date = document.forms["proprietaireForm"]["dateNaissanceProprietaire"].value;
                    var adresse = document.forms["proprietaireForm"]["adresseProprietaire"].value;
                    var email = document.forms["proprietaireForm"]["email"].value;
                    var assurance = document.forms["proprietaireForm"]["assurance"].value;
                    var ccp = document.forms["proprietaireForm"]["ccp"].value;
                    var mdp = document.forms["proprietaireForm"]["mdp"].value;
                    var confirm = document.forms["proprietaireForm"]["mdpConfirmation"].value;
                    if (nom == "" || prenom == "" || date == "" || adresse == "" || email == "" || assurance == "" || ccp == "" || mdp == "" || confirm == "") {
                        alert ("Veuillez saisir tous les champs obligatoires");
                        return false;
                    } else {
                        return true;
                    }
                }
            </script>

        <?php
        if (isset($_POST['valider'])) {
            if ($_POST['mdp'] == $_POST['mdpConfirmation'])
            {
                $proprietaire = $acces_bd->insertProprietaire(
                    $_POST['civiliteproprietaire'],
                    $_POST['nom'],
                    $_POST['prenom'],
                    $_POST['dateNaissanceProprietaire'],
                    $_POST['adresseProprietaire'],
                    $_POST['tel'],
                    $_POST['email'],
                    $_POST['mdp'],
                    $_POST['assurance'],
                    $_POST['ccp']
                );

                echo '<script>alert ("Votre profil a été crée"); </script>';
            } else { echo '<script>alert ("erreur mot de passe différent "); </script>';}
        }
        ?>
    </div>
</section>
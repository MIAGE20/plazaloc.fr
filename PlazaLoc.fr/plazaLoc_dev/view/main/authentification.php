<?php

require_once ("../../model/dao_admin.class.php");
require_once ("../../controller/user.class.php");

$mail = $_POST['email'];
$mdp = $_POST['mdp'];

$acces_bd = new dao_admin();
$user = new UserController($mail, $mdp, $acces_bd);

if ($user -> validerConnexion()) {
    $profile = $user->getProfile();
    if ($profile == "admin") {
        require_once '../admin/viewAdmin.php';
    } else if ($profile == "locataire") {
        require_once '../locataire/viewLocataire.php';
    } else if ($profile == "proprietaire"){
        require_once '../proprietaire/viewProprietaire.php';
    }else {
        echo 'erreur. Main view non definie pour le profile: '.$profile;
        die(__FILE__);
    }
} else {
    /*echo 'SORRY '.$profile.' LOGIN OR PWD INVALID :(';*/
    echo '<script>
            alert("Erreur de connexion! Identifiant ou Mot De Passe incorrect :("); 
            window.location.href = "../../index.php"; 
        </script>';
}

?>
<?php
require_once ("../../model/dao_proprietaire.class.php");
$acces_bd = new dao_proprietaire();
?>

<div class="row">
    <h3 class="title-page">TABLEAU DE BORD</h3>

        <div class=" col-lg-3 box">
        <div class="container-icon">
            <span class="glyphicon glyphicon-home"></span>
        </div>
        <div class="box-content">
        <span class="number">
            <?php
            $idProprietaire = $_COOKIE["user_id"]; 
            $nbBiensByProprietaire = $acces_bd->countBiensByProprietaire($idProprietaire);
            echo $nbBiensByProprietaire;
            ?>
        </span>
            <div class="desc-box">
                <h4>BIENS ENREGISTRES</h4>
                <a href="../proprietaire/viewProprietaire.php?page=402">Visualisez tous vos biens</a>
            </div>
        </div>
    </div>

    <div class=" col-lg-3 box">
        <div class="container-icon">
            <span class="glyphicon glyphicon-folder-open"></span>
        </div>
        <div class="box-content">
        <span class="number">
            <?php
            $idProprietaire = $_COOKIE["user_id"];
            $nbDemandesLocationByProprietaire = $acces_bd->countDemandesLocationByProprietaire($idProprietaire);
            echo $nbDemandesLocationByProprietaire;
            ?>
        </span>
            <div class="desc-box">
                <h4>DEMANDES DE LOCATION</h4>
                <a href="../proprietaire/viewProprietaire.php?page=404">Visualisez toutes vos demandes de location</a>
            </div>
        </div>
    </div>

    <div class=" col-lg-3 box">
        <div class="container-icon">
            <span class="glyphicon glyphicon-file"></span>
        </div>
        <div class="box-content">
        <span class="number">
            <?php
            $idProprietaire = $_COOKIE["user_id"];
            $nbLocationsByProprietaire = $acces_bd->countLocationsByProprietaire($idProprietaire);
            echo $nbLocationsByProprietaire;
            ?>
        </span>
            <div class="desc-box">
                <h4>LOCATIONS ENREGISTRES</h4>
                <a href="../proprietaire/viewProprietaire.php?page=403">Visualisez tous vos locations</a>
            </div>
        </div>
    </div>



    <div class=" col-lg-3 box">
        <div class="container-icon">
            <span class="glyphicon glyphicon-edit"></span>
        </div>
        <div class="box-content">
            <div class="desc-box desc-box-new-bien">
                <h4>AJOUTER UN BIEN</h4>
                <a href="../proprietaire/viewProprietaire.php?page=406">Ajoutez un bien à louer</a>
            </div>
        </div>
    </div>

    <div class=" col-lg-3 box">
        <div class="container-icon">
            <span class="glyphicon glyphicon-edit"></span>
        </div>
        <div class="box-content">
            <div class="desc-box desc-box-new-location">
                <h4>AJOUTER UNE LOCATION</h4>
                <a href="../proprietaire/viewProprietaire.php?page=404">Ajoutez une location</a>
            </div>
        </div>
    </div>

</div>

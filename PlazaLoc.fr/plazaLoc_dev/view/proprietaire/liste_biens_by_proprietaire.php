<?php
require_once ("../../model/dao_proprietaire.class.php");

$acces_bd = new dao_proprietaire();

$idProprietaire = $_COOKIE["user_id"];
$biens = $acces_bd->selectAllBienByProprietaire($idProprietaire);
?>

<h3 class="title-page">MES BIENS</h3>

<div class="container-biens">
    <?php
    foreach ($biens as $bien) {
        ?>
        <div class="box-bien">
            <a href="viewProprietaire.php?page=499&idbien=<?= $bien['idbien'] ; ?>">
                <img src="../../images/<?= $bien['image'] ?>">
                <div class="desc-bien">
                    <h4 class="title-bien"><?= $bien['titlebien'] ?></h4>
                    <h4><?= $bien['superficie'] ?> m²<span class="price"><?= $bien['montantLoyer'] ?> € / mois</span></h4>
                </div>
            </a>
        </div>
        <?php
    }
    ?>
</div>
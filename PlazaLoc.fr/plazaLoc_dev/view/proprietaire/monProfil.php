<?php
require_once '../../model/dao_proprietaire.class.php';
require_once '../../services/user_services.class.php';
$acces_bd = new dao_proprietaire();

if (isset($_POST['valider'])) {
    $acces_bd->modifyProprietaire(array_merge($_POST,  array('idproprietaire' => $_COOKIE['user_id'])));

    UserService::updateUser($_POST['nom'], $_POST['prenom']);

    echo '<script>alert ("Votre profil a été modifié avec succès!"); 
        refresViewOnProfileUpdate();
    </script>';
}

$unProprietaire = $acces_bd->selectOnProprietaire($_COOKIE['user_id']);

?>

<h3 class="title-page">MODIFIER MON PROFIL</h3>

<div class="row">
    <form method="post" action="" class="modif-profil container-form">
        <div class="form-row">
            <div class="row">
                <div class="col-lg-12">
                    <label class="civilite">Civilité:</label>
                    <select name="civiliteproprietaire" class="civilite">
                        <option selected value="<?= $unProprietaire['civilite'] ?>"><?= $unProprietaire['civilite'] ?></option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Nom:</label>
                    <input type="text" name="nom" class="form-control" value="<?= $unProprietaire['nom'] ?>">
                </div>
                <div class="form-group col-md-6">
                    <label>Prénom:</label>
                    <input type="text" name="prenom" class="form-control" value="<?= $unProprietaire['prenom'] ?>">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Date de naissance:</label>
                    <input type="text" name="datenaissance" class="form-control" value="<?= $unProprietaire['datenaissance'] ?>">
                </div>
                <div class="form-group col-md-6">
                    <label>Adresse:</label>
                    <input type="text" name="adresse" class="form-control" value="<?= $unProprietaire['adresse'] ?>">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Téléphone:</label>
                    <input type="text" name="numtel" class="form-control" value="<?= $unProprietaire['numtel'] ?>">
                </div>
                <div class="form-group col-md-6">
                    <label>Email:</label>
                    <input type="text" name="mail" class="form-control" value="<?= $unProprietaire['mail'] ?>">
                </div>
            </div>
			<div class="row">
                <div class="form-group col-md-6">
                    <label>Assurance:</label>
                    <input type="text" name="assurance" class="form-control" value="<?= $unProprietaire['assurance'] ?>">
                </div>
                <div class="form-group col-md-6">
                    <label>CCP:</label>
                    <input type="text" name="ccp" class="form-control" value="<?= $unProprietaire['ccp'] ?>">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Mot de passe:</label>
                    <input type="password" name="mdp" class="form-control" value="<?= $unProprietaire['mdp'] ?>">
                </div>
            </div>
            <div class="container-button-form">
                <button type="submit" class="button" name="valider">Modifier</button>
            </div>
        </div>
    </form>

    <?php
    
    ?>
</div>
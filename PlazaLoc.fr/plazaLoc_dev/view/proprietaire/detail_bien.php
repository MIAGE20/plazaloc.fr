<form method="post" action="">
    <div class="container-bien">
        <div class="col-lg-6">
            <img src="../../images/<?= $unBien['image'] ?>">
        </div>
        <div class="col-lg-6 page-bien-desc">
            <div class="row">
                <div class="col-lg-10">
                    <h3><span class="glyphicon glyphicon-home"></span>
                        <?= $unBien['titlebien'] ?>
                    </h3>
                </div>
                <div class="col-lg-2">
                    <h3>
                        <div class="col-lg-6">
                            <a href="viewProprietaire.php?page=408&idbien=<?= $unBien['idbien'] ?>">
                                <span class="glyphicon glyphicon-edit"></span>
                            </a>
                        </div>
                        <div class="col-lg-6">
                            <button type="submit" name="delete" class="button-noborder">
                                <span class="glyphicon glyphicon-remove"></span>
                            </button>
                        </div>
                    </h3>
                </div>
            </div>
            <p>
                <?= $unBien['descriptionbien'] ?>
            </p>

            <h4 class="page-bien-title">Détails</h4>
            <table class="table table-striped">
                <tbody>
                    <tr>
                        <td>Réf. du bien</td>
                        <td><input type="text" name="idbien" value="<?= $unBien['idbien'] ?>" class="table-input"></td>
                    </tr>
                    <tr>
                        <td>Nombre de pièce(s)</td>
                        <td>
                            <?= $unBien['nbpiece'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Surface</td>
                        <td>
                            <?= $unBien['superficie'] ?> m²</td>
                    </tr>
                    <tr>
                        <td>Etage</td>
                        <td>
                            <?= $unBien['etagebien'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Montant du loyer</td>
                        <td>
                            <?= $unBien['montantLoyer'] ?>€
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <input type="hidden" name="idlocataire" value="<?= $_COOKIE['user_id'] ?>">
        <input type="hidden" name="idproprietaire" value="<?= $unBien['idproprietaire'] ?>">
    </div>
</form>

<?php

if (isset($_POST['delete'])) {
    $deleteBien = $acces_bd->deleteBien($unBien['idbien']);
    echo '<script>alert ("Bien supprimé avec succès!"); window.location.href = "viewProprietaire.php?page=402" </script>';
}

?>
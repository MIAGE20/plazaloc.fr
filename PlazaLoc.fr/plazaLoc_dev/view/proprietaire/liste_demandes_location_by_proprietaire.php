<?php
require_once ("../../model/dao_proprietaire.class.php");
$acces_bd = new dao_proprietaire();
if (isset($_POST['valider'])) {
    $acces_bd->validerDemandeLocation($_POST['iddemandelocation']);
	echo '<script>alert ("Location ajoutée avec succès!"); window.location.href="viewProprietaire.php?page=403" </script>';
}
?>

<div class="row box" style="margin: 10px">
    <h3> MES DEMANDES DE LOCATION</h3>
    <div class="box-content">


        <script type="text/javascript">
            function setSelectedRow(iddemandelocation) {
                var selectedHiddenInput = document.forms["validationDemandeLocationForm"]["iddemandelocation"+iddemandelocation];
                selectedHiddenInput.value = iddemandelocation;
                selectedHiddenInput.name = 'iddemandelocation';
            }
        </script>

        <form method="post" action="" class="col-lg-12" name="validationDemandeLocationForm">
            <table class="table table-striped">
                <tr>
                    <td> Demande n° </td>
                    <td> Date de la demande </td>
                    <td> Date début location </td>
                    <td> Date fin location </td>
                    <td> Référence Bien </td>
                    <td> Référence Locataire </td>
                    <td> Motif location </td>
                    <td> Valider </td>
                </tr>
                <?php
                    $idProprietaire = $_COOKIE["user_id"]; // a corriger plus tard
                    $demandes = $acces_bd->selectAllDemandeLocationByProprietaire($idProprietaire);

                    foreach ($demandes as $demande) {
                        $iddemandelocation = $demande['iddemandelocation'];
                        echo "
                            <input type='hidden' name='iddemandelocation".$iddemandelocation."' />
                            <tr> 	
                                <td>".$iddemandelocation."</td>
        						<td>".$demande['datedemande']."</td>
                                <td>".$demande['datedebut']."</td>
                                <td>".$demande['datefin']."</td>
        						<td>".$demande['idbien']."</td>
        						<td>".$demande['idlocataire']."</td>
                                <td>".$demande['descriptionmotif']."</td>
                                <td><button type='submit' name='valider' onclick='setSelectedRow(".$iddemandelocation.");' class='button-noborder'><span class='glyphicon glyphicon-ok' style='color:green';></span></button></td>
        				    </tr>";
						                    }
                ?>
            </table>
        </form>
    </div>
</div>

<?php
require_once '../../model/dao_proprietaire.class.php';

$acces_bd = new dao_proprietaire();

$unBien = $acces_bd->selectOnBien (isset($_GET['idbien'])?$_GET['idbien'] : 0);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../../layout.css">
    <link href="../../node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    
    <script type="text/javascript" language="JavaScript" src="../../js/manage-profile.js"></script>

    <title>Plaza Loc</title>
</head>
<body>

<div class="logo-title-space">
    <a href=""><img src="../../images/logo.png" alt="" title=""></a>
    <h1><a href="viewProprietaire.php?page=400">PLAZA LOC</a></h1>
</div>

<div class="banner-space">
    <h4>Bienvenue dans votre Espace Propriétaire :

        <?php
            echo $_COOKIE['user_nom']." ".$_COOKIE['user_prenom'];
        ?>

    </h4> 

</div>

<div class="row container-space" style="margin-left:0; margin-right: 0">
    <nav class="col-lg-2">
        <ul>
            <li><a href="../proprietaire/viewProprietaire.php?page=400"><span class="glyphicon glyphicon-dashboard"></span> Tableau de bord</a></li>
            <li><a href="../proprietaire/viewProprietaire.php?page=401"><span class="glyphicon glyphicon-user"></span> Mon profil </a></li>
            <li><a href="../proprietaire/viewProprietaire.php?page=402"><span class="glyphicon glyphicon-home"></span> Mes biens</a></li>
            <li><a href="../proprietaire/viewProprietaire.php?page=403"><span class="glyphicon glyphicon-file"></span> Mes locations</a></li>
			<li><a href="../proprietaire/viewProprietaire.php?page=404"><span class="glyphicon glyphicon-edit"></span> Mes demandes </a></li>
            <li><a href="../proprietaire/viewProprietaire.php?page=406"><span class="glyphicon glyphicon-edit"></span> Ajouter un bien</a></li>
            <li><a href="../proprietaire/viewProprietaire.php?page=405"><span class="glyphicon glyphicon-briefcase"></span> Localiser mon bien</a></li> 
            
        </ul>
    </nav>
    <a href="../../index.php" class="button"><span class="glyphicon glyphicon-log-out"></span> Se déconnecter</a>
    <div class="col-lg-10" style="padding-left: 0; padding-right: 0;">
        <div class="row">
            <h3 class="title-space"><span class="glyphicon glyphicon-cog"></span>Gestion de vos demandes de location et de vos biens</h3>
        </div>
        <div class="row container-boxes">

            <?php
            if (isset($_GET['page'])) {
                $page = $_GET['page'];
            }
            else {
                $page =  400;
            }

            switch ($page) {
                case 400:
                    include 'tableauDeBordProprietaire.php';
                    break;

                 case 401:
                    include '../proprietaire/monProfil.php';
                    break;

                case 402:
                    include '../proprietaire/liste_biens_by_proprietaire.php';
                    break;

                case 403:
                    include '../proprietaire/liste_locations_by_proprietaire.php';
                    break;

                case 404:
                    include '../proprietaire/liste_demandes_location_by_proprietaire.php';
                    break;

                case 405:
                    include '../proprietaire/localiserMonBien.php';
                    break;

                case 406:
                    include '../form/viewNewBien.php';
                    break;
                case 407:
                    include '../form/viewNewLocation.php';
                    break;

                case 408:
                    include '../form/viewModifBien.php';
                    break;
                case 499:
                default:
                    include 'detail_bien.php';
                    
            
                    break;
            }
            ?>
        </div>
    </div>
</div>

</body>
</html>

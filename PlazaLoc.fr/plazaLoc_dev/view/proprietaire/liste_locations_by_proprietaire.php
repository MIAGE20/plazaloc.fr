<?php
    require_once ("../../model/dao_proprietaire.class.php");

    $acces_bd = new dao_proprietaire();

    if (isset($_POST['resilier'])) {
        $acces_bd->deleteLocation($_POST['idlocation']);

    }

?>

<div class="row box" style="margin: 10px">
    <h3>MES LOCATIONS</h3>
    <div class="">
        <script type="text/javascript">
            function confirmResiliation (){
                if (confirm("Souhaitez-vous vraiment résilier cette location?")) {
                    return true;
                } else {
                    return false;
                }
            }
            function setSelectedRow(idlocation) {
                var selectedHiddenInput = document.forms["resilierLocationForm"]["idlocation"+idlocation];
                selectedHiddenInput.value = idlocation;
                selectedHiddenInput.name = 'idlocation';
            }
        </script>    

        <form method="post" action="" name="resilierLocationForm" onsubmit="return confirmResiliation();">
            <table class="table table-striped">
                <tr>
                    <td> Référence Location </td>
                    <td> Montant </td>
                    <td> Début </td>
                    <td> Fin </td>
                    <td> Référence du bien </td>
                    <td> Référence Locataire </td>
                    <td> Motif de la location </td>
                    <td> Résilier </td>
                </tr>
                
                <?php
              
                    $idProprietaire = $_COOKIE["user_id"]; 
                    $locations = $acces_bd->selectAllLocationByProprietaire($idProprietaire);

                    foreach ($locations as $location) {
                        $idlocation = $location['idlocation'];
                        echo "
                            <input type='hidden' name='idlocation".$idlocation."'/>
                            <tr> 
                                <td>".$idlocation."</td>
        						<td>".$location['prixloyer']."</td>
        						<td>".$location['datedebut']."</td>
        						<td>".$location['datefin']."</td>
        						<td>".$location['idbien']."</td>
        						<td>".$location['idlocataire']."</td>
        						<td>".$location['descriptionmotif']."</td>
                                <td><button type='submit' name='resilier' onclick='setSelectedRow(".$idlocation.");' class='button-noborder'><span class='glyphicon glyphicon-remove'></span></button></td>
        				    </tr>";
                    }
                ?>
            </table>
        </form>
    </div>
</div>

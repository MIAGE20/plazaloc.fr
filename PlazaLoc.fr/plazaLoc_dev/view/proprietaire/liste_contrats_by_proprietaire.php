<?php
require_once ("../../model/dao_proprietaire.class.php");

$acces_bd = new dao_proprietaire();

/*
if (isset($_POST[$contrats['idcontrat']])) {
                $deleteContrat = $acces_bd->deleteContrat($contrats['idcontrat']);
                $deleteLocation = $acces_bd->deleteLocation($contrats['idlocation']);
                echo '<script>alert ("Contrat résilié avec succès!"); </script>';
            }
*/
?>
<div class="row box" style="margin: 10px">
    <h3>LISTE DES CONTRATS</h3>
    <div class="box-content">
        <table class="table table-striped">
            <tr>
                <td> Id contrat </td>
                <td> Date debut contrat </td>
                <td> Date fin contrat </td>
                <td> Prix loyer </td>
                <td> Id locataire </td>
                <td> Id proprietaire </td>
                <td> Id Location </td>
                <td> Résilier </td>
            </tr>
            <?php

            $idProprietaire = $_COOKIE["user_id"]; 

            $contrats = $acces_bd->selectAllContratByProprietaire($idProprietaire);


            foreach ($contrats as $contrat) {
                echo "<tr> 
                    	<td>".$contrat['idcontrat']."</td>
						<td>".$contrat['datedebut']."</td>
						<td>".$contrat['datefin']."</td>
						<td>".$contrat['prixloyer']."</td>
						<td>".$contrat['idlocataire']."</td>
						<td>".$contrat['idproprietaire']."</td>
						<td>".$contrat['idlocation']."</td>
						<td><button type='submit' name='".$contrat['idcontrat']."' class='button-noborder'><span class='glyphicon glyphicon-remove'></span></button></td>
				 </tr>";
            }

            
            ?>
        </table>
    </div>
</div>
<div>
<h3 class="title-page" style="margin-top: 60px">MODIFIER UN CONTRAT</h3>

<div class="row">
    <form method="post" action="" class="modif-profil container-form">
        <div class="form-row">

            <div class="row">
                <input type="hidden" name="idproprietaire" value="<?= $_COOKIE['user_id'] ?>">
                <div class="form-group col-md-6">
                    <label>Référence Bien:</label>
                    <input type="text" name="idbien" class="form-control">
                </div>

                <div class="form-group col-md-6">
                <label>Référence Locataire:</label>
                 <input type="text" name="idlocataire" class="form-control">
                </div>

                <div class="form-group col-md-6">
                <label>Date de début du contrat:</label>
                <input type="text" name="datedebut" class="form-control">
                </div>

                <div class="form-group col-md-6">
                    <label>Date de fin du contrat:</label>
                    <input type="text" name="datefin" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Montant du loyer:</label>
                    <input type="text" name="prixloyer" class="form-control">
                </div>
            </div>
            <div class="container-button-form">
                <button type="submit" class="button" name="valider">Valider</button>
            </div>
        </div>
    </form>

    <?php
    if (isset($_POST['valider'])) {
        $contrat = $acces_bd->modifyContrat($_POST);

        echo '<script>alert ("Contrat modifié avec succès!"); </script>';
    }
    ?>
</div>
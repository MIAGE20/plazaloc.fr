<div class="row banner-title banner-title-agence" style="margin-left: 0; margin-right: 0">
    <h1>Qui sommes-nous?</h1>
</div>

<section class="agence">
    <h1>Votre nouvelle agence Stéphane Plaza Immobilier à Jussieu!</h1>
    <h3 class="subtitle">Le réseau Stéphane Plaza s'agrandit avec l'ouverture d'une nouvelle agence à Paris Jussieu.</h3>

    <!-- <div class="container-video"> -->
        <!--<video controls="controls">
            <source src="files/video_pub.mp4" type="video/mp4" />
            <source src="files/video_pub.webm" type="video/webm" />
        </video>-->
        <!-- <iframe width="780" height="370" src="https://www.youtube.com/embed/UFt1L7csbzo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> -->
        <video src="files/video_pub.webm" controls poster="sintel.jpg" width="980" height="670"></video>
    </div>

    <h1>Nos priorités</h1>
    <ul>
        <li>La bienveillance pour nos clients</li>
        <li>La compréhension de leur situation de vie</li>
        <li>L'envie de les faire réussir!</li>
    </ul>

    <div class="container-brand">
        <div class="brand brand1">
            <h3>Une marque authentique</h3>
            <div>
                <img src="images/chat.png" alt="" title="Une marque authentique">
            </div>
            <h4>Nous sommes à votre écoute</h4>
            <p>Pour mieux comprendre votre projet de vie,
                nous prenons le temps d'analyser votre besoin
                et d'y répondre. Nous ne travaillons pas pour nous,
                mais pour vous. Nous communiquons de manière
                transparente... tout simplement parce que
                nous nous engageons vraiment!</p>
        </div>
        <div class="brand brand2">
            <h3>Une marque référente</h3>
            <div>
                <img src="images/bar-chart.png" alt="" title="Une marque référente">
            </div>
            <h4>L'expertise est notre marque de fabrique</h4>
            <p>Avoir l'esprit ouvert, faire preuve de curiosité,
                ne pas avoir peur d'échnager avec des confrères,
                se tenir au courant des lois qui changent
                en permanence: c'est ce qui caractérise
                pour nous un bon agent immobilier.</p>
        </div>
        <div class="brand brand3">
            <h3>Une marque connectée</h3>
            <div>
                <img src="images/wifi.png" alt="" title="Une marque connectée">
            </div>
            <h4>Autant vous prévenir,
                on ne va plus se quitter!</h4>
            <p>Pour nous, l'immobilier au quotidien,
                c'est avec les outils d'aujourd'hui:
                internet, réseaux sociaux, smartphones...
                où que vous soyez, nous resterons connectés.
                Notre marque est là pour vous simplifier la vie!</p>
        </div>
    </div>
    <a href="files/plaquette_4pages.pdf" class="button">Télécharger notre plaquette</a>
</section>
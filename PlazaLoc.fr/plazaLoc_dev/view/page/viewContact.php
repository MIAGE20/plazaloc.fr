<?php
require_once 'model/dao_admin.class.php';

$acces_bd = new dao_admin();
/*$acces_bd = new dao_admin("localhost", "agence_plaza", "root", "");*/
?>


<div class="row banner-title banner-title-contact" style="margin-left: 0; margin-right: 0">
    <h1>Nous contacter</h1>
</div>

<div class="row" style="margin-left: 0; margin-right: 0">
    <div class="container-form">
        <form method="post" action="" class="col-lg-6" name="contactForm" onsubmit="return validateContactForm()">
                <div class="form-group">
                    <label>Nom *:</label>
                    <input type="text" name="nom" class="form-control">
                </div>
                <div class="form-group">
                    <label>Prénom *:</label>
                    <input type="text" name="prenom" class="form-control">
                </div>
                <div class="form-group">
                    <label>Email *:</label>
                    <input type="text" name="email" class="form-control">
                </div>
                <div class="form-group">
                    <label>Téléphone:</label>
                    <input type="text" name="tel" class="form-control">
                </div>
                <div class="form-group">
                    <label>Sujet *:</label>
                    <input type="text" name="sujet" class="form-control">
                </div>
                <div class="row message">
                    <label>Votre message *:</label>
                    <textarea class="form-control" name='message'></textarea>
                    <div class="container-button-form">
                       <button type="submit" class="button" name='valider'>Envoyer</button>
                    </div>
                </div>

            <script type="text/javascript">
                function validateContactForm() {
                    var nom = document.forms["contactForm"]["nom"].value;
                    var prenom = document.forms["contactForm"]["prenom"].value;
                    var email = document.forms["contactForm"]["email"].value;
                    var sujet = document.forms["contactForm"]["sujet"].value;
                    var message = document.forms["contactForm"]["message"].value;
                    if (nom == "" || prenom == "" || email == "" || message == "" || sujet == "") {
                        alert ("Veuillez saisir tous les champs obligatoires");
                        return false;
                    } else {
                        return true;
                    }
                }
            </script>

   
            <?php
                if (isset($_POST['valider'])) {
                    $messages = $acces_bd->insertMessage(
                        $_POST['nom'], 
                        $_POST['prenom'],
                        $_POST['sujet'],
                        $_POST['email'],
                        $_POST['tel'],
                        $_POST['message']);

                    echo '<script>alert ("Votre message est enregistré"); </script>';

                }
            ?>      


        </form>
        <div class="col-lg-6">
            <div id="map"></div>
            <script>
                function initMap() {
                    var uluru = {lat: 48.8461, lng: 2.3548};
                    var map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 14,
                        center: uluru
                    });
                    var marker = new google.maps.Marker({
                        position: uluru,
                        map: map
                    });
                }
            </script>
            <h3>Plaza Loc</h3>
            <p>7, Rue de la Réussite</p>
            <p>75005 Paris</p>
            <p>01 76 39 01 39</p>
        </div>
    </div>
</div>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5aCHMhPs-mlL7JTlQpGAlQq0I2m2hODI&callback=initMap">
</script>
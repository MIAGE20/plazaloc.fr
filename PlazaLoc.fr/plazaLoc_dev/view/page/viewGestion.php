<div class="row banner-title banner-title-gestion-locative" style="margin-left: 0; margin-right: 0">
    <h1>Gestion Locative</h1>
</div>

<div class="title-gestion-locative">
    <h1>Notre agence vous accompane dans la gestion de votre bien</h1>
    <h4>Afin de vous faciliter la vie, faites appel à notre agence!</h4>
</div>

<section class="info-location">
    <div class="container-brand">
        <div class="brand brand1 box-location">
            <h4>Investissement locatif</h4>
            <div>
                <img src="images/calculator.png" alt="" title="">
            </div>
            <p>Vous souhaitez acheter pour louer en toute confiance?
                Plusieurs possibilités s'offrent à vous.
                Découvrez tous nos conseils!</p>
        </div>
        <div class="brand brand2 box-location">
            <h4>Location</h4>
            <div>
                <img src="images/rent.png" alt="" title="">
            </div>
            <p>Nous nous chargeons de promouvoir votre bien, de trouver votre locataire jusqu'à la remise des clés.</p>
        </div>
        <div class="brand brand3 box-location">
            <h4>Gestion locative</h4>
            <div>
                <img src="images/note.png" alt="" title="">
            </div>
            <p>Stéphane Plaza Immobilier Jussieu prend en charge la gestion complète de votre bien:
                de la recherche du locataire à la gestion courante.</p>
        </div>
        <div class="brand brand4 box-location">
            <h4>Location saisonnière</h4>
            <div>
                <img src="images/resort.png" alt="" title="">
            </div>
            <p>Vous avez planifié vos vacances
                et vous êtes à la recherche d'une location pour les vacances?
                Trouvez le bien idéal grâce à nos services!</p>
        </div>
    </div>
    <div class="container-button-form">
        <a href="index.php?page=7" class="button">Créer mon Espace Propriétaire</a>
    </div>
</section>
<div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:1300px;height:460px;overflow:hidden;visibility:hidden;">
    <!-- Loading Screen -->
    <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:460px;text-align:center;background-color:rgba(0,0,0,0.7);">
        <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="slide/img/spin.svg" />
    </div>
    <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1300px;height:800px;overflow:hidden;">
        <div data-p="225.00">
            <img data-u="image" src="slide/img/001.jpg" class="img-slide" />
            <div class="title-slide">
                <h3>Notre expertise</h3>
                <p>Parce que votre projet est unique, nous mettons tout en œuvre afin de le réaliser.</p>
                <a href="index.php?page=3" class="button-slide">En savoir plus</a>
            </div>
        </div>
        <div data-p="225.00">
            <img data-u="image" src="slide/img/002.jpg" class="img-slide" />
            <div class="title-slide">
                <h3>Nouveaux services</h3>
                <p>Notre agence vous propose des services inédits et complètement innovants!</p>
                <a href="files/plaquette_4pages.pdf" class="button-slide">En savoir plus</a>
            </div>
        </div>
        <div data-p="225.00">
            <img data-u="image" src="slide/img/003.jpg" class="img-slide" />
            <div class="title-slide">
                <h3>Gestion locative</h3>
                <p>Nous vous accompagnons dans la gestion de votre bien. </p>
                <a href="index.php?page=2" class="button-slide">En savoir plus</a>
            </div>
        </div>
    </div>
    <!-- Bullet Navigator -->
    <div data-u="navigator" class="jssorb032" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
        <div data-u="prototype" class="i" style="width:16px;height:16px;">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <circle class="b" cx="8000" cy="8000" r="5800"></circle>
            </svg>
        </div>
    </div>
    <!-- Arrow Navigator -->
    <div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
        <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
            <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
        </svg>
    </div>
    <div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
        <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
            <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
        </svg>
    </div>
</div>

<section class="location">
    <h1>Location</h1>
    <h3 class="subtitle">Louez en toute sérénité avec notre agence!</h3>

    <div class="container-location">
        <div class="column-location column-left">
            <h3><a href="index.php?page=6">Je cherche une location</a></h3>
        </div>
        <div class="column-location column-right">
            <h3><a href="index.php?page=7">Je propose une location</a></h3>
        </div>
    </div>
    <div class="desc-location">
        <h3>Comment louer facilement un logement avec Plaza Loc?</h3>
        <ul>
            <li><span class="num-list">1</span> <p>Le locataire décrit sa location idéale en quelques clics</p></li>
            <li><span class="num-list">2</span> <p>Sa demande est transmise aux propriétaires concernés</p></li>
            <li><span class="num-list">3</span> <p>Les propriétaires contactent les locataires qu'ils sélectionnent</p></li>
        </ul>
    </div>
</section>

<section class="services">
    <h1>Nos services</h1>

    <h3 class="subtitle">Plaza Loc vous accompagne dans la réussite de vos projets</h3>

    <div class="container-services">
        <div class="service">
            <div>
                <img src="images/Capture du 2018-03-03 18-12-35.png" alt="" title="Location">
            </div>
            <h3><a href="index.php?page=1">Location</a></h3>
        </div>
        <div class="service">
            <div>
                <img src="images/Capture du 2018-03-03 18-12-57.png" alt="" title="Gestion">
            </div>
            <h3><a href="index.php?page=2">Gestion</a></h3>
        </div>
        <div class="service">
            <div>
                <img src="images/drone.png" alt="" title="Achat">
            </div>
            <h3><a href="files/plaquette_4pages.pdf">Nouveau!</a></h3>
        </div>
    </div>
</section>

<section class="banner">
    <h1>Pourquoi choisir Plaza Loc?</h1>
    <h3 class="subtitle">Nous mettons toutes les garanties de votre côté</h3>

    <div class="container-brand">
        <div class="brand brand1">
            <h3>Une marque authentique</h3>
            <div>
                <img src="images/chat.png" alt="" title="Une marque authentique">
            </div>
            <h4>Nous sommes à votre écoute</h4>
            <p>Pour mieux comprendre votre projet de vie,
                nous prenons le temps d'analyser votre besoin
                et d'y répondre. Nous ne travaillons pas pour nous,
                mais pour vous. Nous communiquons de manière
                transparente... tout simplement parce que
                nous nous engageons vraiment!</p>
        </div>
        <div class="brand brand2">
            <h3>Une marque référente</h3>
            <div>
                <img src="images/bar-chart.png" alt="" title="Une marque référente">
            </div>
            <h4>L'expertise est notre marque de fabrique</h4>
            <p>Avoir l'esprit ouvert, faire preuve de curiosité,
                ne pas avoir peur d'échnager avec des confrères,
                se tenir au courant des lois qui changent
                en permanence: c'est ce qui caractérise
                pour nous un bon agent immobilier.</p>
        </div>
        <div class="brand brand3">
            <h3>Une marque connectée</h3>
            <div>
                <img src="images/wifi.png" alt="" title="Une marque connectée">
            </div>
            <h4>Autant vous prévenir,
                on ne va plus se quitter!</h4>
            <p>Pour nous, l'immobilier au quotidien,
                c'est avec les outils d'aujourd'hui:
                internet, réseaux sociaux, smartphones...
                où que vous soyez, nous resterons connectés.
                Notre marque est là pour vous simplifier la vie!</p>
        </div>
    </div>

    <a href="index.php?page=4" class="button">Nous contacter</a>
</section>

<section class="container-new-services">
    <h1>Nouveau chez Plaza Loc!</h1>

    <div class="container-blocks">
        <div class="new-service new-service1">
            <h3>Home staging</h3>
            <p>Vendez votre bien plus facilement en lui redonnant un coup de peps!</p>
            <a href="files/page_1_plaquette.pdf">En savoir plus</a>
        </div>
        <div class="new-service new-service2">
            <h3>Maison clé en main</h3>
            <p>Une déco épurée et au goût du jour pour un bien prêt à être habité!</p>
            <a href="files/page_2_plaquette.pdf">En savoir plus</a>
        </div>
        <div class="new-service new-service3">
            <h3>Visite virtuelle</h3>
            <p>Visitez votre propriété à construire comme si vous y étiez!</p>
            <a href="files/page_3_plaquette.pdf">En savoir plus</a>
        </div>
        <div class="new-service new-service4">
            <h3>Vidéo par drone</h3>
            <p>Offrez une visite de votre propriété complètement innovante!</p>
            <a href="files/page_4_plaquette.pdf">En savoir plus</a>
        </div>
    </div>
</section>
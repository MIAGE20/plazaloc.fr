<?php 
require_once ('model/dao_localisation.class.php');

$acces_bd = new dao_locataire();
$lesBiens = $acces_bd->catalogBien() ;
?>

<div class="container-biens-catalogue">
    <div class="container-catalogue">
        <?php
        foreach ($lesBiens as $unBien) {
            ?>
            <div class="box-bien">
                <a href="index.php?page=<?= $unBien['idbien'] ; ?>">
                    <img src="images/<?= $unBien['image'] ?>">
                    <div class="desc-bien">
                        <h4 class="title-bien"><?= $unBien['titlebien'] ?></h4>
                        <h4><?= $unBien['superficie'] ?> m²<span class="price"><?= $unBien['montantLoyer'] ?> € / mois</span></h4>
                    </div>
                </a>
            </div>
            <?php
        }
        ?>
    </div>
</div>

<!--<div class="row banner-title banner-title-contact" style="margin-left: 0; margin-right: 0">
    <h1> Rechercher votre bien</h1>
</div> -->

<div class="row" style="margin-left: 0; margin-right: 0"><br>
    <div class="container-form">
           <form method="post" action="" class="col-lg-6" name="trouveBien" onsubmit="return ValidateTrouveBien()">
            <div class="form-group">
                <h1> Rechercher votre bien</h1> <br/>
                <label>Rechercher par quartier *:</label>
                <input type="text" name="quartier" class="form-control">
            </div>
            <div class="container-button-form">
                <button type="submit" class="button" name='valider'>Envoyer</button>
            </div>
    </div>

    <script type="text/javascript">
        function ValidateTrouveBien() {
            var quartier = document.forms["trouveBien"]["quartier"].value;
            if (quartier == "") {
                alert ("Veuillez saisir le nom du quartier");
                return false;
            } else {
                return true;
            }
        }
    </script>
    <?php
    if (isset($_POST['valider'])) {
        $res = $acces_bd->trouveBienByQuartier($_POST["quartier"]);
        if ($res->estVide()) {
            echo '<script>
                    alert("Bien non trouve!")
                </script>';
            die;
        } else {
            echo '
                <div class="row box" style="margin: 10px">
                <h3>'.$res->getNbBiens(); echo ' biens trouves</h3>
                <div class="box-content">
                    <table class="table table-striped">
                        <tr>
                            <td> Ville </td>
                            <td> Adresse </td>
                            <td> Quartier </td>
                            <td> Description </td>
                            <td> nombre de pièces </td>
                            <td> Superficie </td>
                            <td> Montant du loyer </td>
                            <td> Charge forfaitaire </td>
                        </tr>
                 ';
            //$bien = $acces_bd->trouveBienByQuartier($_POST["quartier"]);

            foreach ($res->getBiens() as $bien) {
                echo "
                                    <tr> 
                                    <td>" . $bien['nomville'] . "</td>
                                    <td>" . $bien['adresse'] . "</td>
                                 <!--   <td><em><strong>" . $bien['nomquartier'] . "</strong></em></td> -->
                                    <td><b>" . $bien['nomquartier'] . "</b></td>
                                    <td>" . $bien['titlebien'] . "</td>
                                    <td>" . $bien['nbpiece'] . "</td>
                                    <td>" . $bien['superficie'] . "</td>
                                    <td>" . $bien['montantLoyer'] . "</td>
                                    <td>" . $bien['chargeforfaitaire'] . "</td>
                                   </tr>
                                 ";
            }
        }
    }
?>
                    </table>
                </div>
            </div>
    </form>
</div>

<!-- <div id="map"></div> -->
<!--<div id="map" style="height:50%;width:100%;margin:auto;"></div>-->

<div id="map" style="height:50%;width:75%;margin:auto;margin-top:50px;margin-bottom: 50px"></div>

<script>
    var map;
    var marker;
    var infowindow;
    var red_icon =  'http://maps.google.com/mapfiles/kml/pal2/icon10.png' ;
    var purple_icon =  'http://maps.google.com/mapfiles/kml/pal2/icon10.png' ;
    var locations = <?php echo ((new dao_localisation())->get_all_locations()) ?>;

    function initMap() {
        var paris = {lat: 48.8461, lng: 2.3548};
        infowindow = new google.maps.InfoWindow();
        map = new google.maps.Map(document.getElementById('map'), {
            center: paris,
            zoom: 12
        });


        var i ; var confirmed = 0;
        for (i = 0; i < locations.length; i++) {

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map,
                icon :   locations[i][4] === '1' ?  red_icon  : purple_icon,
                html: document.getElementById('form')
            });

       google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    confirmed =  locations[i][4] === '1' ?  'checked'  :  0;
                    $("#confirmed").prop(confirmed,locations[i][4]);
                    $("#id").val(locations[i][0]);
                    $("#description").val(locations[i][3]);
                    $("#form").show();
                    infowindow.setContent(marker.html);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }
    }
 

    function downloadUrl(url, callback) {
        var request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function() {
            if (request.readyState == 4) {
                callback(request.responseText, request.status);
            }
        };

        request.open('GET', url, true);
        request.send(null);
    }
</script>

<div style="display: none" id="form">
    <table class="map1">
        <tr>
            <input name="id" type='hidden' id='id'/>
            <td><a>Description:</a></td>
            <td><textarea disabled id='description' placeholder='Description'></textarea></td>
        </tr>
    </table>
</div>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5aCHMhPs-mlL7JTlQpGAlQq0I2m2hODI&callback=initMap">
</script>

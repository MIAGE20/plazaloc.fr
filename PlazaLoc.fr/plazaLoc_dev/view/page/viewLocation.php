<div class="row banner-title banner-title-location" style="margin-left: 0; margin-right: 0">
    <h1>Location</h1>
</div>

<div class="title-location">
    <h1>La location facile avec Stéphane Plaza Immobilier Jussieu</h1>
    <h4>Vous recherchez un appartement ou une maison à louer ?</h4>
    <h4>Pour partager une colocation ou encore trouver un logement pour vos vacances ?</h4>
    <h4>Découvrez nos outils et nos conseils adaptés à vos besoins, pour mieux appréhender vos futures démarches de location.</h4>
    <h1>Nos conseils dans les différentes étapes de votre projet</h1>
    <h4>Nous vous accompagnons dans la réussite de vos projets.</h4>
</div>

<section class="info-location">
    <div class="container-brand">
        <div class="brand brand1 box-location">
            <h4>Tout savoir sur la location</h4>
            <div>
                <img src="images/raise-your-hand-to-ask.png" alt="" title="">
            </div>
            <p>Découvrez les étapes clé de votre projet de location</p>
        </div>
        <div class="brand brand2 box-location">
            <h4>La location étudiante</h4>
            <div>
                <img src="images/graduate-student-avatar.png" alt="" title="">
            </div>
            <p>Découvrez tous nos conseils pour une location réussie!</p>
        </div>
        <div class="brand brand3 box-location">
            <h4>Location saisonnière</h4>
            <div>
                <img src="images/resort.png" alt="" title="">
            </div>
            <p>Vous avez planifié vos vacances
                et vous êtes à la recherche d'une location pour les vacances?
                Trouvez le bien idéal grâce à nos services!</p>
        </div>
    </div>
    <div class="container-button-form">
        <a href="index.php?page=6" class="button">Créer mon Espace Locataire</a>
    </div>
</section>
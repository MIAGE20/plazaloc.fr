$(document).ready(function() {

    $('#appartement').on('click', function () {
        var hiddenSelect = $('.hidden-select');
        var hiddenSelectAppart = $('.hidden-select-appart');
        var hiddenSelectMaison = $('.hidden-select-maison');

        hiddenSelect.show();
        hiddenSelectAppart.show();
        hiddenSelectMaison.hide();
    });

    $('#maison').on('click', function () {
        var hiddenSelect = $('.hidden-select');
        var hiddenSelectAppart = $('.hidden-select-appart');
        var hiddenSelectMaison = $('.hidden-select-maison');

        hiddenSelect.show();
        hiddenSelectAppart.hide();
        hiddenSelectMaison.show();
    });

    $('#studio').on('click', function () {
        var hiddenSelect = $('.hidden-select');
        var hiddenSelectAppart = $('.hidden-select-appart');
        var hiddenSelectMaison = $('.hidden-select-maison');

        hiddenSelect.hide();
        hiddenSelectAppart.hide();
        hiddenSelectMaison.hide();
    });

    function showSection(input, hiddenSection, div) {

        $(input).on('click', function() {
            hiddenSection = $(div);
            hiddenSection.show();
        });
    }

    showSection('.radio1', 'hiddenSection1', '.hidden-section1');
    showSection('.radio2', 'hiddenSection2', '.hidden-section2');
});
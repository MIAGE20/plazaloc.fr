$(document).ready(function() {

    $('.link-connexion').on('click', function() {
        var dropdownConnexion = $('.dropdown-connexion');
        dropdownConnexion.show();

        $('.glyphicon-remove').on('click', function () {
            dropdownConnexion.hide();
        });
    });
});